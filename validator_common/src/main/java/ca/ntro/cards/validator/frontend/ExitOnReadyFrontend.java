/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.cards.validator.frontend;

import ca.ntro.app.frontend.FrontendFx;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.session.SessionRegistrar;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.cards.validator.messages.MsgReadyToExit;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_impl.AppWrapperFx;
import javafx.application.Platform;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import ca.ntro.app.Ntro;
import ca.ntro.app.events.EventRegistrar;

public class ExitOnReadyFrontend implements FrontendFx {

	@Override
	public void registerEvents(EventRegistrar registrar) {
		
	}

	@Override
	public void registerViews(ViewRegistrarFx registrar) {
		
	}

	@Override
	public void createTasks(FrontendTasks tasks) {
		tasks.task("exitWhenReady")
		     .waitsFor(message(MsgReadyToExit.class))
		     .executes(inputs -> {
		    	 
		    	 System.out.println("");
		    	 System.out.println("");
		    	 Ntro.exit();
		    	 

		     });
	}

	@Override
	public void registerSessionClass(SessionRegistrar registrar) {
	}

}

package ca.ntro.cards.validator;

import java.io.File;

public class ValidatorUtils {

	public static void resetDir(File dir) {
		System.out.println("\n\n[DELETING DATA] " + dir.getAbsolutePath());
		if(dir.exists()) {
			deleteFiles(dir);
			dir.delete();
		}

		dir.mkdir();
	}

	public static void deleteFiles(File dir) {

		for(File file : dir.listFiles()) {
			if(file.isDirectory()) {
				deleteFiles(file);
				file.delete();
			}else {
				file.delete();
			}
		}
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.cards.validator.backend;

import ca.ntro.app.tasks.backend.BackendTasks;
import ca.ntro.cards.validator.messages.MsgReadyToExit;
import ca.ntro.cards.validator.messages.MsgValidateTestCase;
import ca.ntro.cards.validator.models.ValidatorModel;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.reflection.object_graph.Initialize;
import ca.ntro.app.Ntro;

import static ca.ntro.app.tasks.backend.BackendTasks.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class ValidatorBackend<MODEL extends ValidatorModel> extends CommonBackend<MODEL> {

	@Override
	protected void createAdditionnalTasks(BackendTasks tasks) {

		tasks.task("sendMessagesToTriggerValidation")
			 .waitsFor("init")
			 .executes(inputs -> {
				 
				for(Map.Entry<Class<? extends MODEL>, LinkedHashSet<String>> entry : getModelInstances().entrySet()) {
					Class<? extends MODEL> modelClass = entry.getKey();
					Set<String> modelIds = entry.getValue();
					
					for(String modelId : modelIds) {
						
						if(modelId.equals("")) {

							Ntro.newMessage(MsgValidateTestCase.class)
								.setModelClass(modelClass)
								.setModelId(modelId)
								.send();
							
						}else {

							Ntro.newMessage(MsgValidateTestCase.class)
								.setModelSelection(modelClass, modelId)
								.setModelClass(modelClass)
								.setModelId(modelId)
								.send();
						}
					}
				}
			 });

		tasks.task("sendExitMessage")
			 .waitsFor("sendMessagesToTriggerValidation")
			 .executes(inputs -> {

				Ntro.newMessage(MsgReadyToExit.class).send();

			 });

		for(Class<? extends MODEL> modelClass : getModelClasses()) {

			tasks.task("validate" + NtroCoreImpl.reflection().simpleName(modelClass))
			     .waitsFor("init")
			     .waitsFor(message(MsgValidateTestCase.class))
			     .waitsFor(model(modelClass))
			     .executes(inputs -> {
			    	 
			    	 MsgValidateTestCase msgValidateTestCase = inputs.get(message(MsgValidateTestCase.class));
			    	 MODEL              studentModel         = inputs.get(model(modelClass));
			    	 
			    	 if(msgValidateTestCase.appliesTo(modelClass)) {

						 String modelId = msgValidateTestCase.modelId();
						 
						 if(studentModel.shouldInitializeBeforeValidation()) {
							 studentModel.initialize(modelId);
						 }
						 
						 MODEL testCase = Database.readTestCase(modelClass, modelId);
						 
						 boolean valid = false;
						 
						 try {
							 
							 valid = NtroCoreImpl.reflection().graphEquals(testCase, studentModel);
							 
						 }catch(StackOverflowError e) {
							 e.printStackTrace();
							 
						 }
						 
						 if(valid) {
							 
							 String nomModele = NtroCoreImpl.reflection().simpleName(modelClass);
							 if(!modelId.equals("")) {
								 nomModele += "-" + modelId;
							 }

							 System.out.println("[OK]     votre modèle " + nomModele + " est valide");

						 }else {

							 try {

								
								errorMessage(modelClass, modelId, new File(".").getCanonicalPath());
								

							} catch (IOException e) {

								errorMessage(modelClass, modelId, new File(".").getAbsolutePath());

							}
						 }
			    	 }
			     });
		}
	}

	private void errorMessage(Class<? extends MODEL> modelClass, 
			                  String modelId,
			                  String curDirPath) {

		 String nomModele = NtroCoreImpl.reflection().simpleName(modelClass);
		 if(!modelId.equals("")) {
			 nomModele += "-" + modelId;
		 }
		 
		 String nomGraphe = nomModele.replace("-", "_");

		System.out.println("[ERREUR] votre modèle " 
		                   + nomModele
		                   + " contient des erreurs. Voir la visualisation de votre modèle: " 
		                   + Paths.get(NtroCore.options().graphsPath(), nomGraphe + ".png"));
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.cards.validator;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.ntro.app.Ntro;
import ca.ntro.app.NtroAppFx;
import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.app.frontend.FrontendRegistrarFx;
import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.Model;
import ca.ntro.app.models.ModelRegistrar;
import ca.ntro.app.models.ModelValue;
import ca.ntro.cards.validator.backend.CommonBackend;
import ca.ntro.cards.validator.frontend.ExitOnReadyFrontend;
import ca.ntro.cards.validator.messages.MsgGenerateTestCase;
import ca.ntro.cards.validator.messages.MsgReadyToExit;
import ca.ntro.cards.validator.messages.MsgValidateTestCase;
import ca.ntro.cards.validator.models.ValidatorModel;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.backend.BackendRegistrarNtro;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.reflection.object_graph.Initialize;
import ca.ntro.app.Ntro;

@SuppressWarnings("rawtypes")
public abstract class CommonApp<MODEL extends ValidatorModel,
                                BACKEND extends CommonBackend> implements NtroAppFx, Validator<MODEL> {

	static {
    	NtroAppFx.setOptions(options -> {
    		
    		options.useJarResources(true);

    		options.setCoreOptions(coreOptions -> {
    			coreOptions.displayTasks(false);
    			coreOptions.displayWarnings(false);
    		});
    	});
	}

	private Set<Class<? extends MODEL>> modelClasses = new HashSet<>();
	private Map<Class<? extends MODEL>, LinkedHashSet<String>> modelInstances = new HashMap<>();
	private List<Class<? extends ModelValue>> valueClasses = new ArrayList<>();

	protected Set<Class<? extends MODEL>> getModelClasses(){
		return modelClasses;
	}

	protected List<Class<? extends ModelValue>> getValueClasses(){
		return valueClasses;
	}
	
	protected abstract void validateModels(Validator<MODEL> validator);
	
	@Override
	public void validateModel(Class<? extends MODEL> modelClass, String modelId) {
		modelClasses.add(modelClass);

		LinkedHashSet<String> modelIds = modelInstances.get(modelClass);
		if(modelIds == null) {
			modelIds = new LinkedHashSet<>();                            
			modelInstances.put(modelClass, modelIds);
		}
		
		modelIds.add(modelId);
	}
	
	public CommonApp() {
		if(shouldResetDb()) {
			resetDb();
		}
		
		if(shouldResetStorage()) {
			resetStorage();
		}

		obtainModelsToValidate();
	}
	
	protected abstract boolean shouldResetDb();
	protected abstract boolean shouldResetStorage();

	@Override
	public void registerValue(Class<? extends ModelValue> valueClass) {
		valueClasses.add(valueClass);
	}

	protected void obtainModelsToValidate() {
		validateModels(this);

		if(modelClasses == null || modelClasses.isEmpty()) {
			System.out.println("\n\n\n[FATAL ERROR] please register at least one model class");
			NtroCore.exit(() -> {});
		}
	}

	@Override
	public void registerModels(ModelRegistrar registrar) {
		
		for(Class<? extends Model> modelClass : modelClasses) {

			registrar.registerModel(modelClass);
		}

		for(Class<? extends ModelValue> valueClass : valueClasses) {

			registrar.registerValue(valueClass);
		}
		
		registerAdditonalModels(registrar);
	}

	protected abstract void registerAdditonalModels(ModelRegistrar registrar);

	protected abstract Class<? extends BACKEND> backendClass();

	@SuppressWarnings("unchecked")
	@Override
	public void registerBackend(BackendRegistrar registrar) {
		BackendRegistrarNtro registrarNtro = (BackendRegistrarNtro) registrar;

		BACKEND backend = NtroCore.factory().newInstance(backendClass());
		
		backend.registerModelClasses(getModelClasses());
		backend.registerModelInstances(modelInstances);
		
		registrarNtro.registerBackendObject(backend);
	}

	@Override
	public void registerFrontend(FrontendRegistrarFx registrar) {
		registrar.registerFrontend(ExitOnReadyFrontend.class);
	}

	@Override
	public void registerMessages(MessageRegistrar registrar) {
		registrar.registerMessage(MsgGenerateTestCase.class);
		registrar.registerMessage(MsgValidateTestCase.class);
		registrar.registerMessage(MsgReadyToExit.class);
	}

	private void resetStorage() {
		File storageDir = Paths.get(NtroCore.options().storagePath()).toFile();
		if(storageDir.exists()) {
			ValidatorUtils.resetDir(storageDir);
		}
	}

	private void resetDb() {
		File dbDir = Paths.get(NtroCore.options().projectPath(), "db").toFile();
		if(dbDir.exists()) {
			ValidatorUtils.resetDir(dbDir);
		}
	}

}

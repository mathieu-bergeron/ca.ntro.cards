package ca.ntro.cards.validator.messages;

import ca.ntro.app.messages.Message;
import ca.ntro.cards.validator.models.ValidatorModel;

public class MsgGenerateTestCase extends Message<MsgGenerateTestCase>{
	
	private Class<? extends ValidatorModel> modelClass;
	private String modelId = null;
	
	public MsgGenerateTestCase setModelClass(Class<? extends ValidatorModel> modelClass) {
		this.modelClass = modelClass;
		
		return this;
	}
	
	public MsgGenerateTestCase setModelId(String modelId) {
		this.modelId = modelId;
		
		return this;
	}
	
	public String modelId() {
		return modelId;
	}
	
	public boolean appliesTo(Class<? extends ValidatorModel> modelClass) {
		return this.modelClass.equals(modelClass);
	}
	

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.cards.validator.backend;

import ca.ntro.app.tasks.backend.BackendTasks;
import ca.ntro.cards.validator.messages.MsgGenerateTestCase;
import ca.ntro.cards.validator.messages.MsgReadyToExit;
import ca.ntro.cards.validator.models.ValidatorModel;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.reflection.object_graph.Initialize;

import static ca.ntro.app.tasks.backend.BackendTasks.*;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import ca.ntro.app.Ntro;
import ca.ntro.app.messages.MessageAccessor;

public class GeneratorBackend<MODEL extends ValidatorModel> extends CommonBackend<MODEL> {

	@Override
	protected void createAdditionnalTasks(BackendTasks tasks) {

		tasks.task("sendMessagesToTriggerGeneration")
			 .waitsFor("init")
			 .executes(inputs -> {
				 
				for(Map.Entry<Class<? extends MODEL>, LinkedHashSet<String>> entry : getModelInstances().entrySet()) {
					Class<? extends MODEL> modelClass = entry.getKey();
					Set<String> modelIds = entry.getValue();
					
					for(String modelId : modelIds) {
						
						if(modelId.equals("")) {

							Ntro.newMessage(MsgGenerateTestCase.class)
								.setModelClass(modelClass)
								.setModelId(modelId)
								.send();
							
						}else {

							Ntro.newMessage(MsgGenerateTestCase.class)
								.setModelSelection(modelClass, modelId)
								.setModelClass(modelClass)
								.setModelId(modelId)
								.send();
						}
					}
				}
				
			 });

		tasks.task("sendExitMessage")
			 .waitsFor("sendMessagesToTriggerGeneration")
			 .executes(inputs -> {

				Ntro.newMessage(MsgReadyToExit.class).send();

			 });

		for(Class<? extends MODEL> modelClass : getModelClasses()) {

			tasks.task("generate" + modelClass.getSimpleName())
				 .waitsFor("init")
				 .waitsFor(model(modelClass))
				 .waitsFor(message(MsgGenerateTestCase.class))
				 .executes(inputs -> {
					 
					 MsgGenerateTestCase msgInitializeModel = inputs.get(message(MsgGenerateTestCase.class));
					 MODEL              model              = inputs.get(model(modelClass));

					 if(msgInitializeModel.appliesTo(modelClass)) {

							 String modelId = msgInitializeModel.modelId();
							 
							 model.initialize(modelId);

							 Database.writeTestCase(model, modelId);
							 
							 if(modelId.equals("")) {

								 System.out.println("[GENERATED] " + NtroCoreImpl.reflection().simpleName(modelClass));

							 }else {

								 System.out.println("[GENERATED] " + NtroCoreImpl.reflection().simpleName(modelClass) + "-" + modelId);

							 }
					 }
				 });
		}
	}
}

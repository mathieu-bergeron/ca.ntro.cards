/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift2_procedure;

import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import common.models.enums.Attempt;
import common_procedure.ProcedureApp;
import common_procedure.messages.ProcedureMsgAcceptManualModel;
import common_procedure.test_cases.descriptor.ProcedureTestCaseDescriptor;
import shift2_procedure.backend.Shift2ProcedureBackend;
import shift2_procedure.frontend.Shift2ProcedureFrontend;
import shift2_procedure.frontend.Shift2ProcedureViewData;
import shift2_procedure.frontend.views.Shift2CardsView;
import shift2_procedure.frontend.views.Shift2ProcedureDashboardView;
import shift2_procedure.frontend.views.Shift2ProcedureMessagesView;
import shift2_procedure.frontend.views.Shift2ProcedureRootView;
import shift2_procedure.frontend.views.Shift2ProcedureSettingsView;
import shift2_procedure.frontend.views.fragments.Shift2ProcedureMessageFragment;
import shift2_procedure.messages.Shift2MsgAcceptManualModel;
import shift2_procedure.models.Shift2ProcedureDashboardModel;
import shift2_procedure.models.Shift2ProcedureSettingsModel;
import shift2_procedure.models.Tableau;
import shift2_procedure.models.values.Shift2TestCase;
import shift2_procedure.test_cases.Shift2TestCaseDatabase;
import shift2_procedure.test_cases.descriptor.Shift2TestCaseDescriptor;
import shift2_procedure.test_cases.execution_trace.Shift2ExecutionTrace;

public abstract class   ProcedureDecaler<STUDENT_MODEL extends Tableau>

                extends ProcedureApp<Tableau,           // executable model
                                     STUDENT_MODEL,
                                     STUDENT_MODEL,     // canvas model
                                     Shift2TestCase,
                                     Shift2TestCaseDescriptor,
                                     Shift2TestCaseDatabase,
                                     Shift2ExecutionTrace,
                                     Shift2ProcedureDashboardModel,
                                     Shift2ProcedureSettingsModel,
                                     Shift2MsgAcceptManualModel,
                                     Shift2ProcedureBackend<STUDENT_MODEL>,
                                     Shift2ProcedureRootView,
                                     Shift2CardsView,
                                     Shift2ProcedureDashboardView,
                                     Shift2ProcedureSettingsView,
                                     Shift2ProcedureMessagesView,
                                     Shift2ProcedureMessageFragment,
                                     Shift2ProcedureViewData,
                                     Shift2ProcedureFrontend<STUDENT_MODEL>> {

	@Override
	protected Class<Tableau> executableModelClass() {
		return Tableau.class;
	}

	protected abstract Class<STUDENT_MODEL> classeMonTableau();

	@Override
	protected Class<Shift2TestCase> testCaseClass() {
		return Shift2TestCase.class;
	}

	@Override
	protected Class<Shift2TestCaseDatabase> testCasesModelClass() {
		return Shift2TestCaseDatabase.class;
	}


	@Override
	protected Class<Shift2ProcedureDashboardModel> dashboardModelClass() {
		return Shift2ProcedureDashboardModel.class;
	}


	@Override
	protected Class<Shift2ProcedureSettingsModel> settingsModelClass() {
		return Shift2ProcedureSettingsModel.class;
	}

	@Override
	protected Shift2ProcedureFrontend createFrontend() {
		return new Shift2ProcedureFrontend();
	}


	@Override
	protected Shift2ProcedureBackend createBackend() {
		return new Shift2ProcedureBackend();
	}




	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Class<STUDENT_MODEL> canvasModelClass() {
		return (Class<STUDENT_MODEL>) classeMonTableau();
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return classeMonTableau();
	}

	@Override
	protected Class<Shift2TestCaseDescriptor> testCaseDescriptorClass() {
		return Shift2TestCaseDescriptor.class;
	}

	@Override
	protected Class<Shift2MsgAcceptManualModel> msgAcceptManualModelClass() {
		return Shift2MsgAcceptManualModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}
	
	@Override
	protected Attempt initialAttempt() {
		return Attempt.MANUAL;
	}

}

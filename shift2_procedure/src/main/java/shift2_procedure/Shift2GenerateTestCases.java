/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift2_procedure;

import common.GenerateTestCases;
import shift2_procedure.models.Tableau;
import shift2_procedure.models.values.Shift2TestCase;
import shift2_procedure.test_cases.Shift2TestCaseDatabase;
import shift2_procedure.test_cases.execution_trace.Shift2ExecutionTrace;
import shift2_procedure.test_cases.execution_trace.Shift2ExecutionTraceFull;

public abstract class Shift2GenerateTestCases<STUDENT_MODEL extends Tableau> 

       extends        GenerateTestCases<Tableau, 
                                        STUDENT_MODEL,
                                        Shift2TestCase,
                                        Shift2TestCaseDatabase,
                                        Shift2ExecutionTrace> {

	
	@Override
	protected Class<Shift2TestCase> testCaseClass(){
		return Shift2TestCase.class;
	}

	@Override
	protected Class<Shift2TestCaseDatabase> testCaseDatabaseClass(){
		return Shift2TestCaseDatabase.class;
	}

	@Override
	protected Class<Tableau> executableModelClass() {
		return Tableau.class;
	}

	@Override
	protected boolean shouldWriteJson() {
		return false;
	}

	@Override
	protected Class<? extends Shift2ExecutionTrace> executionTraceClass() {
		return Shift2ExecutionTraceFull.class;
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift2_procedure.backend;



import ca.ntro.app.tasks.backend.BackendTasks;
import common.models.enums.Attempt;

import static ca.ntro.app.tasks.backend.BackendTasks.*;

import shift2_procedure.messages.Shift2MsgAcceptManualModel;
import shift2_procedure.models.Shift2ProcedureDashboardModel;
import shift2_procedure.models.Shift2ProcedureSettingsModel;
import shift2_procedure.models.Tableau;
import shift2_procedure.models.values.Shift2TestCase;
import shift2_procedure.test_cases.Shift2TestCaseDatabase;
import shift2_procedure.test_cases.execution_trace.Shift2ExecutionTrace;
import common_procedure.backend.ProcedureBackend;
import common_procedure.messages.ProcedureMsgAcceptManualModel;

public class   Shift2ProcedureBackend<STUDENT_MODEL extends Tableau>


       extends ProcedureBackend<Tableau,       // ExecutableModel
                                STUDENT_MODEL,
                                STUDENT_MODEL,        // CanvasModel
                                Shift2TestCase,
                                Shift2TestCaseDatabase,
                                Shift2ExecutionTrace,
                                Shift2ProcedureDashboardModel,
                                Shift2ProcedureSettingsModel,
                                Shift2MsgAcceptManualModel> {

	
	@Override
	protected String initialCardsModelId(String initialTestCaseId, Attempt initialAttempt) {
		return initialTestCaseId;
	}
	


}

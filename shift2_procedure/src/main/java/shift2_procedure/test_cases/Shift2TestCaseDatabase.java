/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift2_procedure.test_cases;

import common.test_cases.descriptor.AbstractTestCaseDescriptor;
import common_procedure.test_cases.ProcedureTestCaseDatabase;
import common_procedure.test_cases.execution_trace.ProcedureExecutionTrace;
import shift2_procedure.models.Shift2ProcedureDashboardModel;
import shift2_procedure.models.Tableau;
import shift2_procedure.models.values.Shift2TestCase;
import shift2_procedure.test_cases.execution_trace.Shift2ExecutionTrace;
import ca.ntro.app.Ntro;

public class   Shift2TestCaseDatabase<STUDENT_MODEL extends Tableau> 

       extends ProcedureTestCaseDatabase<Tableau, 
                                         STUDENT_MODEL, 
                                         Shift2TestCase, 
                                         Shift2ExecutionTrace,
                                         Shift2ProcedureDashboardModel> {

    @Override
    public void describeTestCasesToGenerate() {
        
        AbstractTestCaseDescriptor descriptor = AbstractTestCaseDescriptor.create()
                                                          .category("exemples")
                                                          .testCaseId("ex01");
        
        addTestCaseDescriptor(descriptor);

        descriptor = AbstractTestCaseDescriptor.create()
                                               .category("exemples")
                                               .testCaseId("ex02");
        
        addTestCaseDescriptor(descriptor);

        descriptor = AbstractTestCaseDescriptor.create()
                                               .category("exemples")
                                               .testCaseId("ex03");
        
        addTestCaseDescriptor(descriptor);

    }

}

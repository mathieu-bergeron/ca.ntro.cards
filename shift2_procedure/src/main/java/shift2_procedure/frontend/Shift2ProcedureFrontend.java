/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift2_procedure.frontend;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import shift2_procedure.frontend.views.Shift2CardsView;
import shift2_procedure.frontend.views.Shift2ProcedureDashboardView;
import shift2_procedure.frontend.views.Shift2ProcedureMessagesView;
import shift2_procedure.frontend.views.Shift2ProcedureRootView;
import shift2_procedure.frontend.views.Shift2ProcedureSettingsView;
import shift2_procedure.frontend.views.Shift2ReplayView;
import shift2_procedure.frontend.views.Shift2SelectionsView;
import shift2_procedure.frontend.views.Shift2VariablesView;
import shift2_procedure.frontend.views.fragments.Shift2ProcedureMessageFragment;
import shift2_procedure.frontend.views.fragments.Shift2TestCaseFragment;
import shift2_procedure.models.Shift2ProcedureDashboardModel;
import shift2_procedure.models.Shift2ProcedureSettingsModel;
import shift2_procedure.models.Tableau;
import common.frontend.CommonSession;
import common.messages.MsgMessageToUser;
import common_procedure.frontend.ProcedureFrontend;

public class Shift2ProcedureFrontend<STUDENT_MODEL extends Tableau>

       extends ProcedureFrontend<Shift2ProcedureRootView,
                                 Shift2ProcedureSettingsView, 
                                 Shift2CardsView, 
                                 Shift2ProcedureDashboardView, 
                                 Shift2SelectionsView,
                                 Shift2TestCaseFragment,
                                 Shift2ReplayView,
                                 Shift2VariablesView,
                                 Shift2ProcedureMessagesView,
                                 Shift2ProcedureMessageFragment,
                                 Shift2ProcedureViewData, 
                                 STUDENT_MODEL, // CanvasModel
                                 Shift2ProcedureDashboardModel, 
                                 Shift2ProcedureSettingsModel> {


	@Override
	protected boolean isProd() {
		return false;
	}

	@Override
	protected Class<? extends CommonSession> sessionClass() {
		return Shift2Session.class;
	}

	@Override
	protected Class<Shift2ProcedureRootView> rootViewClass() {
		return Shift2ProcedureRootView.class;
	}

	@Override
	protected Class<Shift2ProcedureSettingsView> settingsViewClass() {
		return Shift2ProcedureSettingsView.class;
	}

	@Override
	protected Class<Shift2CardsView> canvasViewClass() {
		return Shift2CardsView.class;
	}

	@Override
	protected Class<Shift2ProcedureDashboardView> dashboardViewClass() {
		return Shift2ProcedureDashboardView.class;
	}


	@Override
	protected Class<Shift2ProcedureViewData> viewDataClass() {
		return Shift2ProcedureViewData.class;
	}



	@Override
	protected Class<Shift2SelectionsView> selectionsViewClass() {
		return Shift2SelectionsView.class;
	}

	@Override
	protected Class<Shift2ReplayView> replayControlsViewClass() {
		return Shift2ReplayView.class;
	}

	@Override
	protected Class<Shift2VariablesView> variablesViewClass() {
		return Shift2VariablesView.class;
	}


	@Override
	protected Class<Shift2TestCaseFragment> testCaseFragmentClass() {
		return Shift2TestCaseFragment.class;
	}

	@Override
	protected Class<Shift2ProcedureMessagesView> messagesViewClass() {
		return Shift2ProcedureMessagesView.class;
	}

	@Override
	protected Class<Shift2ProcedureMessageFragment> messageFragmentClass() {
		return Shift2ProcedureMessageFragment.class;
	}


}

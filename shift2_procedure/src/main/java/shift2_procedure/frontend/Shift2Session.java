package shift2_procedure.frontend;

import common.models.enums.Attempt;
import common_procedure.frontend.ProcedureSession;
import common_procedure.models.ProcedureCardsModel;

public class Shift2Session extends ProcedureSession<Shift2Session> {

	@Override
	public void setCardsModelSelection(Class<? extends ProcedureCardsModel> cardsModelClass, String testCaseId, Attempt attempt) {
		this.setModelSelection(cardsModelClass, testCaseId);
	}

}

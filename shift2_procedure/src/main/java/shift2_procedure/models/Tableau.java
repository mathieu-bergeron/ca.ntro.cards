/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift2_procedure.models;


import ca.ntro.app.Ntro;
import ca.ntro.core.stream.Stream;
import ca.ntro.ntro_core_impl.stream.StreamNtro;
import ca.ntro.core.stream.Visitor;
import common.models.enums.Sorte;
import common.models.values.cards.AbstractCard;
import common.models.values.cards.CardHeap;
import common.models.values.cards.Carte;
import common.models.values.cards.NullCard;
import common.test_cases.descriptor.AbstractTestCaseDescriptor;
import common_procedure.models.ProcedureCardsModel;
import common_procedure.models.values.ComparisonReport;
import shift2_procedure.Shift2Constants;
import shift2_procedure.frontend.Shift2ProcedureViewData;
import shift2_procedure.frontend.views.Shift2VariablesView;
import shift2_procedure.models.world2d.Shift2ProcedureDrawingOptions;
import shift2_procedure.models.world2d.Shift2ProcedureWorld2d;

public class   Tableau

       extends ProcedureCardsModel<Tableau, 
                                   Shift2ProcedureWorld2d, 
                                   Shift2ProcedureDrawingOptions, 
                                   Shift2ProcedureViewData,
                                   Shift2VariablesView> {
	 
	
	public static final int MARGIN_LEFT = 50;
	
	protected int aDeplacer = 0;
	protected int i = -1;
	protected Carte[] cartes = new Carte[0];

	protected Carte memoireA = null;
	protected Carte memoireB = null;
	
	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public Carte[] getCartes() {
		return cartes;
	}

	public void setCartes(Carte[] cartes) {
		this.cartes = cartes;
	}

	public int getADeplacer() {
		return aDeplacer;
	}

	public void setADeplacer(int aDeplacer) {
		this.aDeplacer = aDeplacer;
	}

	public Carte getMemoireA() {
		return memoireA;
	}

	public void setMemoireA(Carte memoireA) {
		this.memoireA = memoireA;
	}

	public Carte getMemoireB() {
		return memoireB;
	}

	public void setMemoireB(Carte memoireB) {
		this.memoireB = memoireB;
	}

	@Override
    public void copyDataFrom(Tableau other) {
        cartes = new Carte[other.cartes.length];
        
        for(int i = 0; i < cartes.length; i++) {
        	cartes[i] = other.cartes[i];
        }
        
        this.i = other.i;
        this.aDeplacer = other.aDeplacer;
        this.memoireA = other.memoireA;
        this.memoireB = other.memoireB;
    }

	@Override
	public boolean isValidNextStep(Tableau manualModel) {
		boolean modified = false;

		return modified;
	}

	@Override
	public ComparisonReport compareToSolution(Tableau solution) {
		
		ComparisonReport report = ComparisonReport.emptyReport();


		return report;
	}
	
	private class MaxXY {
		public double maxX = 0;
		public double maxY = 0;
		public MaxXY(double maxX, double maxY){
			this.maxX = maxX;
			this.maxY = maxY;
		}
	}

    @Override
    protected void updateViewDataImpl(CardHeap cardHeap, Shift2ProcedureViewData cardsViewData) {
		double maxX = 0.0;
		double maxY = 0.0;

    	double cardWidth = Shift2Constants.INITIAL_CARD_WIDTH_MILIMETERS;
		double cardHeight = Shift2Constants.INITIAL_CARD_HEIGHT_MILIMETERS;
		
		MaxXY maxXY = displayMemoryCard(cardsViewData, cardHeap, memoireA, i, 0, cardWidth, cardHeight);
		if((maxXY.maxX + cardWidth) > maxX) {
			maxX = maxXY.maxX + cardWidth;
		}

		if((maxXY.maxY + cardHeight) > maxY) {
			maxY = maxXY.maxY + cardHeight;
		}
		
		maxXY = displayMemoryCard(cardsViewData, cardHeap, memoireB, i, 1, cardWidth, cardHeight);
		if((maxXY.maxX + cardWidth) > maxX) {
			maxX = maxXY.maxX + cardWidth;
		}

		if((maxXY.maxY + cardHeight) > maxY) {
			maxY = maxXY.maxY + cardHeight;
		}
		
		for(int i = 0; i < cartes.length; i++) {

			double targetTopLeftX = MARGIN_LEFT + cardWidth + cardWidth / 2 + i * cardWidth * 3 / 2;
			double targetTopLeftY = cardHeight * 3;
			
			AbstractCard card = cartes[i];
			
			if(card == null) {
				card = new NullCard();
			}

			card = cardHeap.firstInstanceOf(card);
			String card2dId = cardHeap.newCard2dId(card);
			
			cardsViewData.addOrUpdateCard(card2dId,
					                      card,
					                      targetTopLeftX,
					                      targetTopLeftY);
			
			cardsViewData.displayCardFaceUp(card2dId);

			if((targetTopLeftX + cardWidth) > maxX) {
				maxX = targetTopLeftX + cardWidth;
			}
			
			if((targetTopLeftY + cardHeight) > maxY) {
				maxY = targetTopLeftY + cardHeight;
			}
		}
		
		double markerHeight = 25;
		
		double markerTopLeftX = MARGIN_LEFT + 10 + cardWidth + cardWidth / 2 + getADeplacer() * cardWidth * 3 / 2;
		double markerTopLeftY = cardHeight * 3 - cardHeight / 3 - markerHeight;
		
		cardsViewData.addOrUpdateMarker("toRemove", "#ff1122", markerTopLeftX, markerTopLeftY);
		cardHeap.addMarkerId("toRemove");

		if((markerTopLeftX + 25) > maxX) {
			maxX = markerTopLeftX + 25;
		}
		
		if((markerTopLeftY + 25) > maxY) {
			maxY = markerTopLeftY + 25;
		}
		
		markerTopLeftX = MARGIN_LEFT + 10 + cardWidth + cardWidth / 2 + getI() * cardWidth * 3 / 2;
		markerTopLeftY = cardHeight * 4 + cardHeight / 3;
		
		cardsViewData.addOrUpdateMarker("i", markerTopLeftX, markerTopLeftY);
		cardHeap.addMarkerId("i");

		if((markerTopLeftX + 25) > maxX) {
			maxX = markerTopLeftX + 25;
		}
		
		if((markerTopLeftY + 25) > maxY) {
			maxY = markerTopLeftY + 25;
		}

		cardsViewData.resizeWorld2d(maxX + MARGIN_LEFT, maxY + MARGIN_LEFT);
    }

	private MaxXY displayMemoryCard(Shift2ProcedureViewData cardsViewData,
								   CardHeap cardHeap,
								   AbstractCard memoryCard, 
							       double offsetX,
							       double offsetY,
			                       double cardWidth, 
			                       double cardHeight) {

		if(memoryCard == null) {
			memoryCard = new NullCard();
		}

		memoryCard = cardHeap.firstInstanceOf(memoryCard);
		String card2dId = cardHeap.newCard2dId(memoryCard);

		double targetTopLeftX = MARGIN_LEFT + cardWidth + cardWidth / 2 + offsetX * cardWidth * 3 / 2;
		double targetTopLeftY = cardHeight * 3 + cardHeight * offsetY + cardHeight * 2 + offsetY * cardHeight / 4;

		cardsViewData.addOrUpdateCard(card2dId, 
				                      memoryCard,
									  targetTopLeftX,
									  targetTopLeftY);

		cardsViewData.displayCardFaceUp(card2dId);
		
		return new MaxXY(targetTopLeftX, targetTopLeftY);

	}

    @Override
    public void initializeAsTestCase(AbstractTestCaseDescriptor descriptor) {

		i = -1;
		memoireA = null;
		memoireB = null;
    }

    @Override
    public int testCaseSize() {
        
    	return cartes.length;
    }
    
    @Override
    protected Stream<Carte> cards() {
        return new StreamNtro<Carte>() {
            @Override
            public void forEach_(Visitor<Carte> visitor) throws Throwable {
                for(int i = 0; i < cartes.length; i++) {
                	visitor.visit(cartes[i]);
                }
            }
        };
    }


    @Override
    public void run() {
    }


    @Override
    public void displayOn(Shift2VariablesView variablesView) {
    	variablesView.displayADeplacer(String.valueOf(aDeplacer));
    	variablesView.displayI(String.valueOf(i));
    	variablesView.displayMemoireA(String.valueOf(memoireA));
    	variablesView.displayMemoireB(String.valueOf(memoireB));
    }
	
}

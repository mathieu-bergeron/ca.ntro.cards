/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift2_procedure.models;

import common_procedure.models.ProcedureDashboardModel;
import shift2_procedure.frontend.views.Shift2ProcedureDashboardView;
import shift2_procedure.frontend.views.Shift2ReplayView;
import shift2_procedure.frontend.views.Shift2SelectionsView;
import shift2_procedure.frontend.views.fragments.Shift2TestCaseFragment;
import shift2_procedure.test_cases.Shift2TestCaseDatabase;
import shift2_procedure.test_cases.descriptor.Shift2TestCaseDescriptor;

public class Shift2ProcedureDashboardModel extends ProcedureDashboardModel<Shift2ProcedureDashboardView, 
                                                                         Tableau, 
                                                                         Shift2TestCaseDatabase,
                                                                         Shift2TestCaseDescriptor,
                                                                         Shift2ReplayView,
                                                                         Shift2SelectionsView,
                                                                         Shift2TestCaseFragment> {

	@Override
	protected String defaultTestCaseId() {
		return "ex01";
	}
	

	@Override
	public void updateCardsModel(Shift2TestCaseDatabase testCaseDatabase, Tableau cardsModel) {
		
		// XXX: nothing, in this example we do NOT want to update the .json file

	}
	


}

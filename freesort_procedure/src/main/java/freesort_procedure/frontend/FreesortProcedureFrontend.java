/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package freesort_procedure.frontend;

import ca.ntro.app.Ntro;
import ca.ntro.app.events.EventRegistrar;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.session.SessionRegistrar;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import common.messages.MsgMessageToUser;
import common_procedure.frontend.ProcedureFrontend;
import freesort_procedure.frontend.views.FreesortCardsView;
import freesort_procedure.frontend.views.FreesortProcedureDashboardView;
import freesort_procedure.frontend.views.FreesortProcedureMessagesView;
import freesort_procedure.frontend.views.FreesortProcedureRootView;
import freesort_procedure.frontend.views.FreesortProcedureSettingsView;
import freesort_procedure.frontend.views.FreesortReplayView;
import freesort_procedure.frontend.views.FreesortSelectionsView;
import freesort_procedure.frontend.views.FreesortVariablesView;
import freesort_procedure.frontend.views.fragments.FreesortProcedureMessageFragment;
import freesort_procedure.frontend.views.fragments.FreesortTestCaseFragment;
import freesort_procedure.models.FreesortProcedureDashboardModel;
import freesort_procedure.models.FreesortProcedureSettingsModel;
import freesort_procedure.models.TriLibre;

public class FreesortProcedureFrontend<STUDENT_MODEL extends TriLibre>

       extends ProcedureFrontend<FreesortProcedureRootView,
                                 FreesortProcedureSettingsView, 
                                 FreesortCardsView, 
                                 FreesortProcedureDashboardView, 
                                 FreesortSelectionsView,
                                 FreesortTestCaseFragment,
                                 FreesortReplayView,
                                 FreesortVariablesView,
                                 FreesortProcedureMessagesView,
                                 FreesortProcedureMessageFragment,
                                 FreesortProcedureViewData, 
                                 STUDENT_MODEL, // CanvasModel
                                 FreesortProcedureDashboardModel, 
                                 FreesortProcedureSettingsModel> {

	@Override
	protected void registerAdditionnalEvents(EventRegistrar registrar) {
		
	}

	@Override
	protected boolean isProd() {
		return false;
	}

	@Override
	protected Class<FreesortProcedureRootView> rootViewClass() {
		return FreesortProcedureRootView.class;
	}

	@Override
	protected Class<FreesortProcedureSettingsView> settingsViewClass() {
		return FreesortProcedureSettingsView.class;
	}

	@Override
	protected Class<FreesortCardsView> canvasViewClass() {
		return FreesortCardsView.class;
	}

	@Override
	protected Class<FreesortProcedureDashboardView> dashboardViewClass() {
		return FreesortProcedureDashboardView.class;
	}

	@Override
	protected void registerAdditionnalViews(ViewRegistrarFx registrar) {
		
	}

	@Override
	protected Class<FreesortProcedureViewData> viewDataClass() {
		return FreesortProcedureViewData.class;
	}

	@Override
	protected void addSubTasksToInitialization(FrontendTasks subTasks) {

	}

	@Override
	protected void addSubTasksToNavigation(FrontendTasks subTasks) {

	}

	@Override
	protected void addSubTasksToSettings(FrontendTasks subTasks) {

	}


	@Override
	protected Class<FreesortSelectionsView> selectionsViewClass() {
		return FreesortSelectionsView.class;
	}

	@Override
	protected Class<FreesortReplayView> replayControlsViewClass() {
		return FreesortReplayView.class;
	}

	@Override
	protected Class<FreesortVariablesView> variablesViewClass() {
		return FreesortVariablesView.class;
	}

	@Override
	protected void addSubTasksToCards(FrontendTasks subTasks) {
	}

	@Override
	protected Class<FreesortTestCaseFragment> testCaseFragmentClass() {
		return FreesortTestCaseFragment.class;
	}

	@Override
	protected Class<FreesortProcedureMessagesView> messagesViewClass() {
		return FreesortProcedureMessagesView.class;
	}

	@Override
	protected Class<FreesortProcedureMessageFragment> messageFragmentClass() {
		return FreesortProcedureMessageFragment.class;
	}



}

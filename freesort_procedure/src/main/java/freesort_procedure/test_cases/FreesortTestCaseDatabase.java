/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package freesort_procedure.test_cases;

import common.test_cases.descriptor.AbstractTestCaseDescriptor;
import common_procedure.test_cases.ProcedureTestCaseDatabase;
import common_procedure.test_cases.execution_trace.ProcedureExecutionTrace;
import freesort_procedure.models.FreesortProcedureDashboardModel;
import freesort_procedure.models.TriLibre;
import freesort_procedure.models.values.FreesortTestCase;
import freesort_procedure.test_cases.execution_trace.FreesortExecutionTrace;

public class   FreesortTestCaseDatabase<STUDENT_MODEL extends TriLibre> 

       extends ProcedureTestCaseDatabase<TriLibre, 
                                         STUDENT_MODEL, 
                                         FreesortTestCase, 
                                         FreesortExecutionTrace,
                                         FreesortProcedureDashboardModel> {

	@Override
	public void describeTestCasesToGenerate() {
		
		AbstractTestCaseDescriptor descriptor = AbstractTestCaseDescriptor.create()
				                                          .category("exemples")
				                                          .testCaseId("ex01");
		
		addTestCaseDescriptor(descriptor);

		descriptor = AbstractTestCaseDescriptor.create()
				                               .category("exemples")
				                               .testCaseId("ex02");
		
		addTestCaseDescriptor(descriptor);

		descriptor = AbstractTestCaseDescriptor.create()
				                               .category("exemples")
				                               .testCaseId("ex03");
		
		addTestCaseDescriptor(descriptor);

		descriptor = AbstractTestCaseDescriptor.create()
				                               .category("exemples")
				                               .testCaseId("ex04");
		
		addTestCaseDescriptor(descriptor);

		descriptor = AbstractTestCaseDescriptor.create()
				                               .category("exemples")
				                               .testCaseId("ex05");
		
		addTestCaseDescriptor(descriptor);

		descriptor = AbstractTestCaseDescriptor.create()
				                               .category("exemples")
				                               .testCaseId("ex06");
		
		addTestCaseDescriptor(descriptor);

		descriptor = AbstractTestCaseDescriptor.create()
				                               .category("exemples")
				                               .testCaseId("ex07");
		
		addTestCaseDescriptor(descriptor);

		descriptor = AbstractTestCaseDescriptor.create()
				                               .category("exemples")
				                               .testCaseId("ex08");
		
		addTestCaseDescriptor(descriptor);
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package freesort_procedure;

import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import common_procedure.ProcedureApp;
import common_procedure.test_cases.descriptor.ProcedureTestCaseDescriptor;
import freesort_procedure.backend.FreesortProcedureBackend;
import freesort_procedure.frontend.FreesortProcedureFrontend;
import freesort_procedure.frontend.FreesortProcedureViewData;
import freesort_procedure.frontend.views.FreesortCardsView;
import freesort_procedure.frontend.views.FreesortProcedureDashboardView;
import freesort_procedure.frontend.views.FreesortProcedureMessagesView;
import freesort_procedure.frontend.views.FreesortProcedureRootView;
import freesort_procedure.frontend.views.FreesortProcedureSettingsView;
import freesort_procedure.frontend.views.fragments.FreesortProcedureMessageFragment;
import freesort_procedure.messages.FreesortMsgAcceptManualModel;
import freesort_procedure.models.FreesortProcedureDashboardModel;
import freesort_procedure.models.FreesortProcedureSettingsModel;
import freesort_procedure.models.TriLibre;
import freesort_procedure.models.values.FreesortTestCase;
import freesort_procedure.test_cases.FreesortTestCaseDatabase;
import freesort_procedure.test_cases.descriptor.FreesortTestCaseDescriptor;
import freesort_procedure.test_cases.execution_trace.FreesortExecutionTrace;

public abstract class   ProcedureTriLibre<STUDENT_MODEL extends TriLibre>

                extends ProcedureApp<TriLibre,           // executable model
                                     STUDENT_MODEL,
                                     STUDENT_MODEL,     // canvas model
                                     FreesortTestCase,
                                     FreesortTestCaseDescriptor,
                                     FreesortTestCaseDatabase,
                                     FreesortExecutionTrace,
                                     FreesortProcedureDashboardModel,
                                     FreesortProcedureSettingsModel,
                                     FreesortMsgAcceptManualModel,
                                     FreesortProcedureBackend<STUDENT_MODEL>,
                                     FreesortProcedureRootView,
                                     FreesortCardsView,
                                     FreesortProcedureDashboardView,
                                     FreesortProcedureSettingsView,
                                     FreesortProcedureMessagesView,
                                     FreesortProcedureMessageFragment,
                                     FreesortProcedureViewData,
                                     FreesortProcedureFrontend<STUDENT_MODEL>> {

                                    	   

	@Override
	protected Class<TriLibre> executableModelClass() {
		return TriLibre.class;
	}


	protected abstract Class<STUDENT_MODEL> classeTriLibre();

	@Override
	protected Class<FreesortTestCase> testCaseClass() {
		return FreesortTestCase.class;
	}

	@Override
	protected Class<FreesortTestCaseDatabase> testCasesModelClass() {
		return FreesortTestCaseDatabase.class;
	}


	@Override
	protected Class<FreesortProcedureDashboardModel> dashboardModelClass() {
		return FreesortProcedureDashboardModel.class;
	}


	@Override
	protected Class<FreesortProcedureSettingsModel> settingsModelClass() {
		return FreesortProcedureSettingsModel.class;
	}

	@Override
	protected FreesortProcedureFrontend createFrontend() {
		return new FreesortProcedureFrontend();
	}


	@Override
	protected FreesortProcedureBackend createBackend() {
		return new FreesortProcedureBackend();
	}


	@Override
	protected void registerAdditionnalModels(ModelRegistrar registrar) {
	}

	@Override
	protected void registerAdditionnalMessages(MessageRegistrar registrar) {

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Class<STUDENT_MODEL> canvasModelClass() {
		return (Class<STUDENT_MODEL>) classeTriLibre();
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return classeTriLibre();
	}

	@Override
	protected Class<FreesortTestCaseDescriptor> testCaseDescriptorClass() {
		return FreesortTestCaseDescriptor.class;
	}

	@Override
	protected Class<FreesortMsgAcceptManualModel> msgAcceptManualModelClass() {
		return FreesortMsgAcceptManualModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}

}

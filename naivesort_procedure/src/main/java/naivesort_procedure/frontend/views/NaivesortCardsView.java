/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package naivesort_procedure.frontend.views;

import java.net.URL;
import java.util.ResourceBundle;

import ca.ntro.app.Ntro;
import ca.ntro.app.fx.controls.ResizableWorld2dCanvasFx;
import common_procedure.frontend.views.ProcedureCanvasView;
import javafx.fxml.FXML;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Pane;
import naivesort_procedure.NaivesortConstants;
import naivesort_procedure.frontend.views.controls.NaivesortMainCanvas;

public class NaivesortCardsView extends ProcedureCanvasView {

	@FXML
	private Pane cardsViewContainer;

	//@FXML
	//private NaivesortPreviewCanvas previewCanvas;

	@FXML
	private SplitPane mainSplitPane;

	@FXML
	private NaivesortMainCanvas mainCanvas;

	@FXML
	private Pane dashboardContainer;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		Ntro.assertNotNull(cardsViewContainer);
		//Ntro.assertNotNull("previewCanvas", previewCanvas);
		Ntro.assertNotNull(mainCanvas);
		Ntro.assertNotNull(dashboardContainer);
		Ntro.assertNotNull(mainSplitPane);

		super.initialize(location, resources);
	}

	@Override
	protected ResizableWorld2dCanvasFx mainCanvas() {
		return mainCanvas;
	}

	@Override
	protected ResizableWorld2dCanvasFx previewCanvas() {
		//return previewCanvas;
		return null;
	}

	@Override
	protected Pane dashboardContainer() {
		return dashboardContainer;
	}

	@Override
	protected Pane cardsViewContainer() {
		return cardsViewContainer;
	}

	@Override
	protected double initialWorldHeight() {
		return NaivesortConstants.INITIAL_WORLD_HEIGHT;
	}

	@Override
	protected double initialWorldWidth() {
		return NaivesortConstants.INITIAL_WORLD_WIDTH;
	}

	@Override
	protected double initialPreviewCanvasScreenHeight() {
		return NaivesortConstants.INITIAL_PREVIEW_CANVAS_SCREEN_HEIGHT;
	}

	@Override
	protected SplitPane mainSplitPane() {
		return mainSplitPane;
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package naivesort_procedure.models;

import common_procedure.models.ProcedureSettingsModel;
import naivesort_procedure.frontend.views.NaivesortProcedureSettingsView;
import naivesort_procedure.models.world2d.NaivesortProcedureDrawingOptions;

public class NaivesortProcedureSettingsModel extends ProcedureSettingsModel<NaivesortProcedureSettingsView, 
                                                                       NaivesortProcedureDrawingOptions> 

       implements NaivesortProcedureDrawingOptions {

	@Override
	public NaivesortProcedureDrawingOptions drawingOptions() {
		return this;
	}

	@Override
	public boolean useFourCardColors() {
		return getUseFourCardColors();
	}

}

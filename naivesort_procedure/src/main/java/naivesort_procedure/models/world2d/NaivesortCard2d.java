/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package naivesort_procedure.models.world2d;

import common.models.enums.Sorte;
import common.models.values.cards.AbstractCard;
import common_procedure.models.world2d.ProcedureCard2d;
import naivesort_procedure.NaivesortConstants;

public class NaivesortCard2d extends ProcedureCard2d<NaivesortProcedureWorld2d> {

	public NaivesortCard2d() {
		super();
	}


	public NaivesortCard2d(String card2dId, AbstractCard card) {
		super(card2dId, card);
	}

	public NaivesortCard2d(int rank, Sorte suit) {
		super(rank, suit);
	}

	@Override
	protected double initialWidth() {
		return NaivesortConstants.INITIAL_CARD_WIDTH_MILIMETERS;
	}

	@Override
	protected double initialHeight() {
		return NaivesortConstants.INITIAL_CARD_HEIGHT_MILIMETERS;
	}



	@Override
	protected void flipCard() {
		super.flipCard();
		
		getWorld2d().registerFlippedCard(this);
	}


}

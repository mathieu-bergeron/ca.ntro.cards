/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package naivesort_procedure;

import common.GenerateTestCases;
import naivesort_procedure.models.TriNaif;
import naivesort_procedure.models.values.NaivesortTestCase;
import naivesort_procedure.test_cases.NaivesortTestCaseDatabase;
import naivesort_procedure.test_cases.execution_trace.NaivesortExecutionTrace;
import naivesort_procedure.test_cases.execution_trace.NaivesortExecutionTraceFull;

public abstract class NaivesortGenerateTestCases<STUDENT_MODEL extends TriNaif> 

       extends        GenerateTestCases<TriNaif, 
                                        STUDENT_MODEL,
                                        NaivesortTestCase,
                                        NaivesortTestCaseDatabase,
                                        NaivesortExecutionTrace> {

	
	@Override
	protected Class<NaivesortTestCase> testCaseClass(){
		return NaivesortTestCase.class;
	}

	@Override
	protected Class<NaivesortTestCaseDatabase> testCaseDatabaseClass(){
		return NaivesortTestCaseDatabase.class;
	}

	@Override
	protected Class<TriNaif> executableModelClass() {
		return TriNaif.class;
	}

	@Override
	protected boolean shouldWriteJson() {
		return false;
	}

	@Override
	protected Class<? extends NaivesortExecutionTrace> executionTraceClass() {
		return NaivesortExecutionTraceFull.class;
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_procedure.test_cases.descriptor;

import java.util.Map;

import ca.ntro.app.Ntro;
import common.test_cases.descriptor.AbstractAttemptDescriptor;
import common_procedure.test_cases.descriptor.ProcedureTestCaseDescriptor;
import shift3_procedure.frontend.views.fragments.Shift3TestCaseFragment;

public class Shift3TestCaseDescriptor extends ProcedureTestCaseDescriptor<Shift3TestCaseFragment> {

	public void fastForwardToLastStep() {
		for(Object obj : getAttempts().entrySet()) {
			if(obj instanceof Map.Entry) {
				Map.Entry entry = (Map.Entry) obj;
				Object value = entry.getValue();
				if(value instanceof AbstractAttemptDescriptor) {
					AbstractAttemptDescriptor attempt = (AbstractAttemptDescriptor) value;
					attempt.fastForwardToLastStep();
				}
			}
		}
	}

}

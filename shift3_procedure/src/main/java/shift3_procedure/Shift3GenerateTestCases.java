/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_procedure;

import common.GenerateTestCases;
import shift3_procedure.models.Tableau;
import shift3_procedure.models.values.Shift3TestCase;
import shift3_procedure.test_cases.Shift3TestCaseDatabase;
import shift3_procedure.test_cases.execution_trace.Shift3ExecutionTrace;
import shift3_procedure.test_cases.execution_trace.Shift3ExecutionTraceFull;

public abstract class Shift3GenerateTestCases<STUDENT_MODEL extends Tableau> 

       extends        GenerateTestCases<Tableau, 
                                        STUDENT_MODEL,
                                        Shift3TestCase,
                                        Shift3TestCaseDatabase,
                                        Shift3ExecutionTrace> {

	
	@Override
	protected Class<Shift3TestCase> testCaseClass(){
		return Shift3TestCase.class;
	}

	@Override
	protected Class<Shift3TestCaseDatabase> testCaseDatabaseClass(){
		return Shift3TestCaseDatabase.class;
	}

	@Override
	protected Class<Tableau> executableModelClass() {
		return Tableau.class;
	}

	@Override
	protected boolean shouldWriteJson() {
		return false;
	}

	@Override
	protected Class<? extends Shift3ExecutionTrace> executionTraceClass() {
		return Shift3ExecutionTraceFull.class;
	}

}

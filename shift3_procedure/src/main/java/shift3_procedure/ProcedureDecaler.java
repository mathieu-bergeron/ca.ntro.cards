/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_procedure;

import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import common.models.enums.Attempt;
import common_procedure.ProcedureApp;
import common_procedure.messages.ProcedureMsgAcceptManualModel;
import common_procedure.test_cases.descriptor.ProcedureTestCaseDescriptor;
import shift3_procedure.backend.Shift3ProcedureBackend;
import shift3_procedure.frontend.Shift2ProcedureFrontend;
import shift3_procedure.frontend.Shift2ProcedureViewData;
import shift3_procedure.frontend.views.Shift3CardsView;
import shift3_procedure.frontend.views.Shift3ProcedureDashboardView;
import shift3_procedure.frontend.views.Shift3ProcedureMessagesView;
import shift3_procedure.frontend.views.Shift3ProcedureRootView;
import shift3_procedure.frontend.views.Shift3ProcedureSettingsView;
import shift3_procedure.frontend.views.fragments.Shift3ProcedureMessageFragment;
import shift3_procedure.messages.Shift3MsgAcceptManualModel;
import shift3_procedure.models.Shift3ProcedureDashboardModel;
import shift3_procedure.models.Shift3ProcedureSettingsModel;
import shift3_procedure.models.Tableau;
import shift3_procedure.models.values.CarteIncomplete;
import shift3_procedure.models.values.Shift3TestCase;
import shift3_procedure.test_cases.Shift3TestCaseDatabase;
import shift3_procedure.test_cases.descriptor.Shift3TestCaseDescriptor;
import shift3_procedure.test_cases.execution_trace.Shift3ExecutionTrace;

public abstract class   ProcedureDecaler<STUDENT_MODEL extends Tableau, 
                                         STUDENT_CARD extends CarteIncomplete>

                extends ProcedureApp<Tableau,           // executable model
                                     STUDENT_MODEL,
                                     STUDENT_MODEL,     // canvas model
                                     Shift3TestCase,
                                     Shift3TestCaseDescriptor,
                                     Shift3TestCaseDatabase,
                                     Shift3ExecutionTrace,
                                     Shift3ProcedureDashboardModel,
                                     Shift3ProcedureSettingsModel,
                                     Shift3MsgAcceptManualModel,
                                     Shift3ProcedureBackend<STUDENT_MODEL>,
                                     Shift3ProcedureRootView,
                                     Shift3CardsView,
                                     Shift3ProcedureDashboardView,
                                     Shift3ProcedureSettingsView,
                                     Shift3ProcedureMessagesView,
                                     Shift3ProcedureMessageFragment,
                                     Shift2ProcedureViewData,
                                     Shift2ProcedureFrontend<STUDENT_MODEL>> {

	
	@Override
	protected Class<Tableau> executableModelClass() {
		return Tableau.class;
	}

	protected abstract Class<STUDENT_MODEL> classeMonTableau();

	protected abstract Class<STUDENT_CARD> classeMaCarte();

	@Override
	protected Class<Shift3TestCase> testCaseClass() {
		return Shift3TestCase.class;
	}

	@Override
	protected Class<Shift3TestCaseDatabase> testCasesModelClass() {
		return Shift3TestCaseDatabase.class;
	}


	@Override
	protected Class<Shift3ProcedureDashboardModel> dashboardModelClass() {
		return Shift3ProcedureDashboardModel.class;
	}


	@Override
	protected Class<Shift3ProcedureSettingsModel> settingsModelClass() {
		return Shift3ProcedureSettingsModel.class;
	}

	@Override
	protected Shift2ProcedureFrontend createFrontend() {
		return new Shift2ProcedureFrontend();
	}


	@Override
	protected Shift3ProcedureBackend createBackend() {
		return new Shift3ProcedureBackend();
	}




	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Class<STUDENT_MODEL> canvasModelClass() {
		return (Class<STUDENT_MODEL>) classeMonTableau();
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return classeMonTableau();
	}

	@Override
	protected Class<Shift3TestCaseDescriptor> testCaseDescriptorClass() {
		return Shift3TestCaseDescriptor.class;
	}

	@Override
	protected Class<Shift3MsgAcceptManualModel> msgAcceptManualModelClass() {
		return Shift3MsgAcceptManualModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}

	@Override
	protected Attempt initialAttempt() {
		return Attempt.CODE;
	}

	@Override
	protected void registerAdditionnalModels(ModelRegistrar registrar) {
		super.registerAdditionnalModels(registrar);
		
		registrar.registerValue(classeMaCarte());
	}

}

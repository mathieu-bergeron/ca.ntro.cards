/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_procedure.models;

import ca.ntro.app.Ntro;
import common_procedure.models.ProcedureDashboardModel;
import shift3_procedure.frontend.views.Shift3ProcedureDashboardView;
import shift3_procedure.frontend.views.Shift3ReplayView;
import shift3_procedure.frontend.views.Shift3SelectionsView;
import shift3_procedure.frontend.views.fragments.Shift3TestCaseFragment;
import shift3_procedure.test_cases.Shift3TestCaseDatabase;
import shift3_procedure.test_cases.descriptor.Shift3TestCaseDescriptor;

public class Shift3ProcedureDashboardModel extends ProcedureDashboardModel<Shift3ProcedureDashboardView, 
                                                                         Tableau, 
                                                                         Shift3TestCaseDatabase,
                                                                         Shift3TestCaseDescriptor,
                                                                         Shift3ReplayView,
                                                                         Shift3SelectionsView,
                                                                         Shift3TestCaseFragment> {

	@Override
	public void addOrUpdateTestCase(Shift3TestCaseDescriptor testCaseDescriptor) {
		
		// XXX: always show last step
		testCaseDescriptor.fastForwardToLastStep();
		
		// XXX: simply overwrite existing test case, we do not memorize currentStep in .json
		testCaseDescriptor.setParentModel(this);
		

		byId.addTestCase(testCaseDescriptor);
		byCategory.addTestCase(testCaseDescriptor);
		
		version++;
	}

	@Override
	protected String defaultTestCaseId() {
		return "ex01";
	}


}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_procedure.models.values;

import common.models.enums.Sorte;
import common.models.values.cards.Carte;
import javafx.scene.canvas.GraphicsContext;
import shift3_procedure.models.world2d.Shift3ProcedureDrawingOptions;

public abstract class CarteIncomplete extends Carte<Shift3ProcedureDrawingOptions> {

	private static final long serialVersionUID = -7658957428600519658L;

	public CarteIncomplete() {
		super();
	}

	public CarteIncomplete(int numero, Sorte sorte) {
		super(numero,sorte);
	}
	
	
	@Override
	public void drawFaceUp(GraphicsContext gc, 
			               double topLeftX, 
			               double topLeftY, 
			               double width, 
			               double height, 
			               int levelOfDetails) {

		gc.save();

		gc.translate(topLeftX, topLeftY);
		
		dessinerCarte(gc);
		
		gc.restore();
	}
	
	protected abstract void dessinerCarte(GraphicsContext gc);
	

}

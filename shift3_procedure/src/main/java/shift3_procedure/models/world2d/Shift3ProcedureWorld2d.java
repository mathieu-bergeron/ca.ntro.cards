/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_procedure.models.world2d;

import java.util.ArrayList;
import java.util.List;

import ca.ntro.app.Ntro;
import ca.ntro.app.world2d.Object2dFx;
import shift3_procedure.Shift3Constants;
import shift3_procedure.messages.Shift3MsgAcceptManualModel;
import shift3_procedure.models.Tableau;
import common.CommonConstants;
import common.models.values.cards.AbstractCard;
import common.models.values.cards.Carte;
import common_procedure.messages.ProcedureMsgAcceptManualModel;
import common_procedure.models.world2d.ProcedureObject2d;
import common_procedure.models.world2d.ProcedureWorld2d;

public class Shift3ProcedureWorld2d extends ProcedureWorld2d {
    
    
    @Override
    public void buildAndSendManualModel() {
     	
    	List<Shift3Card2d> cards2d = new ArrayList<>();
		
		for(Object2dFx<?> object2d : getObjects()) {

			if(object2d instanceof Shift3Card2d) {

				cards2d.add((Shift3Card2d) object2d);

			}
		}
		
		cards2d.sort((obj01, obj02) -> {
			return Double.compare(obj01.getTopLeftX(), obj02.getTopLeftX());
		});
    	
    	
		//double markerTopLeftX = 10 + cardWidth + cardWidth / 2 + getIndicePlusPetit() * cardWidth * 3 / 2;
		double cardWidth = Shift3Constants.INITIAL_CARD_WIDTH_MILIMETERS;
		double cardHeight = Shift3Constants.INITIAL_CARD_HEIGHT_MILIMETERS;

		Object2dFx<?> marker2d = objectById("i");
		int indexOfI = (int) Math.round((marker2d.getTopLeftX() - 10 - cardWidth - cardWidth / 2) * 2 / 3 / cardWidth);
		
		marker2d = objectById("toRemove");
		int indexOfToRemove = (int) Math.round((marker2d.getTopLeftX() - 10 - cardWidth - cardWidth / 2) * 2 / 3 / cardWidth);
        
        Tableau manualModel = new Tableau();
        
        //List<Carte> cards = new ArrayList<>();
        Carte[] cards = new Carte[cards2d.size()];
        for(int i = 0; i < cards2d.size(); i++) {
        	AbstractCard card = cards2d.get(i).getCard();
        	
        	if(card instanceof Carte) {
        	
        		//cards.add((Carte) card);
        		cards[i] = (Carte) card;
        		
        	}else {
        		
        		//cards.add(null);
        		cards[i] = null;
        	}      	
        }
        
        manualModel.setI(indexOfI);
        manualModel.setADeplacer(indexOfToRemove);
        manualModel.setCartes(cards);

        Ntro.newMessage(Shift3MsgAcceptManualModel.class)
            .setManualModel(manualModel)
            .send();
    }

}

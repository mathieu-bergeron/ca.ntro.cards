/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_procedure.frontend;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import shift3_procedure.frontend.views.Shift3CardsView;
import shift3_procedure.frontend.views.Shift3ProcedureDashboardView;
import shift3_procedure.frontend.views.Shift3ProcedureMessagesView;
import shift3_procedure.frontend.views.Shift3ProcedureRootView;
import shift3_procedure.frontend.views.Shift3ProcedureSettingsView;
import shift3_procedure.frontend.views.Shift3ReplayView;
import shift3_procedure.frontend.views.Shift3SelectionsView;
import shift3_procedure.frontend.views.Shift3VariablesView;
import shift3_procedure.frontend.views.fragments.Shift3ProcedureMessageFragment;
import shift3_procedure.frontend.views.fragments.Shift3TestCaseFragment;
import shift3_procedure.models.Shift3ProcedureDashboardModel;
import shift3_procedure.models.Shift3ProcedureSettingsModel;
import shift3_procedure.models.Tableau;
import common.messages.MsgMessageToUser;
import common_procedure.frontend.ProcedureFrontend;

public class Shift2ProcedureFrontend<STUDENT_MODEL extends Tableau>

       extends ProcedureFrontend<Shift3ProcedureRootView,
                                 Shift3ProcedureSettingsView, 
                                 Shift3CardsView, 
                                 Shift3ProcedureDashboardView, 
                                 Shift3SelectionsView,
                                 Shift3TestCaseFragment,
                                 Shift3ReplayView,
                                 Shift3VariablesView,
                                 Shift3ProcedureMessagesView,
                                 Shift3ProcedureMessageFragment,
                                 Shift2ProcedureViewData, 
                                 STUDENT_MODEL, // CanvasModel
                                 Shift3ProcedureDashboardModel, 
                                 Shift3ProcedureSettingsModel> {


	@Override
	protected boolean isProd() {
		return false;
	}

	@Override
	protected Class<Shift3ProcedureRootView> rootViewClass() {
		return Shift3ProcedureRootView.class;
	}

	@Override
	protected Class<Shift3ProcedureSettingsView> settingsViewClass() {
		return Shift3ProcedureSettingsView.class;
	}

	@Override
	protected Class<Shift3CardsView> canvasViewClass() {
		return Shift3CardsView.class;
	}

	@Override
	protected Class<Shift3ProcedureDashboardView> dashboardViewClass() {
		return Shift3ProcedureDashboardView.class;
	}


	@Override
	protected Class<Shift2ProcedureViewData> viewDataClass() {
		return Shift2ProcedureViewData.class;
	}



	@Override
	protected Class<Shift3SelectionsView> selectionsViewClass() {
		return Shift3SelectionsView.class;
	}

	@Override
	protected Class<Shift3ReplayView> replayControlsViewClass() {
		return Shift3ReplayView.class;
	}

	@Override
	protected Class<Shift3VariablesView> variablesViewClass() {
		return Shift3VariablesView.class;
	}


	@Override
	protected Class<Shift3TestCaseFragment> testCaseFragmentClass() {
		return Shift3TestCaseFragment.class;
	}

	@Override
	protected Class<Shift3ProcedureMessagesView> messagesViewClass() {
		return Shift3ProcedureMessagesView.class;
	}

	@Override
	protected Class<Shift3ProcedureMessageFragment> messageFragmentClass() {
		return Shift3ProcedureMessageFragment.class;
	}


}

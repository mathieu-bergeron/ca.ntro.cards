/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_procedure.frontend;

import common.models.values.cards.AbstractCard;
import common_procedure.frontend.ProcedureViewData;
import common_procedure.models.world2d.ProcedureCard2d;
import shift3_procedure.models.world2d.Shift3Card2d;
import shift3_procedure.models.world2d.Shift3ProcedureDrawingOptions;
import shift3_procedure.models.world2d.Shift3ProcedureWorld2d;

public class Shift2ProcedureViewData extends ProcedureViewData<Shift3ProcedureWorld2d, Shift3ProcedureDrawingOptions> {

	@Override
	protected Shift3ProcedureWorld2d newWorld2d() {
		return new Shift3ProcedureWorld2d();
	}

	@Override
	protected ProcedureCard2d newCard2d(String card2dId, AbstractCard card) {
		return new Shift3Card2d(card2dId, card);
	}

	@Override
	protected Shift3ProcedureDrawingOptions defaultDrawingOptions() {
		return new Shift3ProcedureDrawingOptions() {
			@Override
			public boolean useFourCardColors() {
				return true;
			}
		};
	}


}

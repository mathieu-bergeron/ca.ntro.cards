/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package freesort_efficiency.backend;

import ca.ntro.app.tasks.backend.BackendTasks;
import common_efficiency.backend.EfficiencyBackend;
import common_procedure.test_cases.execution_trace.ProcedureExecutionTrace;
import freesort_efficiency.models.FreesortEfficiencyDashboardModel;
import freesort_efficiency.models.FreesortEfficiencySettingsModel;
import freesort_efficiency.models.FreesortGraphsModel;
import freesort_procedure.models.TriLibre;
import freesort_procedure.models.values.FreesortTestCase;
import freesort_procedure.test_cases.FreesortTestCaseDatabase;
import freesort_procedure.test_cases.execution_trace.FreesortExecutionTrace;

public class FreesortEfficiencyBackend<STUDENT_MODEL extends TriLibre>

       extends EfficiencyBackend<TriLibre, 
                                 STUDENT_MODEL,
                                 FreesortGraphsModel,            // CanvasModel
                                 FreesortTestCase,
                                 FreesortTestCaseDatabase,
                                 FreesortExecutionTrace,
                                 FreesortEfficiencyDashboardModel,
                                 FreesortEfficiencySettingsModel> {



	@Override
	protected void addSubTasksToModifyCanvasModel(BackendTasks subTasks) {
		
	}

	@Override
	protected void addSubTasksToModifyDashboardModel(BackendTasks subTasks) {
		
	}

	@Override
	protected void addSubTasksToModifySettingsModel(BackendTasks subTasks) {
		
	}


	@Override
	protected void createAdditionalTasks(BackendTasks tasks) {
		
	}

}

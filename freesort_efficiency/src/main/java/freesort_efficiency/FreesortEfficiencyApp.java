/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package freesort_efficiency;


import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import common_efficiency.EfficiencyApp;
import freesort_efficiency.backend.FreesortEfficiencyBackend;
import freesort_efficiency.frontend.FreesortEfficiencyFrontend;
import freesort_efficiency.frontend.FreesortEfficiencyViewData;
import freesort_efficiency.frontend.views.FreesortEfficiencyDashboardView;
import freesort_efficiency.frontend.views.FreesortEfficiencyMessagesView;
import freesort_efficiency.frontend.views.FreesortEfficiencyRootView;
import freesort_efficiency.frontend.views.FreesortEfficiencySettingsView;
import freesort_efficiency.frontend.views.FreesortGraphsView;
import freesort_efficiency.frontend.views.fragments.FreesortEfficiencyMessageFragment;
import freesort_efficiency.models.FreesortEfficiencyDashboardModel;
import freesort_efficiency.models.FreesortEfficiencySettingsModel;
import freesort_efficiency.models.FreesortGraphsModel;
import freesort_procedure.models.TriLibre;
import freesort_procedure.models.values.FreesortTestCase;
import freesort_procedure.test_cases.FreesortTestCaseDatabase;
import freesort_procedure.test_cases.execution_trace.FreesortExecutionTrace;

public abstract class   FreesortEfficiencyApp<STUDENT_MODEL extends TriLibre>

                extends EfficiencyApp<TriLibre, 
                                      STUDENT_MODEL,
                                      FreesortGraphsModel,
                                      FreesortTestCase,
                                      FreesortTestCaseDatabase,
                                      FreesortExecutionTrace,
                                      FreesortEfficiencyDashboardModel,
                                      FreesortEfficiencySettingsModel,
                                      FreesortEfficiencyBackend<STUDENT_MODEL>,
                                      FreesortEfficiencyRootView,
                                      FreesortGraphsView,
                                      FreesortEfficiencyDashboardView,
                                      FreesortEfficiencySettingsView,
                                      FreesortEfficiencyMessagesView,
                                      FreesortEfficiencyMessageFragment,
                                      FreesortEfficiencyViewData,
                                      FreesortEfficiencyFrontend> {

	@Override
	protected FreesortEfficiencyFrontend createFrontend() {
		return new FreesortEfficiencyFrontend();
	}

	@Override
	protected FreesortEfficiencyBackend createBackend() {
		return new FreesortEfficiencyBackend();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class<TriLibre> executableModelClass() {
		return TriLibre.class;
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return classeTriNaif();
	}
	
	protected abstract Class<STUDENT_MODEL> classeTriNaif();

	@Override
	protected Class<FreesortGraphsModel> canvasModelClass() {
		return FreesortGraphsModel.class;
	}

	@Override
	protected Class<FreesortTestCase> testCaseClass() {
		return FreesortTestCase.class;
	}

	@Override
	protected Class<FreesortTestCaseDatabase> testCasesModelClass() {
		return FreesortTestCaseDatabase.class;
	}

	@Override
	protected Class<FreesortEfficiencyDashboardModel> dashboardModelClass() {
		return FreesortEfficiencyDashboardModel.class;
	}

	@Override
	protected Class<FreesortEfficiencySettingsModel> settingsModelClass() {
		return FreesortEfficiencySettingsModel.class;
	}

	@Override
	protected void registerAdditionnalModels(ModelRegistrar registrar) {
		
	}

	@Override
	protected void registerAdditionnalMessages(MessageRegistrar registrar) {
		
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}
}

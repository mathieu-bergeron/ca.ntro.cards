/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_efficiency;

import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import common.CommonApp;
import common.models.enums.Attempt;
import common.test_cases.CommonTestCase;
import common.test_cases.CommonTestCaseDatabase;
import common_efficiency.backend.EfficiencyBackend;
import common_efficiency.frontend.EfficiencyFrontend;
import common_efficiency.frontend.EfficiencyViewData;
import common_efficiency.frontend.views.EfficiencyDashboardView;
import common_efficiency.frontend.views.EfficiencyGraphsView;
import common_efficiency.frontend.views.EfficiencyMessagesView;
import common_efficiency.frontend.views.EfficiencyRootView;
import common_efficiency.frontend.views.EfficiencySettingsView;
import common_efficiency.frontend.views.fragments.EfficiencyMessageFragment;
import common_efficiency.models.EfficiencyDashboardModel;
import common_efficiency.models.EfficiencyGraphsModel;
import common_efficiency.models.EfficiencySettingsModel;
import common_efficiency.test_cases.execution_trace.EfficiencyExecutionTraceSizeOnly;
import common_procedure.models.ProcedureCardsModel;
import common_procedure.test_cases.execution_trace.ProcedureExecutionTrace;

public abstract class EfficiencyApp<EXECUTABLE_MODEL extends ProcedureCardsModel,
                                    STUDENT_MODEL    extends EXECUTABLE_MODEL,
                                    CANVAS_MODEL     extends EfficiencyGraphsModel,
                                    TEST_CASE        extends CommonTestCase,
                                    TEST_CASES_MODEL extends CommonTestCaseDatabase,
                                    EXECUTION_TRACE  extends ProcedureExecutionTrace,
                                    DASHBOARD_MODEL  extends EfficiencyDashboardModel,
                                    SETTINGS_MODEL   extends EfficiencySettingsModel,
                                                                                                      
                                    BACKEND extends EfficiencyBackend<EXECUTABLE_MODEL,
                                                                      STUDENT_MODEL,
                                                                      CANVAS_MODEL, 
                                                                      TEST_CASE, 
                                                                      TEST_CASES_MODEL, 
                                                                      EXECUTION_TRACE,
                                                                      DASHBOARD_MODEL, 
                                                                      SETTINGS_MODEL>,
                                   
                                    ROOT_VIEW        extends EfficiencyRootView, 
                                    CARDS_VIEW       extends EfficiencyGraphsView, 
                                    DASHBOARD_VIEW   extends EfficiencyDashboardView,
                                    SETTINGS_VIEW    extends EfficiencySettingsView,
                                    MESSAGES_VIEW    extends EfficiencyMessagesView,
                                    MESSAGE_FRAGMENT extends EfficiencyMessageFragment,
                                    CARDS_VIEW_DATA  extends EfficiencyViewData,
                                     
                                    FRONTEND extends EfficiencyFrontend<ROOT_VIEW, 
                                                                        SETTINGS_VIEW, 
                                                                        CARDS_VIEW, 
                                                                        DASHBOARD_VIEW, 
                                                                        MESSAGES_VIEW,
                                                                        MESSAGE_FRAGMENT,
                                                                        CARDS_VIEW_DATA,
                                                                        CANVAS_MODEL,
                                                                        DASHBOARD_MODEL,
                                                                        SETTINGS_MODEL>>

               extends CommonApp<EXECUTABLE_MODEL,
                                 STUDENT_MODEL,
                                 CANVAS_MODEL,
                                 TEST_CASE,
                                 TEST_CASES_MODEL,
                                 EXECUTION_TRACE,
                                 DASHBOARD_MODEL,
                                 SETTINGS_MODEL,
                                 BACKEND,
                                 ROOT_VIEW,
                                 CARDS_VIEW,
                                 DASHBOARD_VIEW,
                                 SETTINGS_VIEW,
                                 MESSAGES_VIEW,
                                 MESSAGE_FRAGMENT,
                                 CARDS_VIEW_DATA,
                                 FRONTEND> {

	@Override
	protected Class<? extends EXECUTION_TRACE> executionTraceClass() {
		return (Class<? extends EXECUTION_TRACE>) EfficiencyExecutionTraceSizeOnly.class;
	}

    @Override
	protected void additionnalBackendInitialization(BACKEND backend) {
    }

	@Override
	protected void registerAdditionnalModels(ModelRegistrar registrar) {
		
	}

	@Override
	protected void registerAdditionnalMessages(MessageRegistrar registrar) {
		
	}

	@Override
	protected Attempt initialAttempt() {
		return Attempt.SOLUTION;
	}

}

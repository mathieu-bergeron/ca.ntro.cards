/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_efficiency.frontend;

import ca.ntro.app.events.EventRegistrar;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import common.frontend.CommonFrontend;
import common.frontend.CommonSession;
import common_efficiency.frontend.views.EfficiencyDashboardView;
import common_efficiency.frontend.views.EfficiencyGraphsView;
import common_efficiency.frontend.views.EfficiencyMessagesView;
import common_efficiency.frontend.views.EfficiencyRootView;
import common_efficiency.frontend.views.EfficiencySettingsView;
import common_efficiency.frontend.views.fragments.EfficiencyMessageFragment;
import common_efficiency.models.EfficiencyDashboardModel;
import common_efficiency.models.EfficiencyGraphsModel;
import common_efficiency.models.EfficiencySettingsModel;

public abstract class EfficiencyFrontend<ROOT_VIEW       extends EfficiencyRootView, 
                                         SETTINGS_VIEW    extends EfficiencySettingsView,
                                         CANVAS_VIEW      extends EfficiencyGraphsView, 
                                         DASHBOARD_VIEW   extends EfficiencyDashboardView,
                                         MESSAGES_VIEW    extends EfficiencyMessagesView,
                                         MESSAGE_FRAGMENT extends EfficiencyMessageFragment,
                                         VIEW_DATA        extends EfficiencyViewData,
                                         GRAPHS_MODEL     extends EfficiencyGraphsModel,
                                         DASHBOARD_MODEL  extends EfficiencyDashboardModel,
                                         SETTINGS_MODEL   extends EfficiencySettingsModel>

       extends CommonFrontend<ROOT_VIEW, 
                              SETTINGS_VIEW, 
                              CANVAS_VIEW, 
                              DASHBOARD_VIEW,
                              MESSAGES_VIEW,
                              MESSAGE_FRAGMENT,
                              VIEW_DATA,
                              GRAPHS_MODEL,
                              DASHBOARD_MODEL,
                              SETTINGS_MODEL> {

	@Override
	protected Class<? extends CommonSession> sessionClass() {
		return CommonSession.class;
	}

	@Override
	protected void initializeViewData(VIEW_DATA viewData) {
	}

	@Override
	protected void addDashboardSubViewLoaders(FrontendTasks subTasks) {
	}

	@Override
	protected void addSubTasksToInitialization(FrontendTasks subTasks) {
		
	}

	@Override
	protected void addSubTasksToViewData(FrontendTasks subTasks) {
		
	}


	@Override
	protected void addSubTasksToNavigation(FrontendTasks subTasks) {
		
	}

	@Override
	protected void addSubTasksToSettings(FrontendTasks subTasks) {
		
	}

	@Override
	protected void addSubTasksToDashboard(FrontendTasks subTasks) {
		
	}

	@Override
	protected void addSubTasksToMessages(FrontendTasks subTasks) {

	}

	@Override
	protected void createAdditionnalTasks(FrontendTasks tasks) {
		
	}

	@Override
	protected void registerAdditionnalEvents(EventRegistrar registrar) {

	}

	@Override
	protected void registerAdditionnalViews(ViewRegistrarFx registrar) {
		
	}

	@Override
	protected void installDashboardSubViews(SimpleTaskCreator<?> taskCreator) {
		taskCreator.executes(inputs -> {});
	}

	@Override
	protected String cssProdPath() {
		return "/efficiency/prod.css";
	}

	@Override
	protected String cssDevPath() {
		return "/efficiency/dev.css";
	}

	@Override
	protected String rootViewXmlPath() {
		return "/efficiency/root.xml";
	}

	@Override
	protected String settingsViewXmlPath() {
		return "/efficiency/settings.xml";
		
	}

	@Override
	protected String canvasViewXmlPath() {
		return "/efficiency/canvas.xml";
	}

	@Override
	protected String dashboardViewXmlPath() {
		return "/efficiency/dashboard.xml";
		
	}

	@Override
	protected String messagesViewXmlPath() {
		return "/efficiency/messages.xml";
	}

	@Override
	protected String messageFragmentXmlPath() {
		return "/fragments/message.xml";
	}


	@Override
	protected String stringsFrPath() {
		return "/efficiency/strings_fr.properties";
	}

	@Override
	protected String stringsEnPath() {
		return "/efficiency/strings_en.properties";
	}

}

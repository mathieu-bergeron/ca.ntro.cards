/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift_efficiency.frontend;

import shift_efficiency.frontend.views.ShiftEfficiencyDashboardView;
import shift_efficiency.frontend.views.ShiftEfficiencyMessagesView;
import shift_efficiency.frontend.views.ShiftEfficiencyRootView;
import shift_efficiency.frontend.views.ShiftEfficiencySettingsView;
import shift_efficiency.frontend.views.ShiftGraphsView;
import shift_efficiency.frontend.views.fragments.ShiftEfficiencyMessageFragment;
import shift_efficiency.models.ShiftEfficiencyDashboardModel;
import shift_efficiency.models.ShiftEfficiencySettingsModel;
import shift_efficiency.models.ShiftGraphsModel;
import ca.ntro.app.Ntro;
import common.messages.MsgMessageToUser;
import common_efficiency.frontend.EfficiencyFrontend;

public class ShiftEfficiencyFrontend 

       extends EfficiencyFrontend<ShiftEfficiencyRootView,
                                  ShiftEfficiencySettingsView, 
                                  ShiftGraphsView, 
                                  ShiftEfficiencyDashboardView, 
                                  ShiftEfficiencyMessagesView,
                                  ShiftEfficiencyMessageFragment,
                                  ShiftEfficiencyViewData, 
                                  ShiftGraphsModel, 
                                  ShiftEfficiencyDashboardModel, 
                                  ShiftEfficiencySettingsModel> {

	@Override
	protected boolean isProd() {
		return true;
	}

	@Override
	protected Class<ShiftEfficiencyRootView> rootViewClass() {
		return ShiftEfficiencyRootView.class;
	}

	@Override
	protected Class<ShiftEfficiencySettingsView> settingsViewClass() {
		return ShiftEfficiencySettingsView.class;
	}

	@Override
	protected Class<ShiftGraphsView> canvasViewClass() {
		return ShiftGraphsView.class;
	}

	@Override
	protected Class<ShiftEfficiencyDashboardView> dashboardViewClass() {
		return ShiftEfficiencyDashboardView.class;
	}


	@Override
	protected Class<ShiftEfficiencyViewData> viewDataClass() {
		return ShiftEfficiencyViewData.class;
	}


	@Override
	protected Class<ShiftEfficiencyMessagesView> messagesViewClass() {
		return ShiftEfficiencyMessagesView.class;
	}

	@Override
	protected Class<ShiftEfficiencyMessageFragment> messageFragmentClass() {
		return ShiftEfficiencyMessageFragment.class;
	}

}

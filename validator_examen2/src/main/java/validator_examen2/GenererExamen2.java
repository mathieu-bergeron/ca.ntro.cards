/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_examen2;

import ca.ntro.app.models.ModelRegistrar;
import ca.ntro.cards.validator.GeneratorApp;
import ca.ntro.cards.validator.backend.GeneratorBackend;
import validator_examen2.backend.Examen2GeneratorBackend;
import validator_examen2.models.Examen2Model;
import validator_examen2.models.gr1.Bovin;
import validator_examen2.models.gr1.Loup;
import validator_examen2.models.gr1.Meute;
import validator_examen2.models.gr1.Troupeau;

public abstract class GenererExamen2 extends GeneratorApp<Examen2Model> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class<? extends GeneratorBackend> backendClass() {
		return Examen2GeneratorBackend.class;
	}

	@Override
	protected void registerAdditonalModels(ModelRegistrar registrar) {
		registrar.registerValue(Loup.class);
		registrar.registerValue(Bovin.class);
		registrar.registerValue(Troupeau.class);
		registrar.registerValue(Meute.class);
	}


}

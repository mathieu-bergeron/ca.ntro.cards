/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_examen2.models.gr2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ntro.ntro_core_impl.reflection.object_graph.Initialize;
import validator_examen2.models.Examen2Model;

public abstract class Examen2Gr2 extends Examen2Model<Examen2Gr2> implements Initialize {

	private static final long serialVersionUID = 8295201566647831760L;

	protected Map<String, Object> vehicules = new HashMap<>();
	protected List<Object> groupes = new ArrayList<>();

	public Map<String, Object> getVehicules() {
		return vehicules;
	}

	public void setVehicules(Map<String, Object> vehicules) {
		this.vehicules = vehicules;
	}

	public List<Object> getGroupes() {
		return groupes;
	}

	public void setGroupes(List<Object> groupes) {
		this.groupes = groupes;
	}

	@Override
	public void copyDataFrom(Examen2Gr2 other) {
		this.vehicules = other.vehicules;
		this.groupes = other.groupes;
	}

}

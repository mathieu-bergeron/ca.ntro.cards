/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_examen2.models.theorie3_1_gr1;

import java.util.ArrayList;
import java.util.List;

public class MonEquipe implements Equipe {
	
	private List<Object> athletes = new ArrayList<>();

	@Override
	public void ajouterAthlete(Athlete athelete) {
		this.athletes.add(athelete);
	}

	@Override
	public Athlete plusUtile() {
		Athlete plusUtile = null;
		
		if(athletes.size() > 0) {
			plusUtile = (Athlete) athletes.get(0);
		}
		
		for(int i = 0; i < athletes.size(); i++) {
			Athlete courant = (Athlete) athletes.get(i);

			// TODO: comparer plusUtile au courant
			//       pour voir le courant est plusUtile
			
		}
		
		return plusUtile;
	}

}

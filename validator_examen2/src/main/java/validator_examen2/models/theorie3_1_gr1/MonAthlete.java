/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_examen2.models.theorie3_1_gr1;

import validator_examen2.models.Examen2Model;

public class MonAthlete extends Examen2Model<MonAthlete> implements Athlete {

	private String nom;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public MonAthlete(String nom) {
		setNom(nom);
	}

	@Override
	public String nom() {
		return getNom();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	private static final long serialVersionUID = -8499162620214503441L;

	@Override
	public void copyDataFrom(MonAthlete other) {
		this.nom = other.nom;
	}

	@Override
	public void initialize(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean shouldInitializeBeforeValidation() {
		// TODO Auto-generated method stub
		return false;
	}

}

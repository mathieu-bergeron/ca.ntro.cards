/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_examen2.models.gr1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ntro.ntro_core_impl.reflection.object_graph.Initialize;
import validator_examen2.models.Examen2Model;

public abstract class Examen2Gr1 extends Examen2Model<Examen2Gr1> implements Initialize {

	private static final long serialVersionUID = 8295201566647831760L;

	protected Map<String, Object> animaux = new HashMap<>();
	protected List<Object> groupes = new ArrayList<>();

	public List<Object> getGroupes() {
		return groupes;
	}

	public void setGroupes(List<Object> groupes) {
		this.groupes = groupes;
	}

	public Map<String, Object> getAnimaux() {
		return animaux;
	}

	public void setAnimaux(Map<String, Object> animaux) {
		this.animaux = animaux;
	}

	@Override
	public void copyDataFrom(Examen2Gr1 other) {
		this.animaux = other.animaux;
		this.groupes = other.groupes;
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package foo_efficiency.frontend;

import foo_efficiency.frontend.views.FooEfficiencyDashboardView;
import foo_efficiency.frontend.views.FooEfficiencyMessagesView;
import foo_efficiency.frontend.views.FooEfficiencyRootView;
import foo_efficiency.frontend.views.FooEfficiencySettingsView;
import foo_efficiency.frontend.views.FooGraphsView;
import foo_efficiency.frontend.views.fragments.FooEfficiencyMessageFragment;
import foo_efficiency.models.FooEfficiencyDashboardModel;
import foo_efficiency.models.FooEfficiencySettingsModel;
import foo_efficiency.models.FooGraphsModel;
import ca.ntro.app.Ntro;
import common.messages.MsgMessageToUser;
import common_efficiency.frontend.EfficiencyFrontend;

public class FooEfficiencyFrontend 

       extends EfficiencyFrontend<FooEfficiencyRootView,
                                  FooEfficiencySettingsView, 
                                  FooGraphsView, 
                                  FooEfficiencyDashboardView, 
                                  FooEfficiencyMessagesView,
                                  FooEfficiencyMessageFragment,
                                  FooEfficiencyViewData, 
                                  FooGraphsModel, 
                                  FooEfficiencyDashboardModel, 
                                  FooEfficiencySettingsModel> {

	@Override
	protected boolean isProd() {
		return true;
	}

	@Override
	protected Class<FooEfficiencyRootView> rootViewClass() {
		return FooEfficiencyRootView.class;
	}

	@Override
	protected Class<FooEfficiencySettingsView> settingsViewClass() {
		return FooEfficiencySettingsView.class;
	}

	@Override
	protected Class<FooGraphsView> canvasViewClass() {
		return FooGraphsView.class;
	}

	@Override
	protected Class<FooEfficiencyDashboardView> dashboardViewClass() {
		return FooEfficiencyDashboardView.class;
	}


	@Override
	protected Class<FooEfficiencyViewData> viewDataClass() {
		return FooEfficiencyViewData.class;
	}


	@Override
	protected Class<FooEfficiencyMessagesView> messagesViewClass() {
		return FooEfficiencyMessagesView.class;
	}

	@Override
	protected Class<FooEfficiencyMessageFragment> messageFragmentClass() {
		return FooEfficiencyMessageFragment.class;
	}

}

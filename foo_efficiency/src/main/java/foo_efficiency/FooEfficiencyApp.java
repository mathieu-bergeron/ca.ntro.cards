/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package foo_efficiency;

import foo_efficiency.backend.FooEfficiencyBackend;
import foo_efficiency.frontend.FooEfficiencyFrontend;
import foo_efficiency.frontend.FooEfficiencyViewData;
import foo_efficiency.frontend.views.FooEfficiencyDashboardView;
import foo_efficiency.frontend.views.FooEfficiencyMessagesView;
import foo_efficiency.frontend.views.FooEfficiencyRootView;
import foo_efficiency.frontend.views.FooEfficiencySettingsView;
import foo_efficiency.frontend.views.FooGraphsView;
import foo_efficiency.frontend.views.fragments.FooEfficiencyMessageFragment;
import foo_efficiency.models.FooEfficiencyDashboardModel;
import foo_efficiency.models.FooEfficiencySettingsModel;
import foo_efficiency.models.FooGraphsModel;
import foo_procedure.models.FooCardsModel;
import foo_procedure.models.values.FooTestCase;
import foo_procedure.test_cases.FooTestCaseDatabase;
import foo_procedure.test_cases.execution_trace.FooExecutionTrace;
import common.models.enums.Attempt;
import common_efficiency.EfficiencyApp;

public abstract class   FooEfficiencyApp<STUDENT_MODEL extends FooCardsModel>

                extends EfficiencyApp<FooCardsModel, 
                                      STUDENT_MODEL,
                                      FooGraphsModel,
                                      FooTestCase,
                                      FooTestCaseDatabase,
                                      FooExecutionTrace,
                                      FooEfficiencyDashboardModel,
                                      FooEfficiencySettingsModel,
                                      FooEfficiencyBackend<STUDENT_MODEL>,
                                      FooEfficiencyRootView,
                                      FooGraphsView,
                                      FooEfficiencyDashboardView,
                                      FooEfficiencySettingsView,
                                      FooEfficiencyMessagesView,
                                      FooEfficiencyMessageFragment,
                                      FooEfficiencyViewData,
                                      FooEfficiencyFrontend> {

	@Override
	protected FooEfficiencyFrontend createFrontend() {
		return new FooEfficiencyFrontend();
	}

	@Override
	protected FooEfficiencyBackend createBackend() {
		return new FooEfficiencyBackend();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class<FooCardsModel> executableModelClass() {
		return FooCardsModel.class;
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return studentClass();
	}
	
	// TODO: renommer
	protected abstract Class<STUDENT_MODEL> studentClass();

	@Override
	protected Class<FooGraphsModel> canvasModelClass() {
		return FooGraphsModel.class;
	}

	@Override
	protected Class<FooTestCase> testCaseClass() {
		return FooTestCase.class;
	}

	@Override
	protected Class<FooTestCaseDatabase> testCasesModelClass() {
		return FooTestCaseDatabase.class;
	}

	@Override
	protected Class<FooEfficiencyDashboardModel> dashboardModelClass() {
		return FooEfficiencyDashboardModel.class;
	}

	@Override
	protected Class<FooEfficiencySettingsModel> settingsModelClass() {
		return FooEfficiencySettingsModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}

	@Override
	protected Attempt initialAttempt() {
		return Attempt.SOLUTION;
	}
}

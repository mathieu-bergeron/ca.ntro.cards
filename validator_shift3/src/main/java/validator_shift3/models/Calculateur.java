/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_shift3.models;

import ca.ntro.ntro_core_impl.reflection.object_graph.Initialize;

public class Calculateur extends Shift3Model<Calculateur> implements Initialize {
	private static final long serialVersionUID = -8785951526269181120L;

	private int n;
	private Factoriel tete;
	
	public int getN() {
		return n;
	}
	public void setN(int n) {
		this.n = n;
	}
	public Factoriel getTete() {
		return tete;
	}
	public void setTete(Factoriel tete) {
		this.tete = tete;
	}

	@Override
	public void copyDataFrom(Calculateur other) {
		this.tete = other.tete;
		this.n = other.n;
	}

	@Override
	public void initialize() {
		n = 4;
		tete = new Factoriel();
		tete.setN(n);
		
		tete.calculerFactoriel();
		
	}
	@Override
	public void initialize(String id) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean shouldInitializeBeforeValidation() {
		// TODO Auto-generated method stub
		return false;
	}
}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_shift3.models;

import java.util.ArrayList;
import java.util.List;

import ca.ntro.app.models.Model;
import ca.ntro.cards.validator.models.ValidatorModel;

public class Film extends Shift3Model<Film> {

	private static final long serialVersionUID = 3710180538800463658L;

	private String titre;

	private List<Personnage> personnages = new ArrayList<>();

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public List<Personnage> getPersonnages() {
		return personnages;
	}

	public void setPersonnages(List<Personnage> personnages) {
		this.personnages = personnages;
		//this.personnages = new ArrayList<>(personnages);
	}

	@Override
	public void copyDataFrom(Film other) {
		this.titre = other.titre;
		this.personnages = other.personnages;
	}

	@Override
	public void initialize(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean shouldInitializeBeforeValidation() {
		// TODO Auto-generated method stub
		return false;
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_shift3.models;

import java.io.Serializable;

import ca.ntro.app.models.ModelValue;


public class Factoriel implements ModelValue, Serializable {

	private static final long serialVersionUID = -6634474788240839519L;

	private int n;
	private long reponse;
	private Factoriel moinsUn;

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public long getReponse() {
		return reponse;
	}

	public void setReponse(long reponse) {
		this.reponse = reponse;
	}

	public Factoriel getMoinsUn() {
		return moinsUn;
	}

	public void setMoinsUn(Factoriel moinsUn) {
		this.moinsUn = moinsUn;
	}

	public void calculerFactoriel() {
		if(n == 0) {

			reponse = 1;

		}else {
			
			moinsUn = new Factoriel();
			moinsUn.setN(n-1);
			moinsUn.calculerFactoriel();
			
			reponse = n * moinsUn.reponse;
		}
		
	}
}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package validator_shift3;

import ca.ntro.app.models.ModelRegistrar;
import ca.ntro.cards.validator.GeneratorApp;
import ca.ntro.cards.validator.backend.GeneratorBackend;
import validator_shift3.backend.Shift2GeneratorBackend;
import validator_shift3.models.Carte;
import validator_shift3.models.Factoriel;
import validator_shift3.models.GardeRobe;
import validator_shift3.models.MaCarte;
import validator_shift3.models.Personnage;
import validator_shift3.models.Shift3Model;

public abstract class GenererShift2 extends GeneratorApp<Shift3Model> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class<? extends GeneratorBackend> backendClass() {
		return Shift2GeneratorBackend.class;
	}

	@Override
	protected void registerAdditonalModels(ModelRegistrar registrar) {
		registrar.registerValue(Carte.class);
		registrar.registerValue(MaCarte.class);
		registrar.registerValue(Personnage.class);
		registrar.registerValue(GardeRobe.class);
		registrar.registerValue(Factoriel.class);
	}


}

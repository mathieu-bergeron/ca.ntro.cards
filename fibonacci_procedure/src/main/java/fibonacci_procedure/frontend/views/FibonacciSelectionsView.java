/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package fibonacci_procedure.frontend.views;

import java.net.URL;
import java.util.ResourceBundle;

import ca.ntro.app.Ntro;
import common_procedure.frontend.views.ProcedureSelectionsView;
import fibonacci_procedure.frontend.views.fragments.FibonacciTestCaseFragment;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;

public class FibonacciSelectionsView extends ProcedureSelectionsView<FibonacciTestCaseFragment> {

	@FXML
	private Pane testCaseContainer;
	
	@FXML 
	private Pane idContainer;

	@FXML 
	private Pane sizeContainer;

	@FXML 
	private Pane manualContainer;

	@FXML 
	private Pane codeContainer;

	@FXML 
	private Pane solutionContainer;
	
	@Override
	public void initialize() {
		
		Ntro.assertNotNull(testCaseContainer);

		Ntro.assertNotNull(idContainer);
		Ntro.assertNotNull(sizeContainer);
		//Ntro.assertNotNull("manualContainer", manualContainer);
		Ntro.assertNotNull(codeContainer);
		Ntro.assertNotNull(solutionContainer);
		Ntro.assertNotNull(testCaseContainer);
		
		super.initialize();
	}

	@Override
	protected Pane testCaseContainer() {
		return testCaseContainer;
	}

	@Override
	protected Pane idContainer() {
		return idContainer;
	}

	@Override
	protected Pane sizeContainer() {
		return sizeContainer;
	}

	@Override
	protected Pane manualContainer() {
		return manualContainer;
	}

	@Override
	protected Pane codeContainer() {
		return codeContainer;
	}

	@Override
	protected Pane solutionContainer() {
		return solutionContainer;
	}
}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.frontend.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewFx;
import common_procedure.frontend.views.fragments.ProcedureTestCaseFragment;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public abstract class ProcedureSelectionsView<TEST_CASE_FRAGMENT extends ProcedureTestCaseFragment> extends ViewFx {

	protected abstract Pane idContainer();

	protected abstract Pane sizeContainer();

	protected abstract Pane manualContainer();

	protected abstract Pane codeContainer();

	protected abstract Pane solutionContainer();
	
	protected abstract Pane testCaseContainer();
	
	private Map<String, TEST_CASE_FRAGMENT> testCaseFragmentById      = new HashMap<>();
	private List<String>                    testCaseFragmentIdInOrder = new ArrayList<>();
	
	private long version = -1;
	
	@Override
	public void initialize() {
		if(solutionContainer() != null) {
			solutionContainer().setFocusTraversable(false);
		}
	}

	public void clearTestCases() {
		testCaseContainer().getChildren().clear();
		testCaseFragmentById.clear();
		testCaseFragmentIdInOrder.clear();
	}

	public void insertTestCase(int index, TEST_CASE_FRAGMENT testCaseFragment) {
		testCaseFragmentById.put(testCaseFragment.testCaseId(), testCaseFragment);
		testCaseContainer().getChildren().add(index, testCaseFragment.rootNode());
		testCaseFragmentIdInOrder.add(index, testCaseFragment.testCaseId());

		installWidthBindingsWhenNeeded(testCaseFragment);
		hideColumnsWhenNeeded(testCaseFragment);
		
		setStyleClasses();
	}

	private void installWidthBindingsWhenNeeded(TEST_CASE_FRAGMENT testCaseFragment) {
		if(idContainer() != null) {
			testCaseFragment.idContainer().translateXProperty().bind(idContainer().layoutXProperty());
			testCaseFragment.idContainer().minWidthProperty().bind(idContainer().widthProperty());
			testCaseFragment.idContainer().maxWidthProperty().bind(idContainer().widthProperty());
		}
		
		if(sizeContainer() != null) {
			testCaseFragment.sizeContainer().translateXProperty().bind(sizeContainer().layoutXProperty());
			testCaseFragment.sizeContainer().minWidthProperty().bind(sizeContainer().widthProperty());
			testCaseFragment.sizeContainer().maxWidthProperty().bind(sizeContainer().widthProperty());
		}

		if(manualContainer() != null) {
			testCaseFragment.manualContainer().translateXProperty().bind(manualContainer().layoutXProperty());
			testCaseFragment.manualContainer().minWidthProperty().bind(manualContainer().widthProperty());
			testCaseFragment.manualContainer().maxWidthProperty().bind(manualContainer().widthProperty());
		}

		if(codeContainer() != null) {
			testCaseFragment.codeContainer().translateXProperty().bind(codeContainer().layoutXProperty());
			testCaseFragment.codeContainer().minWidthProperty().bind(codeContainer().widthProperty());
			testCaseFragment.codeContainer().maxWidthProperty().bind(codeContainer().widthProperty());
		}

		if(solutionContainer() != null) {
			testCaseFragment.solutionContainer().translateXProperty().bind(solutionContainer().layoutXProperty());
			testCaseFragment.solutionContainer().minWidthProperty().bind(solutionContainer().widthProperty());
			testCaseFragment.solutionContainer().maxWidthProperty().bind(solutionContainer().widthProperty());
		}
	}

	private void hideColumnsWhenNeeded(TEST_CASE_FRAGMENT testCaseFragment) {
		if(manualContainer() == null) {
			testCaseFragment.manualContainer().setVisible(false);
		}

		if(codeContainer() == null) {
			testCaseFragment.codeContainer().setVisible(false);
		}

		if(solutionContainer() == null) {
			testCaseFragment.solutionContainer().setVisible(false);
		}
	}

	private void setStyleClasses() {

		int testCaseIndex = 0;
		TEST_CASE_FRAGMENT lastTestCaseFragment = null;
		for(String testCaseId : testCaseFragmentIdInOrder) {

			TEST_CASE_FRAGMENT testCaseFragment = testCaseFragmentById.get(testCaseId);

			if(testCaseFragment != null) {
				testCaseFragment.rootNode().getStyleClass().clear();
				testCaseFragment.rootNode().getStyleClass().add("center");
				testCaseFragment.rootNode().getStyleClass().add("test-case-fragment-container");
				if(testCaseIndex%2==0) {
					testCaseFragment.rootNode().getStyleClass().add("test-case-fragment-container-even");
				}else {
					testCaseFragment.rootNode().getStyleClass().add("test-case-fragment-container-odd");
				}
				
				lastTestCaseFragment = testCaseFragment;
				testCaseIndex++;
			}
		}
		
		if(lastTestCaseFragment != null) {
			lastTestCaseFragment.rootNode().getStyleClass().add("test-case-fragment-container-last");
		}
	}
	
	public TEST_CASE_FRAGMENT testCaseFragment(String testCaseId) {
		return testCaseFragmentById.get(testCaseId);
	}

	public boolean hasEarlierVersion(long version) {
		return this.version < version;
	}

	public void memorizeVersion(long version) {
		this.version = version;
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.frontend.views;

import ca.ntro.app.Ntro;
import common.frontend.views.CommonRootView;
import common.messages.MsgStopExecutionReplay;
import common_procedure.frontend.events.EvtStartExecutionReplay;
import common_procedure.frontend.events.EvtToggleExecutionReplay;
import common_procedure.messages.MsgExecutionFastForwardToLastStep;
import common_procedure.messages.MsgExecutionRewindToFirstStep;
import common_procedure.messages.MsgExecutionStepBack;
import common_procedure.messages.MsgExecutionStepForward;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ProcedureRootView extends CommonRootView {
	

	@Override
	protected boolean onKeyEventNotConsumed(KeyEvent keyEvent) {
		boolean consumed = false;

		if(keyEvent.getCode().equals(KeyCode.SPACE)) {

			Ntro.newEvent(EvtToggleExecutionReplay.class)
				.trigger();

			consumed = true;

		} else if(keyEvent.getCode().equals(KeyCode.LEFT)) {

			Ntro.newMessage(MsgStopExecutionReplay.class)
			    .send();

			Ntro.newMessage(MsgExecutionStepBack.class)
			    .send();

			consumed = true;

		} else if(keyEvent.getCode().equals(KeyCode.RIGHT)) {

			Ntro.newMessage(MsgStopExecutionReplay.class)
			    .send();
			
			Ntro.newMessage(MsgExecutionStepForward.class)
			    .send();

			consumed = true;

		} else if(keyEvent.getCode().equals(KeyCode.UP)) {

			Ntro.newMessage(MsgStopExecutionReplay.class)
			    .send();
			
			Ntro.newMessage(MsgExecutionRewindToFirstStep.class)
			    .send();

			consumed = true;

		} else if(keyEvent.getCode().equals(KeyCode.DOWN)) {

			Ntro.newMessage(MsgStopExecutionReplay.class)
			    .send();
			
			Ntro.newMessage(MsgExecutionFastForwardToLastStep.class)
			    .send();

			consumed = true;
		}
		
		return consumed;
	}

}

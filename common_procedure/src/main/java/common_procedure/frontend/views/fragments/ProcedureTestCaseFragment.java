/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.frontend.views.fragments;


import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewFragmentFx;
import common.models.enums.Attempt;
import common.test_cases.descriptor.AbstractAttemptDescriptor;
import common_procedure.frontend.events.EvtChangeTestCaseAttempt;
import common_procedure.messages.MsgChangeTestCaseAttempt;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public abstract class ProcedureTestCaseFragment extends ViewFragmentFx {
	
	private String testCaseId;

	protected abstract Label testCaseIdLabel();
	protected abstract Label inputSizeLabel();
	protected abstract Button manualButton();
	protected abstract Button codeButton();
	protected abstract Button solutionButton();
	
	public abstract Pane idContainer();
	public abstract Pane sizeContainer();
	public abstract Pane manualContainer();
	public abstract Pane codeContainer();
	public abstract Pane solutionContainer();
	
	private String solutionText;
	private String errorText;
	
	@Override
	public void initialize() {
		
		EvtChangeTestCaseAttempt evtChangeTestCaseAttempt = Ntro.newEvent(EvtChangeTestCaseAttempt.class);
		
		if(manualButton() != null) {
			manualButton().setFocusTraversable(false);
			manualButton().setOnAction(evtFx -> {
				
				evtChangeTestCaseAttempt.setTestCaseId(testCaseId);
				evtChangeTestCaseAttempt.setAttempt(Attempt.MANUAL);
				evtChangeTestCaseAttempt.trigger();
				
			});
		}

		codeButton().setOnAction(evtFx -> {
			codeButton().setFocusTraversable(false);
			
			evtChangeTestCaseAttempt.setTestCaseId(testCaseId);
			evtChangeTestCaseAttempt.setAttempt(Attempt.CODE);
			evtChangeTestCaseAttempt.trigger();
			
		});

		solutionButton().setOnAction(evtFx -> {
			solutionButton().setFocusTraversable(false);
			
			evtChangeTestCaseAttempt.setTestCaseId(testCaseId);
			evtChangeTestCaseAttempt.setAttempt(Attempt.SOLUTION);
			evtChangeTestCaseAttempt.trigger();
			
		});
		
		this.solutionText = resources().getString("solutionText");
		this.errorText = resources().getString("errorText");
	}

	public void displayTestCaseId(String testCaseId) {
		testCaseIdLabel().setText(testCaseId);
	}

	public void displayInputSize(String inputSize) {
		inputSizeLabel().setText(inputSize);
	}

	public void displaySolution(AbstractAttemptDescriptor attempt, boolean isCurrentAttempt) {
		solutionButton().setText(solutionText);
		
		if(isCurrentAttempt) {

			solutionButton().setStyle("-fx-background-color:lightblue;-fx-text-fill:green;-fx-font-weight:bold");
			
		}else {

			solutionButton().setStyle("-fx-background-color:lightgray;-fx-text-fill:green;-fx-font-weight:bold");
			
		}
	}

	public void displayManual(AbstractAttemptDescriptor attempt, boolean isCurrentAttempt) {
		
		if(manualButton() != null) {
			
			manualButton().setText("¤");
			displayIsCurrentAttempt(manualButton(), isCurrentAttempt);
		}
	}

	public void displayCode(AbstractAttemptDescriptor attempt, boolean isCurrentAttempt) {
		if(attempt.isLoaded() && attempt.isASolution()) {

			codeButton().setText(solutionText);
			
			if(isCurrentAttempt) {

				codeButton().setStyle("-fx-background-color:lightblue;-fx-text-fill:green;-fx-font-weight:bold");
				
			}else {

				codeButton().setStyle("-fx-background-color:lightgray;-fx-text-fill:green;-fx-font-weight:bold");
				
			}


		}else if(attempt.isLoaded() && !attempt.isASolution()){

			codeButton().setText(errorText);

			if(isCurrentAttempt) {

				codeButton().setStyle("-fx-background-color:lightblue;-fx-text-fill:red;-fx-font-weight:bold;");
				
			}else {

				codeButton().setStyle("-fx-background-color:lightgray;-fx-text-fill:red;-fx-font-weight:bold;");
				
			}

		}else {

			codeButton().setText("¤");

			if(isCurrentAttempt) {

				codeButton().setStyle("-fx-background-color:lightblue;");
				
			}else {

				codeButton().setStyle("-fx-background-color:gray;");
				
			}
			
		}

	}

	private void displayIsCurrentAttempt(Button button, boolean isCurrentAttempt) {
		if(isCurrentAttempt) {

			button.setStyle("-fx-background-color: lightblue;");

		}else {

			button.setStyle("-fx-background-color: lightgray;");

		}

	}

	public void enableTestCaseSelection() {
		if(manualButton() != null) {
			manualButton().setDisable(false);
			manualButton().setFocusTraversable(false);
		}

		if(codeButton() != null) {
			codeButton().setDisable(false);
			codeButton().setFocusTraversable(false);
		}

		if(solutionButton() != null) {
			solutionButton().setDisable(false);
			solutionButton().setFocusTraversable(false);
		}
	}

	public void disableTestCaseSelection() {
		if(manualButton() != null) {
			manualButton().setDisable(true);
			manualButton().setFocusTraversable(false);
		}

		if(manualButton() != null) {
			codeButton().setDisable(true);
			manualButton().setFocusTraversable(false);
		}

		if(solutionButton() != null) {
			solutionButton().setDisable(true);
			manualButton().setFocusTraversable(false);
		}
	}

	public void memorizeTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}
	
	public String testCaseId() {
		return testCaseId;
	}

}

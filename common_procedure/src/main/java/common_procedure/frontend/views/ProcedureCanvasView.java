/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.frontend.views;

import java.util.Set;

import ca.ntro.app.Ntro;
import ca.ntro.app.fx.controls.ResizableWorld2dCanvasFx;
import common.frontend.views.CommonCanvasView;
import common.models.world2d.CommonDrawingOptions;
import common.models.world2d.CommonWorld2d;
import common_procedure.frontend.events.MouseEvtOnPreviewCanvas;

public abstract class ProcedureCanvasView extends CommonCanvasView {

	protected abstract ResizableWorld2dCanvasFx previewCanvas();

	protected abstract double initialPreviewCanvasScreenHeight();

	@Override
	public void initialize() {
		super.initialize();

		initializeMainCanvas();
		initializePreviewCanvas();
	}

	private void initializeMainCanvas() {
		if(mainCanvas() != null) {
			mainCanvas().setInitialWorldSize(initialWorldWidth(), initialWorldHeight());
			mainCanvas().setClipModes(Set.of());
		}
	}

	private void initializePreviewCanvas() {
		if(previewCanvas() != null) {
			double screenHeight = initialPreviewCanvasScreenHeight();
			double screenWidth = screenHeight * initialWorldWidth() / initialWorldHeight();

			previewCanvas().setMinWidth(screenWidth);
			previewCanvas().setMaxWidth(screenWidth);
			previewCanvas().setMinHeight(screenHeight);
			previewCanvas().setMaxHeight(screenHeight);

			previewCanvas().setWorldWidth(initialWorldWidth());
			previewCanvas().setWorldHeight(initialWorldHeight());

			previewCanvas().relocateResizeViewport(0, 0, initialWorldWidth(), initialWorldHeight());

			MouseEvtOnPreviewCanvas mouseEvtOnTabletop = Ntro.newEvent(MouseEvtOnPreviewCanvas.class);
			
			previewCanvas().onMouseEvent(world2dMouseEventFx -> {
				mouseEvtOnTabletop.setWorld2dMouseEventFx(world2dMouseEventFx);
				mouseEvtOnTabletop.trigger();
			});
		}
	}

	public void drawWorld2d(CommonWorld2d world2d, CommonDrawingOptions options) {
		super.drawWorld2d(world2d, options);
		
		if(previewCanvas() != null) {
			world2d.drawOn(previewCanvas());
		}
	}

	public void clearCanvas() {
		super.clearCanvas();
		if(previewCanvas() != null) {
			previewCanvas().clearCanvas();
		}
	}

	public void drawViewport() {
		if(previewCanvas() != null) {
			previewCanvas().drawOnWorld(gc -> {
				gc.strokeRect(mainCanvas().getViewportTopLeftX(),
							  mainCanvas().getViewportTopLeftY(),
							  mainCanvas().getViewportWidth(),
							  mainCanvas().getViewportHeight());
			});
		}
	}


}

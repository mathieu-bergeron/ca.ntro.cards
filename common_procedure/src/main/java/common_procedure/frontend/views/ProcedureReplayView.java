/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.frontend.views;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewFx;
import common.messages.MsgStopExecutionReplay;
import common_procedure.frontend.events.EvtStartExecutionReplay;
import common_procedure.messages.MsgExecutionFastForwardToLastStep;
import common_procedure.messages.MsgExecutionRewindToFirstStep;
import common_procedure.messages.MsgExecutionStepBack;
import common_procedure.messages.MsgExecutionStepForward;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public abstract class ProcedureReplayView extends ViewFx {

	protected abstract Label numberOfStepsLabel();

	protected abstract Label currentStepLabel();

	protected abstract Label numberOfCardsLabel();

	protected abstract Button playButton();

	protected abstract Button pauseButton();

	protected abstract Button oneStepButton();

	protected abstract Button backStepButton();

	protected abstract Button rewindButton();

	protected abstract Button fastForwardButton();


	@Override
	public void initialize() {

		if(playButton() != null) {
			playButton().setFocusTraversable(false);

			playButton().setOnAction(evtFx -> {
				Ntro.newEvent(EvtStartExecutionReplay.class)
				    .trigger();
			});
		}
		
		if(pauseButton() != null) {
			pauseButton().setFocusTraversable(false);

			pauseButton().setOnAction(evtFx -> {
				Ntro.newMessage(MsgStopExecutionReplay.class)
					.send();
			});
		}
		
		if(oneStepButton() != null) {
			oneStepButton().setFocusTraversable(false);
			
			oneStepButton().setOnAction(evtFx -> {
				Ntro.newMessage(MsgStopExecutionReplay.class)
					.send();
				Ntro.newMessage(MsgExecutionStepForward.class)
				    .send();
			});
		}
		
		if(backStepButton() != null) {
			backStepButton().setFocusTraversable(false);

			backStepButton().setOnAction(evtFx -> {
				Ntro.newMessage(MsgStopExecutionReplay.class)
					.send();
				Ntro.newMessage(MsgExecutionStepBack.class)
					.send();
			});
		}
		
		if(rewindButton() != null) {
			rewindButton().setFocusTraversable(false);

			rewindButton().setOnAction(evtFx -> {
				Ntro.newMessage(MsgStopExecutionReplay.class)
					.send();
				Ntro.newMessage(MsgExecutionRewindToFirstStep.class)
				    .send();
			});
		}
		
		if(fastForwardButton() != null) {
			fastForwardButton().setFocusTraversable(false);

			fastForwardButton().setOnAction(evtFx -> {
				Ntro.newMessage(MsgStopExecutionReplay.class)
					.send();
				Ntro.newMessage(MsgExecutionFastForwardToLastStep.class)
					.send();
			});
		}

	}

	public void disableExecutionButtons() {
		playButton().setDisable(true);
		pauseButton().setDisable(true);
		oneStepButton().setDisable(true);
		fastForwardButton().setDisable(true);
		rewindButton().setDisable(true);
	}

	public void displayNumberOfSteps(String numberOfSteps) {
		if(numberOfStepsLabel() != null) {
			numberOfStepsLabel().setText(numberOfSteps);
		}
	}

	public void displayCurrentStep(String currentStep) {
		if(currentStepLabel() != null) {
			currentStepLabel().setText(currentStep);
		}
	}

	public void displayNumberOfCards(String numberOfCards) {
		if(numberOfCardsLabel() != null) {
			numberOfCardsLabel().setText(numberOfCards);
		}
	}



}

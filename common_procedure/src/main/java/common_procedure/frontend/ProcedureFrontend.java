/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.frontend;


import ca.ntro.app.frontend.ViewLoader;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import ca.ntro.app.Ntro;
import ca.ntro.app.events.EventRegistrar;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.modified.Modified;
import ca.ntro.app.session.Session;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import common.frontend.CommonFrontend;
import common.frontend.CommonSession;
import common.messages.MsgStopExecutionReplay;
import common_procedure.frontend.events.EvtChangeTestCaseAttempt;
import common_procedure.frontend.events.EvtResizeWorld2d;
import common_procedure.frontend.events.EvtStartExecutionReplay;
import common_procedure.frontend.events.EvtToggleExecutionReplay;
import common_procedure.frontend.events.MouseEvtOnPreviewCanvas;
import common_procedure.frontend.views.ProcedureCanvasView;
import common_procedure.frontend.views.ProcedureDashboardView;
import common_procedure.frontend.views.ProcedureMessagesView;
import common_procedure.frontend.views.ProcedureReplayView;
import common_procedure.frontend.views.ProcedureRootView;
import common_procedure.frontend.views.ProcedureSelectionsView;
import common_procedure.frontend.views.ProcedureSettingsView;
import common_procedure.frontend.views.ProcedureVariablesView;
import common_procedure.frontend.views.fragments.ProcedureMessageFragment;
import common_procedure.frontend.views.fragments.ProcedureTestCaseFragment;
import common_procedure.models.ProcedureCardsModel;
import common_procedure.models.ProcedureDashboardModel;
import common_procedure.models.ProcedureSettingsModel;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

public abstract class ProcedureFrontend<ROOT_VIEW            extends ProcedureRootView, 
                                        SETTINGS_VIEW        extends ProcedureSettingsView,
                                        CARDS_VIEW           extends ProcedureCanvasView, 
                                        DASHBOARD_VIEW       extends ProcedureDashboardView,
                                        SELECTIONS_VIEW      extends ProcedureSelectionsView,
                                        TEST_CASE_FRAGMENT   extends ProcedureTestCaseFragment,
                                        REPLAY_CONTROLS_VIEW extends ProcedureReplayView,
                                        VARIABLES_VIEW       extends ProcedureVariablesView,
                                        MESSAGES_VIEW        extends ProcedureMessagesView,
                                        MESSAGE_FRAGMENT     extends ProcedureMessageFragment,
                                        VIEW_DATA            extends ProcedureViewData,
                                        CARDS_MODEL          extends ProcedureCardsModel,
                                        DASHBOARD_MODEL      extends ProcedureDashboardModel,
                                        SETTINGS_MODEL       extends ProcedureSettingsModel>

       extends CommonFrontend<ROOT_VIEW,
                              SETTINGS_VIEW,
                              CARDS_VIEW,
                              DASHBOARD_VIEW,
                              MESSAGES_VIEW,
                              MESSAGE_FRAGMENT,
                              VIEW_DATA,
                              CARDS_MODEL,
                              DASHBOARD_MODEL,
                              SETTINGS_MODEL> {

	@Override
	protected Class<? extends CommonSession> sessionClass() {
		return ProcedureSession.class;
	}
                            	  
    protected Class<CARDS_MODEL> cardsModelClass(){
    	return getCanvasModelClass();
    }
    
    protected abstract Class<TEST_CASE_FRAGMENT> testCaseFragmentClass();

	@Override
	public void registerEvents(EventRegistrar registrar) {
		super.registerEvents(registrar);

		registrar.registerEvent(MouseEvtOnPreviewCanvas.class);
		registrar.registerEvent(EvtStartExecutionReplay.class);
		registrar.registerEvent(EvtToggleExecutionReplay.class);
		registrar.registerEvent(EvtResizeWorld2d.class);
		registrar.registerEvent(EvtChangeTestCaseAttempt.class);
	}

	@Override
	public void registerViews(ViewRegistrarFx registrar) {
		super.registerViews(registrar);

		registrar.registerView(selectionsViewClass(), "/selections.xml");
		registrar.registerView(replayControlsViewClass(), "/replay.xml");
		registrar.registerView(variablesViewClass(), "/variables.xml");
		registrar.registerView(testCaseFragmentClass(), "/fragments/test_case.xml");
	}

	@Override
	protected void initializeViewData(VIEW_DATA viewData) {
		viewData.setCardsModelClass(cardsModelClass());
	}
	

	@Override
	protected void addSubTasksToViewData(FrontendTasks tasks) {

		mouseEvtOnPreviewCanvas(tasks);

		startCodeExecution(tasks);

		stopCodeExecution(tasks);

		toggleCodeExecution(tasks);
		
		resizeWorld2d(tasks);
		
	}
	

	private void startCodeExecution(FrontendTasks tasks) {
		tasks.task("startCodeExecution")

		      .waitsFor(event(EvtStartExecutionReplay.class))

		      .waitsFor(created(viewDataClass()))
		      
		      .executes(inputs -> {
		    	  
		    	  // FIXME: should be in the backend
		    	  NtroImpl.models().suspendDiskOperations();

		    	  VIEW_DATA cardsViewData = inputs.get(created(viewDataClass()));
		    	  cardsViewData.startExecutionReplay();
		      });
	}

	private void toggleCodeExecution(FrontendTasks tasks) {
		tasks.task("toggleCodeExecution")

		      .waitsFor(event(EvtToggleExecutionReplay.class))

		      .waitsFor(created(viewDataClass()))
		      
		      .executes(inputs -> {
		    	  
		    	  VIEW_DATA cardsViewData = inputs.get(created(viewDataClass()));
		    	  
		    	  cardsViewData.toggleExecutionReplayViaEventOrMessage();
		      });
	}


	private void stopCodeExecution(FrontendTasks tasks) {
		tasks.task("stopCodeExecution")

		      .waitsFor(message(MsgStopExecutionReplay.class))

		      .waitsFor(created(viewDataClass()))
		      
		      .executes(inputs -> {

		    	  // FIXME: should be in the backend
		    	  NtroImpl.models().resumeDiskOperations();
		    	  
		    	  VIEW_DATA cardsViewData = inputs.get(created(viewDataClass()));
		    	  cardsViewData.stopExecutionReplay();

		      });
	}



	private void mouseEvtOnPreviewCanvas(FrontendTasks tasks) {
		tasks.task("mouseEvtOnPreviewCanvas")
		
		      .waitsFor(event(MouseEvtOnPreviewCanvas.class))
		      
		      .waitsFor(created(canvasViewClass()))
		      
		      .executes(inputs -> {
		    	  
		    	  MouseEvtOnPreviewCanvas mouseEventOnTabletop = inputs.get(event(MouseEvtOnPreviewCanvas.class));
		    	  CARDS_VIEW         cardsView            = inputs.get(created(canvasViewClass()));
		    	  
		    	  mouseEventOnTabletop.applyTo(cardsView);
		      });
	}

	private void resizeWorld2d(FrontendTasks tasks) {
		tasks.task("resizeWorld2d")
		
		      .waitsFor(event(EvtResizeWorld2d.class))

		      .waitsFor(created(canvasViewClass()))
		      
		      .executes(inputs -> {
		    	  
		    	  EvtResizeWorld2d evtResizeWorld2d = inputs.get(event(EvtResizeWorld2d.class));
		    	  CARDS_VIEW       cardsView        = inputs.get(created(canvasViewClass()));
		    	  
		    	  evtResizeWorld2d.applyTo(cardsView);

		      });
	}

	@Override
	protected void addSubTasksToDashboard(FrontendTasks tasks) {

		tasks.task("refreshReplayControls")
		
			 .waitsFor(modified(getDashboardModelClass()))

		      .waitsFor(created(replayControlsViewClass()))
		
		     .executes(inputs -> {
		    	 
		    	 REPLAY_CONTROLS_VIEW      replayView        = inputs.get(created(replayControlsViewClass()));
		    	 Modified<DASHBOARD_MODEL> modifiedDashboard = inputs.get(modified(getDashboardModelClass()));
		    	 
		    	 modifiedDashboard.currentValue().displayOn(replayView);

		     });

		tasks.task("displayTestCases")
		
			 .waitsFor(viewLoader(testCaseFragmentClass()))

			 .waitsFor(modified(getDashboardModelClass()))
			 
			 .waitsFor(created(selectionsViewClass()))
		
		     .executes(inputs -> {
		    	 
		    	 ProcedureSession               session                = (ProcedureSession) Ntro.session(sessionClass());
		    	 SELECTIONS_VIEW                selectionsView         = inputs.get(created(selectionsViewClass()));
		    	 ViewLoader<TEST_CASE_FRAGMENT> testCaseFragmentLoader = inputs.get(viewLoader(testCaseFragmentClass()));
		    	 Modified<DASHBOARD_MODEL>      modifiedDashboard      = inputs.get(modified(getDashboardModelClass()));
		    	 
				 modifiedDashboard.currentValue().setModelSelection(cardsModelClass(), session);
		    	 modifiedDashboard.currentValue().displayOn(selectionsView, testCaseFragmentLoader);

		     });

	}



	@Override
	protected void addDashboardSubViewLoaders(FrontendTasks subTasks) {

		subTasks.task(create(selectionsViewClass()))

		        .waitsFor(viewLoader(selectionsViewClass()))
		        
		        .executesAndReturnsValue(inputs -> {

		        	   ViewLoader<SELECTIONS_VIEW> selectionsViewLoader = inputs.get(viewLoader(selectionsViewClass()));
		        	   
		        	   return selectionsViewLoader.createView();
		        });

		subTasks.task(create(replayControlsViewClass()))

		        .waitsFor(viewLoader(replayControlsViewClass()))
		        
		        .executesAndReturnsValue(inputs -> {

		        	   ViewLoader<REPLAY_CONTROLS_VIEW> replayControlViewLoader = inputs.get(viewLoader(replayControlsViewClass()));
		        	   
		        	   return replayControlViewLoader.createView();
		        });

		subTasks.task(create(variablesViewClass()))

		        .waitsFor(viewLoader(variablesViewClass()))
		        
		        .executesAndReturnsValue(inputs -> {

		        	   ViewLoader<VARIABLES_VIEW> variablesViewLoader = inputs.get(viewLoader(variablesViewClass()));
		        	   
		        	   return variablesViewLoader.createView();
		        });
	}

	@Override
	protected void installDashboardSubViews(SimpleTaskCreator<?> taskCreator) {
		
		taskCreator.waitsFor(created(selectionsViewClass()))
		           .waitsFor(created(replayControlsViewClass()))
		           .waitsFor(created(variablesViewClass()))
		           .waitsFor(created(dashboardViewClass()))
		           
		           .executes(inputs -> {
		        	   
		        	   SELECTIONS_VIEW      selectionsView     = inputs.get(created(selectionsViewClass()));
		        	   REPLAY_CONTROLS_VIEW replayControlsView = inputs.get(created(replayControlsViewClass()));
		        	   VARIABLES_VIEW       variablesView      = inputs.get(created(variablesViewClass()));
		        	   DASHBOARD_VIEW       dashboardView      = inputs.get(created(dashboardViewClass()));
		        	   
		        	   dashboardView.installSelectionsView(selectionsView);
		        	   dashboardView.installReplayControlsView(replayControlsView);
		        	   dashboardView.installVariablesView(variablesView);
		           });
	}

	protected abstract Class<SELECTIONS_VIEW> selectionsViewClass();
	protected abstract Class<REPLAY_CONTROLS_VIEW> replayControlsViewClass();
	protected abstract Class<VARIABLES_VIEW> variablesViewClass();
	
	
	@Override
	protected void createAdditionnalTasks(FrontendTasks tasks) {
		
		tasks.taskGroup("Cards")
		
		     .waitsFor("Initialization")
		     
		     .contains(subTasks -> {

		    	 userWantsToChangeTestCaseAttempt(subTasks);
		    	 
		    	 displayCardsModel(subTasks);
		    	 
		    	 addSubTasksToCards(subTasks);

		     });
	}

	protected void addSubTasksToCards(FrontendTasks subTasks) {
		
	}

	private void userWantsToChangeTestCaseAttempt(FrontendTasks subTasks) {
		
		subTasks.task("userWantsToChangeTestCaseAttempt")
		
		     .waitsFor(event(EvtChangeTestCaseAttempt.class))
		     
		     .executes(inputs -> {
		    	 
		    	 ProcedureSession         session                  = (ProcedureSession) Ntro.session(sessionClass());
		    	 EvtChangeTestCaseAttempt evtChangeTestCaseAttempt = inputs.get(event(EvtChangeTestCaseAttempt.class));
		    	 

		    	 evtChangeTestCaseAttempt.setModelSelection(cardsModelClass(), session);
		    	 
		    	 evtChangeTestCaseAttempt.sendMsgChangeTestCaseAttempt();
		    	 
		     });
	}

	private void displayCardsModel(FrontendTasks tasks) {
		
		tasks.task("displayCardsModel")
		
		     .waitsFor(modified(cardsModelClass()))
		     
		     .waitsFor(created(variablesViewClass()))
		     
		     .executes(inputs -> {
		    	 
		    	 VARIABLES_VIEW         variablesView      = inputs.get(created(variablesViewClass()));
		    	 Modified<CARDS_MODEL>  modifiedCardsModel = inputs.get(modified(cardsModelClass()));
		    	 
		    	 modifiedCardsModel.currentValue().displayOn(variablesView);
		     });
	}

	@Override
	protected void addSubTasksToMessages(FrontendTasks subTasks) {
	}

	@Override
	protected void registerAdditionnalViews(ViewRegistrarFx registrar) {
		
	}

	@Override
	protected void addSubTasksToInitialization(FrontendTasks subTasks) {

	}

	@Override
	protected void addSubTasksToNavigation(FrontendTasks subTasks) {

	}

	@Override
	protected void addSubTasksToSettings(FrontendTasks subTasks) {

	}

	@Override
	protected void registerAdditionnalEvents(EventRegistrar registrar) {
		
	}

	@Override
	protected String cssProdPath() {
		return "/prod.css";
	}

	@Override
	protected String cssDevPath() {
		return "/dev.css";
	}

	@Override
	protected String rootViewXmlPath() {
		return "/root.xml";
	}

	@Override
	protected String settingsViewXmlPath() {
		return "/settings.xml";
		
	}

	@Override
	protected String canvasViewXmlPath() {
		return "/canvas.xml";
	}

	@Override
	protected String dashboardViewXmlPath() {
		return "/dashboard.xml";
		
	}

	@Override
	protected String messagesViewXmlPath() {
		return "/messages.xml";
	}

	@Override
	protected String messageFragmentXmlPath() {
		return "/fragments/message.xml";
	}

	@Override
	protected String stringsFrPath() {
		return "strings_fr.properties";
	}

	@Override
	protected String stringsEnPath() {
		return "strings_en.properties";
	}
	
}

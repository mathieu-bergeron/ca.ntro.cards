package common_procedure.frontend;

import common.frontend.CommonSession;
import common.models.enums.Attempt;
import common_procedure.models.ProcedureCardsModel;

public class ProcedureSession<S extends ProcedureSession<?>> extends CommonSession<S> {

	

	public void setCardsModelSelection(Class<? extends ProcedureCardsModel> cardsModelClass, String testCaseId, Attempt attempt) {
		this.setModelSelection(cardsModelClass, CommonSession.defaultCanvasModelId(testCaseId, attempt));
	}


}

package common_procedure.frontend.events;

import ca.ntro.app.events.Event;
import common_procedure.frontend.views.ProcedureCanvasView;

public class EvtResizeWorld2d extends Event {
	
	private double width = 0;
	private double height = 0;

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public EvtResizeWorld2d() {

	}

	public void applyTo(ProcedureCanvasView cardsView) {

		cardsView.resizeWorld2dRelocateViewport(width, height);
	}

}

package common_procedure.frontend.events;

import ca.ntro.app.Ntro;
import ca.ntro.app.events.Event;
import ca.ntro.app.models.Model;
import ca.ntro.app.session.Session;
import common.models.enums.Attempt;
import common_procedure.frontend.ProcedureSession;
import common_procedure.messages.MsgChangeTestCaseAttempt;
import common_procedure.models.ProcedureCardsModel;

public class EvtChangeTestCaseAttempt extends Event {

	private String testCaseId;
	private Attempt attempt;
	
	public EvtChangeTestCaseAttempt setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;

		return this;
	}
	
	public EvtChangeTestCaseAttempt setAttempt(Attempt attempt) {
		this.attempt = attempt;
		
		return this;
	}

	public void setModelSelection(Class<? extends ProcedureCardsModel> cardsModelClass, ProcedureSession session) {
		session.setCardsModelSelection(cardsModelClass, testCaseId, attempt);
	}


	public void sendMsgChangeTestCaseAttempt() {
		Ntro.newMessage(MsgChangeTestCaseAttempt.class)
		    .setTestCaseId(testCaseId)
		    .setAttempt(attempt)
		    .send();
	}



}

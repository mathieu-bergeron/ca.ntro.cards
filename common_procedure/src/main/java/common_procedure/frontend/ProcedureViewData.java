/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.frontend;

import java.util.HashSet;
import java.util.Set;

import ca.ntro.app.Ntro;
import ca.ntro.app.world2d.Object2dFx;
import ca.ntro.core.stream.Stream;
import common.CommonConstants;
import common.frontend.CommonViewData;
import common.messages.MsgStopExecutionReplay;
import common.models.values.cards.AbstractCard;
import common.models.values.cards.CardHeap;
import common.models.values.cards.Carte;
import common.models.world2d.CommonObject2d;
import common_procedure.frontend.events.EvtResizeWorld2d;
import common_procedure.frontend.events.EvtStartExecutionReplay;
import common_procedure.messages.MsgExecutionStepForward;
import common_procedure.models.ProcedureCardsModel;
import common_procedure.models.world2d.ProcedureCard2d;
import common_procedure.models.world2d.ProcedureDrawingOptions;
import common_procedure.models.world2d.ProcedureMarker2d;
import common_procedure.models.world2d.ProcedureObject2d;
import common_procedure.models.world2d.ProcedureWorld2d;

public abstract class   ProcedureViewData<WORLD2D  extends ProcedureWorld2d,
                                          OPTIONS  extends ProcedureDrawingOptions>

                extends CommonViewData<ProcedureObject2d<WORLD2D>, WORLD2D, OPTIONS> {

	private boolean isExecutionReplayInProgress = false;
	private double timeSinceLastReplayStep;
	
	private Class<? extends ProcedureCardsModel> cardsModelClass;

	public void setCardsModelClass(Class<? extends ProcedureCardsModel> cardsModelClass) {
		this.cardsModelClass = cardsModelClass;
		world2d().setCardsModelClass(cardsModelClass);
	}
	

	public void onTimePasses(double secondsElapsed) {
		if(isExecutionReplayInProgress) {

			timeSinceLastReplayStep -= secondsElapsed;
			
			if(timeSinceLastReplayStep < 0) {
				timeSinceLastReplayStep = CommonConstants.SECONDS_BETWEEN_EXECUTION_STEPS;

				Ntro.newMessage(MsgExecutionStepForward.class)
					.send();
			}
		}

		super.onTimePasses(secondsElapsed);
	}

	public void startExecutionReplay() {
		isExecutionReplayInProgress = true;
		timeSinceLastReplayStep = CommonConstants.SECONDS_BETWEEN_EXECUTION_STEPS;
	}

	public void stopExecutionReplay() {
		isExecutionReplayInProgress = false;
	}

	public void toggleExecutionReplayViaEventOrMessage() {
		if(isExecutionReplayInProgress) {
			
			Ntro.newMessage(MsgStopExecutionReplay.class).send();
			
		}else {
			
			Ntro.newEvent(EvtStartExecutionReplay.class).trigger();
			
		}
		
	}

	public void addOrUpdateMarker(String markerId, 
			                      String color, 
			                      double topLeftX, 
			                      double topLeftY) {
		
		ProcedureMarker2d<WORLD2D> marker2d = null;

		marker2d = (ProcedureMarker2d) world2d().objectById(markerId);

		if(marker2d == null) {
			marker2d = new ProcedureMarker2d(markerId, color);
			world2d().addObject2d(marker2d);
		}
		
		marker2d.setTopLeftX(topLeftX);
		marker2d.setTopLeftY(topLeftY);
	}

	public void addOrUpdateMarker(String markerId, 
			                      double topLeftX, 
			                      double topLeftY) {

		addOrUpdateMarker(markerId, "#03cffc", topLeftX, topLeftY);
	}

	public void setCardFaceUp(String cardId, boolean faceUp) {

		ProcedureCard2d card2d = (ProcedureCard2d) world2d().objectById(cardId);

		if(card2d != null) {
			card2d.setFaceUp(faceUp);
		}
	}

	public void setCardFaceUp(AbstractCard card, boolean faceUp) {
		setCardFaceUp(card.id(), faceUp);
	}

	public void displayCardFaceDown(String card2dId) {
		setCardFaceUp(card2dId, false);
	}

	public void displayCardFaceDown(AbstractCard card) {
		setCardFaceUp(card.id(), false);
	}

	public void displayCardFaceUp(String card2dId) {
		setCardFaceUp(card2dId, true);
	}

	public void displayCardFaceUp(AbstractCard card) {
		setCardFaceUp(card.id(), true);
	}

	public void removeNullCards() {
		Set<String> toRemove = new HashSet<>();
		
		for(Object2dFx<?> object2d : world2d.getObjects()) {
			if(object2d instanceof ProcedureCard2d) {
				if(((ProcedureCard2d) object2d).isNullCard()) {
					toRemove.add(object2d.id());
				}
			}
		}
		
		world2d.removeObjectsWithIdIn(toRemove);

	}

	public void addOrUpdateCard(AbstractCard card, double topLeftX, double topLeftY) {
		addOrUpdateCard(card.id(), card, topLeftX, topLeftY);
	}

	public void addOrUpdateCard(String card2dId, 
			                    AbstractCard card, 
			                    double topLeftX, 
			                    double topLeftY) {
		
		boolean animateCard = true;

		ProcedureCard2d card2d = (ProcedureCard2d) world2d().objectById(card2dId);
		
		if(card2d == null) {

			card2d = newCard2d(card2dId, card);
			world2d().addObject2d(card2d);
			animateCard = false;

		}else {

			card2d.setCard(card);

		}
		
		if(card2d.isNullCard()) {
			animateCard = false;
		}
		
		if(animateCard) {

			card2d.setTarget(topLeftX, topLeftY);
			
		}else {
			
			card2d.setTopLeftX(topLeftX);
			card2d.setTopLeftY(topLeftY);

		}
	}

	protected abstract ProcedureCard2d newCard2d(String card2dId, AbstractCard card);

	public void removeCardsNotIn(Set<String> ids) {
		world2d.removeObjectsWithIdNotIn(ids);
	}

	public void resizeWorld2d(double width, double height) { 

		double padding = 10;

		EvtResizeWorld2d evtResizeRelocateViewport = Ntro.newEvent(EvtResizeWorld2d.class);

		evtResizeRelocateViewport.setWidth(width + padding);
		evtResizeRelocateViewport.setHeight(height + padding);
		
		evtResizeRelocateViewport.trigger();

	}




}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.test_cases.descriptor;

import ca.ntro.app.Ntro;
import common.models.enums.Attempt;
import common.test_cases.descriptor.CommonTestCaseDescriptor;
import common_procedure.frontend.views.fragments.ProcedureTestCaseFragment;

public class ProcedureTestCaseDescriptor<TEST_CASE_FRAGMENT extends ProcedureTestCaseFragment> extends CommonTestCaseDescriptor {

	public void display(String currentTestCaseId, 
			            Attempt currentAttempt, 
			            TEST_CASE_FRAGMENT testCaseFragment) {

		testCaseFragment.memorizeTestCaseId(getTestCaseId());
		testCaseFragment.displayTestCaseId(getTestCaseId());
		testCaseFragment.displayInputSize(String.valueOf(getInputSize()));
		
		
		boolean isCurrentTestCase = getTestCaseId().equals(currentTestCaseId);
		
		testCaseFragment.displayManual(attempt(Attempt.MANUAL), isCurrentTestCase && currentAttempt.equals(Attempt.MANUAL));
		testCaseFragment.displayCode(attempt(Attempt.CODE), isCurrentTestCase && currentAttempt.equals(Attempt.CODE));
		testCaseFragment.displaySolution(attempt(Attempt.SOLUTION), isCurrentTestCase && currentAttempt.equals(Attempt.SOLUTION));

	}



}

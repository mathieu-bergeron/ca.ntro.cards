/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.backend;

import ca.ntro.app.tasks.backend.BackendTasks;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import common.backend.CommonBackend;
import common.frontend.CommonSession;
import common.messages.MsgRefreshDashboard;
import common.models.enums.Attempt;
import common.test_cases.CommonTestCase;
import common_procedure.frontend.ProcedureSession;
import common_procedure.messages.MsgChangeTestCaseAttempt;
import common_procedure.messages.MsgExecutionFastForwardToLastStep;
import common_procedure.messages.MsgExecutionRewindToFirstStep;
import common_procedure.messages.MsgExecutionStepBack;
import common_procedure.messages.MsgExecutionStepForward;
import common_procedure.messages.ProcedureMsgAcceptManualModel;
import common_procedure.models.ProcedureCardsModel;
import common_procedure.models.ProcedureDashboardModel;
import common_procedure.models.ProcedureSettingsModel;
import common_procedure.test_cases.ProcedureTestCase;
import common_procedure.test_cases.ProcedureTestCaseDatabase;
import common_procedure.test_cases.execution_trace.ProcedureExecutionTrace;

import static ca.ntro.app.tasks.backend.BackendTasks.*;

import ca.ntro.app.Ntro;

public abstract class ProcedureBackend<EXECUTABLE_MODEL        extends ProcedureCardsModel,
                                       STUDENT_MODEL           extends EXECUTABLE_MODEL,
                                       CANVAS_MODEL            extends ProcedureCardsModel,
                                       TEST_CASE               extends ProcedureTestCase,
                                       TEST_CASE_DATABASE      extends ProcedureTestCaseDatabase,
                                       EXECUTION_TRACE         extends ProcedureExecutionTrace,
                                       DASHBOARD_MODEL         extends ProcedureDashboardModel,
                                       SETTINGS_MODEL          extends ProcedureSettingsModel,
                                       MSG_ACCEPT_MANUAL_MODEL extends ProcedureMsgAcceptManualModel>

                extends CommonBackend<EXECUTABLE_MODEL, 
                                      STUDENT_MODEL,
                                      CANVAS_MODEL,
                                      TEST_CASE, 
                                      TEST_CASE_DATABASE, 
                                      EXECUTION_TRACE,
                                      DASHBOARD_MODEL, 
                                      SETTINGS_MODEL> {
                                    	  
    private Class<MSG_ACCEPT_MANUAL_MODEL> msgAcceptManualModelClass;

    public Class<MSG_ACCEPT_MANUAL_MODEL> getMsgAcceptManualModelClass() {
		return msgAcceptManualModelClass;
	}

	public void setMsgAcceptManualModelClass(Class<MSG_ACCEPT_MANUAL_MODEL> msgAcceptManualModelClass) {
		this.msgAcceptManualModelClass = msgAcceptManualModelClass;
	}
	
	protected String initialCardsModelId(String initialTestCaseId, Attempt initialAttempt) {
		return CommonSession.defaultCanvasModelId(initialTestCaseId, initialAttempt);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public void earlyModelInitialization(String initialTestCaseId, Attempt initialAttempt) {

		if(initialTestCaseId == null || initialAttempt == null) return;
		
		DASHBOARD_MODEL dashboardModel = NtroImpl.models().load(getDashboardModelClass());
		
		dashboardModel.initialize();
		dashboardModel.setCurrentTestCaseModelIdIfNeeded(initialTestCaseId, initialAttempt);

		dashboardModel.loadInitialTestCase(getTestCaseDatabase());
		
		if(initialAttempt == Attempt.CODE) {
			dashboardModel.executeInitialTestCase(getTestCaseDatabase());
		}

		dashboardModel.updateCurrentTestCase(getTestCaseDatabase());
		
		String initialCardsModelId = initialCardsModelId(initialTestCaseId, initialAttempt);
		
		CANVAS_MODEL canvasModel = NtroImpl.models().load(getCanvasModelClass(), initialCardsModelId);

		dashboardModel.updateCardsModel(getTestCaseDatabase(), canvasModel);
		
		NtroImpl.models().save(getDashboardModelClass(), dashboardModel);
		NtroImpl.models().save(getCanvasModelClass(), initialCardsModelId);

	}

	protected void addSubTasksToModifyTestCasesModel(BackendTasks tasks) {
	}

	protected void addSubTasksToModifyCanvasModel(BackendTasks tasks) {
	}

	protected void addSubTasksToModifyDashboardModel(BackendTasks tasks) {

		tasks.task("refreshDashboard")
		
		     .waitsFor(message(MsgRefreshDashboard.class))
		     
		     .waitsFor(model(getDashboardModelClass()))

		     .executes(inputs -> {
		    	 
		    	 DASHBOARD_MODEL   dashboardModel       = inputs.get(model(getDashboardModelClass()));
		    	 
		    	 getTestCaseDatabase().addOrUpdateTestCases(dashboardModel);

		     });
	}

	@Override
	protected void addSubTasksToModifySettingsModel(BackendTasks subTasks) {
	}

	@Override
	protected void createAdditionalTasks(BackendTasks tasks) {
		tasks.taskGroup("AccessTestCaseDatabase")

		     .waitsFor(model(getCanvasModelClass()))

		     .waitsFor(model(getDashboardModelClass()))
		     
		     .contains(subTasks -> {

				executionRewind(subTasks);

				executionStepBack(subTasks);

				executionStepForward(subTasks);

				executionFastForward(subTasks);
				
				changeCurrentTestCase(subTasks);
				
				acceptManualModel(subTasks);
		    	 
		     });
	}

	protected void addSubTasksToAccessTestCaseDatabase(BackendTasks subTasks) {
		
	}


	private void executionRewind(BackendTasks tasks) {
		tasks.task("executionRewind")
		
		     .waitsFor(message(MsgExecutionRewindToFirstStep.class))

		     .waitsFor(model(getDashboardModelClass()))
		     .waitsFor(model(getCanvasModelClass()))

		     .executes(inputs -> {
		    	 
		    	 DASHBOARD_MODEL dashboardModel = inputs.get(model(getDashboardModelClass()));
		    	 CANVAS_MODEL cardsModel = inputs.get(model(getCanvasModelClass()));

		    	 dashboardModel.rewindToFirstStep();
		    	 dashboardModel.updateCardsModel(testCaseDatabase(), cardsModel);
		     });
	}

	private void executionStepBack(BackendTasks tasks) {
		tasks.task("executionStepBack")
		
		     .waitsFor(message(MsgExecutionStepBack.class))

		     .waitsFor(model(getDashboardModelClass()))
		     .waitsFor(model(getCanvasModelClass()))

		     .executes(inputs -> {
		    	 
		    	 DASHBOARD_MODEL dashboardModel = inputs.get(model(getDashboardModelClass()));
		    	 CANVAS_MODEL cardsModel = inputs.get(model(getCanvasModelClass()));

		    	 dashboardModel.stepBackward();
		    	 dashboardModel.updateCardsModel(testCaseDatabase(), cardsModel);
		     });
	}

	private void executionStepForward(BackendTasks tasks) {
		tasks.task("executionStepForward")
		
		     .waitsFor(message(MsgExecutionStepForward.class))

		     .waitsFor(model(getDashboardModelClass()))
		     .waitsFor(model(getCanvasModelClass()))

		     .executes(inputs -> {
		    	 
		    	 DASHBOARD_MODEL dashboardModel = inputs.get(model(getDashboardModelClass()));
		    	 CANVAS_MODEL cardsModel = inputs.get(model(getCanvasModelClass()));
		    	 
		    	 dashboardModel.stepForward();
		    	 dashboardModel.updateCardsModel(testCaseDatabase(), cardsModel);
		     });
	}

	private void executionFastForward(BackendTasks tasks) {
		tasks.task("executionFastForward")
		
		     .waitsFor(message(MsgExecutionFastForwardToLastStep.class))

		     .waitsFor(model(getDashboardModelClass()))
		     .waitsFor(model(getCanvasModelClass()))

		     .executes(inputs -> {
		    	 
		    	 DASHBOARD_MODEL dashboardModel = inputs.get(model(getDashboardModelClass()));
		    	 CANVAS_MODEL cardsModel = inputs.get(model(getCanvasModelClass()));
		    	 
		    	 dashboardModel.fastForwardToLastStep();
		    	 dashboardModel.updateCardsModel(testCaseDatabase(), cardsModel);
		     });
	}

	private void changeCurrentTestCase(BackendTasks tasks) {
		tasks.task("changeCurrentTestCase")
		
		     .waitsFor(message(MsgChangeTestCaseAttempt.class))

		     .waitsFor(model(getDashboardModelClass()))
		     .waitsFor(model(getCanvasModelClass()))

		     .executes(inputs -> {
		    	 
		    	 MsgChangeTestCaseAttempt msgChangeCurrentTestCase = inputs.get(message(MsgChangeTestCaseAttempt.class));
		    	 DASHBOARD_MODEL          dashboardModel           = inputs.get(model(getDashboardModelClass()));
		    	 CANVAS_MODEL             cardsModel               = inputs.get(model(getCanvasModelClass()));
		    	 
		    	 msgChangeCurrentTestCase.applyTo(dashboardModel);

		    	 dashboardModel.updateCardsModel(testCaseDatabase(), cardsModel);

		     });
	}



	private void acceptManualModel(BackendTasks tasks) {
		tasks.task("acceptManualModel")
		
		     .waitsFor(message(getMsgAcceptManualModelClass()))
		     
		     .waitsFor(model(getDashboardModelClass()))

		     .waitsFor(model(getCanvasModelClass()))

		     .executes(inputs -> {
		    	 
		    	 MSG_ACCEPT_MANUAL_MODEL  msgAcceptManualModel     = inputs.get(message(getMsgAcceptManualModelClass()));
		    	 DASHBOARD_MODEL          dashboardModel           = inputs.get(model(getDashboardModelClass()));
		    	 CANVAS_MODEL             cardsModel               = inputs.get(model(getCanvasModelClass()));
		    	 
		    	 msgAcceptManualModel.applyTo(cardsModel, 
		    			 				      dashboardModel,
		    			                      testCaseDatabase());
		     });
	}



}

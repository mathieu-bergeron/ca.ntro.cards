/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.models;


import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewLoader;
import ca.ntro.app.session.Session;
import ca.ntro.core.NtroCore;
import common.messages.MsgStopExecutionReplay;
import common.models.CommonDashboardModel;
import common.models.enums.Attempt;
import common.test_cases.descriptor.AbstractAttemptDescriptor;
import common.test_cases.descriptor.AbstractTestCaseDescriptor;
import common.test_cases.descriptor.CommonTestCaseDescriptor;
import common.test_cases.descriptor.CurrentAttemptHolder;
import common.test_cases.indexing.TestCaseById;
import common.test_cases.indexing.TestCasesByCategory;
import common_procedure.frontend.ProcedureSession;
import common_procedure.frontend.events.EvtChangeTestCaseAttempt;
import common_procedure.frontend.views.ProcedureDashboardView;
import common_procedure.frontend.views.ProcedureReplayView;
import common_procedure.frontend.views.ProcedureSelectionsView;
import common_procedure.frontend.views.fragments.ProcedureTestCaseFragment;
import common_procedure.test_cases.ProcedureTestCaseDatabase;
import common_procedure.test_cases.descriptor.ProcedureAttemptDescriptor;
import common_procedure.test_cases.descriptor.ProcedureTestCaseDescriptor;

public abstract class ProcedureDashboardModel<DASHBOARD_VIEW     extends ProcedureDashboardView,
								              CARDS_MODEL        extends ProcedureCardsModel, 
                                              TEST_CASE_DATABASE extends ProcedureTestCaseDatabase,
                                              TEST_CASE_DESC     extends ProcedureTestCaseDescriptor,
                                              REPLAY_VIEW        extends ProcedureReplayView,
                                              SELECTIONS_VIEW    extends ProcedureSelectionsView,
                                              TEST_CASE_FRAGMENT extends ProcedureTestCaseFragment>

       extends CommonDashboardModel<DASHBOARD_VIEW, TEST_CASE_DESC, TEST_CASE_DATABASE> 

       implements CurrentAttemptHolder {
    	   
    	   
    protected long version = 0;
    	   
    protected TestCaseById<CARDS_MODEL, TEST_CASE_DESC> byId = new TestCaseById<>();
    protected TestCasesByCategory<CARDS_MODEL, TEST_CASE_DESC> byCategory = new TestCasesByCategory<>();

	protected Attempt currentAttempt = null;
	protected String currentTestCaseId = null;

	public String getCurrentTestCaseId() {
		return currentTestCaseId;
	}

	public void setCurrentTestCaseId(String currentTestCaseId) {
		this.currentTestCaseId = currentTestCaseId;
	}

	public Attempt getCurrentMode() {
		return currentAttempt;
	}

	public void setCurrentMode(Attempt currentMode) {
		this.currentAttempt = currentMode;
	}

	public TestCaseById<CARDS_MODEL, TEST_CASE_DESC> getById() {
		return byId;
	}

	public void setById(TestCaseById<CARDS_MODEL, TEST_CASE_DESC> byId) {
		this.byId = byId;
	}

	public TestCasesByCategory<CARDS_MODEL, TEST_CASE_DESC> getByCategory() {
		return byCategory;
	}

	public void setByCategory(TestCasesByCategory<CARDS_MODEL, TEST_CASE_DESC> byCategory) {
		this.byCategory = byCategory;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	protected abstract String defaultTestCaseId();

	public void loadInitialTestCase(TEST_CASE_DATABASE testCaseDatabase) {
		TEST_CASE_DESC testCase = (TEST_CASE_DESC) testCaseDatabase.loadTestCaseBlocking(currentTestCaseId);

		testCase.setParentModel(this);
	}

	public void executeInitialTestCase(TEST_CASE_DATABASE testCaseDatabase) {
		testCaseDatabase.executeTestCaseBlocking(currentTestCaseId);;
	}

	public void updateCardsModel(TEST_CASE_DATABASE testCaseDatabase, CARDS_MODEL cardsModel) {
		TEST_CASE_DESC testCase = byId.testCaseById(currentTestCaseId);

		if(testCase != null) {

			AbstractAttemptDescriptor attempt = testCase.attempt(currentAttempt);

			if(attempt != null) {
				
				int step = attempt.currentStep();

				testCaseDatabase.updateCardsModel(currentTestCaseId, currentAttempt, step, cardsModel);
			}
		}
	}

	public void displayOn(REPLAY_VIEW replayView) {
		TEST_CASE_DESC testCase = byId.testCaseById(currentTestCaseId);
		
		if(testCase != null) {
			replayView.displayNumberOfCards(String.valueOf(testCase.inputSize()));

			AbstractAttemptDescriptor attempt = testCase.attempt(currentAttempt);
			
			if(attempt != null) {
				replayView.displayCurrentStep(String.valueOf(attempt.currentStep() + 1));
				replayView.displayNumberOfSteps(String.valueOf(attempt.numberOfSteps()));
			}
		}
	}

	@Override
	public void displayOn(DASHBOARD_VIEW dashboardView) {
	}

	@Override
	public void addOrUpdateTestCase(TEST_CASE_DESC testCaseDescriptor) {
		
		testCaseDescriptor.setParentModel(this);

		TEST_CASE_DESC storedTestCase = byId.addOrUpdateTestCase(testCaseDescriptor);
		byCategory.addTestCase(storedTestCase);
		
		version++;
	}
	
	public void changeTestCaseAttempt(String testCaseId, Attempt attempt) {
		this.currentTestCaseId = testCaseId;
		this.currentAttempt = attempt;
		this.version++;
		
		Ntro.newMessage(MsgStopExecutionReplay.class).send();
	}

	public void displayOn(SELECTIONS_VIEW selectionsView, ViewLoader<TEST_CASE_FRAGMENT> testCaseFragmentLoader) {

		if(selectionsView.hasEarlierVersion(version)) {

			selectionsView.memorizeVersion(version);
			
			byCategory.inOrder().reduceToResult(0, (testCaseIndex, testCaseDescriptor) -> {
				

				TEST_CASE_FRAGMENT existingFragment = (TEST_CASE_FRAGMENT) selectionsView.testCaseFragment(testCaseDescriptor.testCaseId());
				
				if(existingFragment != null) {

					testCaseDescriptor.display(currentTestCaseId, 
							                   currentAttempt,
							                   existingFragment);

				}else {

					TEST_CASE_FRAGMENT newFragment = testCaseFragmentLoader.createView();
					
					testCaseDescriptor.display(currentTestCaseId, 
							                   currentAttempt,
							                   newFragment);

					selectionsView.insertTestCase(testCaseIndex, newFragment);
				}

				return testCaseIndex+1;

			}).throwException();
		}
	}

	public int numberOfTestCases() {
		return byId.size();
	}

	public boolean containsTestCase(TEST_CASE_DESC testCaseDescriptor) {
		return containsTestCase(testCaseDescriptor.getTestCaseId());
	}

	public boolean containsTestCase(String testCaseId) {
		return byId.testCaseById(testCaseId) != null;
	}

	public void initialize() {
		byId.testCases().forEach(testCase -> {
			testCase.setLoaded(false);
			testCase.setIsSolution(false);
		});

		version = 0;
	}

	public void stepBackward() {
		TEST_CASE_DESC testCase = byId.testCaseById(currentTestCaseId);

		if(testCase != null) {

			AbstractAttemptDescriptor attempt = testCase.attempt(currentAttempt);
			
			if(attempt != null) {

				attempt.stepBackward();

			}
		}
	}

	public void stepForward() {
		TEST_CASE_DESC testCase = byId.testCaseById(currentTestCaseId);

		if(testCase != null) {

			AbstractAttemptDescriptor attempt = testCase.attempt(currentAttempt);
			
			if(attempt != null) {

				attempt.stepForward();

			}
		}
	}

	public void rewindToFirstStep() {
		TEST_CASE_DESC testCase = byId.testCaseById(currentTestCaseId);

		if(testCase != null) {

			AbstractAttemptDescriptor attempt = testCase.attempt(currentAttempt);
			
			if(attempt != null) {

				attempt.rewindToFirstStep();

			}
		}
	}

	public void fastForwardToLastStep() {
		TEST_CASE_DESC testCase = byId.testCaseById(currentTestCaseId);

		if(testCase != null) {

			AbstractAttemptDescriptor attempt = testCase.attempt(currentAttempt);
			
			if(attempt != null) {

				attempt.fastForwardToLastStep();

			}
		}
	}

	public void pushManualExecutionStep(TEST_CASE_DATABASE testCaseDatabase, CARDS_MODEL cardsModel) {
		testCaseDatabase.pushManualExecutionStep(currentTestCaseId, cardsModel);

		testCaseDatabase.addOrUpdateTestCase(currentTestCaseId, this);
	}

	public void updateCurrentTestCase(TEST_CASE_DATABASE testCaseDatabase) {
		testCaseDatabase.addOrUpdateTestCase(currentTestCaseId, this);
	}

	@Override
	public void loadDbFromDir(TEST_CASE_DATABASE testCaseDatabase) {
		testCaseDatabase.loadFromDbDir(currentTestCaseId);
	}

	@Override
	public boolean isCurrentAttempt(String testCaseId, Attempt attempt) {
		return currentTestCaseId.equals(testCaseId) && currentAttempt.equals(attempt);
	}

	public void setCurrentTestCaseModelIdIfNeeded(String initialTestCaseId, Attempt initialAttempt) {
		if(this.currentTestCaseId == null
				|| this.currentAttempt == null) {
			
			this.currentTestCaseId = initialTestCaseId;
			this.currentAttempt = initialAttempt;
			
		}
		
	}
	
	public void triggerEvtChangeCurrentTestCaseAttempt() {
		Ntro.newEvent(EvtChangeTestCaseAttempt.class)
		    .setTestCaseId(currentTestCaseId)
		    .setAttempt(currentAttempt)
		    .trigger();
	}

	public void setModelSelection(Class<CARDS_MODEL> cardsModelClass, ProcedureSession session) {
		session.setCardsModelSelection(cardsModelClass, currentTestCaseId, currentAttempt);
	}


}


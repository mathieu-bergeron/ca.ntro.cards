/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.models.world2d;

import ca.ntro.app.Ntro;
import ca.ntro.app.fx.controls.World2dMouseEventFx;
import common.frontend.events.EvtMoveViewport;
import common.models.world2d.CommonWorld2d;
import common_procedure.models.ProcedureCardsModel;
import javafx.scene.input.MouseEvent;

public abstract class ProcedureWorld2d<WORLD2D  extends ProcedureWorld2d<WORLD2D, OPTIONS>,
                                       OPTIONS  extends ProcedureDrawingOptions>

       extends CommonWorld2d {

	private ProcedureObject2d<?> draggedObject2d = null;

	private double anchorX;
	private double anchorY;
	private EvtMoveViewport evtMoveViewport = Ntro.newEvent(EvtMoveViewport.class);
	
	private Class<? extends ProcedureCardsModel> cardsModelClass;

	public void setCardsModelClass(Class<? extends ProcedureCardsModel> cardsModelClass) {
		this.cardsModelClass = cardsModelClass;
	}
	
	protected Class<? extends ProcedureCardsModel> cardsModelClass(){
		return cardsModelClass;
	}

	@Override
	protected void onMouseEventNotConsumed(World2dMouseEventFx mouseEvent) {
		MouseEvent evtFx = mouseEvent.mouseEventFx();
		double worldX = mouseEvent.worldX();
		double worldY = mouseEvent.worldY();

		if(draggedObject2d != null 
				&& evtFx.getEventType().equals(MouseEvent.MOUSE_DRAGGED)
				&& evtFx.isPrimaryButtonDown()) {

			draggedObject2d.dragTo(worldX, worldY);

		}else if(draggedObject2d != null 
				&& evtFx.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
			
			forgetDraggedObject2d();

		}else if(draggedObject2d == null 
				&& evtFx.getEventType().equals(MouseEvent.MOUSE_PRESSED)
				&& evtFx.isMiddleButtonDown()) {
			
			anchorX = evtFx.getX();
			anchorY = evtFx.getY();

		}else if(draggedObject2d == null 
				&& evtFx.getEventType().equals(MouseEvent.MOUSE_DRAGGED)
				&& evtFx.isMiddleButtonDown()) {
			
			evtMoveViewport.setIncrementX(mouseEvent.worldWidthFromCanvasWidth((anchorX - evtFx.getX())));
			evtMoveViewport.setIncrementY(mouseEvent.worldHeightFromCanvasHeight(anchorY - evtFx.getY()));

			anchorX = evtFx.getX();
			anchorY = evtFx.getY();
			
			evtMoveViewport.trigger();
		}
	}

	protected void forgetDraggedObject2d() {
		draggedObject2d = null;
		buildAndSendManualModel();
	}

	public void registerDraggedObject2d(ProcedureObject2d<WORLD2D> object2d) {
		this.draggedObject2d = object2d;
	}

	public abstract void buildAndSendManualModel();
}

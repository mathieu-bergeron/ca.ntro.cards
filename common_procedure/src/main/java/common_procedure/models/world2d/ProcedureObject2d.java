/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common_procedure.models.world2d;

import ca.ntro.app.fx.controls.World2dMouseEventFx;
import common.models.world2d.CommonObject2d;
import common_procedure.ProcedureConstants;
import javafx.scene.input.MouseEvent;

public abstract class ProcedureObject2d<WORLD2D  extends ProcedureWorld2d>

       extends        CommonObject2d<WORLD2D> {

	private double dragOffsetX;
	private double dragOffsetY;
	
	private static final double EPSILON = 1;
	private static final double INCREASE_SPEED_BELOW = 50;
	
	private double targetTopLeftX;
	private double targetTopLeftY;
	
	private int directionX = 0;
	private int directionY = 0;
	
	@Override
	public void initialize() {
		targetTopLeftX = getTopLeftX();
		targetTopLeftY = getTopLeftY();
		directionX = 0;
		directionY = 0;
		setSpeedX(0);
		setSpeedY(0);
	}

	public void setTarget(double targetTopLeftX, double targetTopLeftY) {
		this.targetTopLeftX = targetTopLeftX;
		this.targetTopLeftY = targetTopLeftY;
		
		double distanceToTargetX = Math.abs(targetTopLeftX - getTopLeftX());
		double distanceToTargetY = Math.abs(targetTopLeftY - getTopLeftY());

		directionX = Double.compare(targetTopLeftX, getTopLeftX());
		directionY = Double.compare(targetTopLeftY, getTopLeftY());
		
		setSpeedX(distanceToTargetX / ProcedureConstants.SECONDS_TO_REACH_TARGET * directionX);
		setSpeedY(distanceToTargetY / ProcedureConstants.SECONDS_TO_REACH_TARGET * directionY);

		checkIfTargetReached(1);
	}

	private void checkIfTargetReached(double speedFactor) {
		double distanceToTargetX = Math.abs(targetTopLeftX - getTopLeftX());
		double distanceToTargetY = Math.abs(targetTopLeftY - getTopLeftY());

		int nextDirectionX = Double.compare(targetTopLeftX, getTopLeftX());
		int nextDirectionY = Double.compare(targetTopLeftY, getTopLeftY());


		if(distanceToTargetX <= INCREASE_SPEED_BELOW
				|| distanceToTargetY <= INCREASE_SPEED_BELOW) {

			setSpeedX(getSpeedX() * speedFactor);
			setSpeedY(getSpeedY() * speedFactor);

		}
		
		if(distanceToTargetX <= EPSILON
				|| directionX != nextDirectionX) {
			
			reachTargetX();

		}

		if(distanceToTargetY <= EPSILON
				|| directionY != nextDirectionY) {
			
			reachTargetY();

		}
	}
	
	@Override 
	public void setTopLeftX(double topLeftX) {
		super.setTopLeftX(topLeftX);
		this.targetTopLeftX = topLeftX;
	}

	@Override 
	public void setTopLeftY(double topLeftY) {
		super.setTopLeftY(topLeftY);
		this.targetTopLeftY = topLeftY;
	}

	private void reachTargetY() {
		setTopLeftY(targetTopLeftY);
		setSpeedY(0);
		directionY = 0;
	}

	private void reachTargetX() {
		setTopLeftX(targetTopLeftX);
		setSpeedX(0);
		directionX = 0;
	}

	@Override
	protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {
		MouseEvent evtFx = mouseEvent.mouseEventFx();
		double worldX = mouseEvent.worldX();
		double worldY = mouseEvent.worldY();
		
		if(evtFx.getEventType().equals(MouseEvent.MOUSE_PRESSED)
				&& evtFx.isPrimaryButtonDown()) {
			
			dragOffsetX = worldX - getTopLeftX();
			dragOffsetY = worldY - getTopLeftY();
			
			onDragStarts();
			
			return true;
			
		}

		return false;
	}

	public void onTimePasses(double secondsElapsed) {
		super.onTimePasses(secondsElapsed);

		checkIfTargetReached(1 + 0.5 * Math.abs(0.5 - secondsElapsed));
	}

	public void dragTo(double worldX, double worldY) {
		setTopLeftX(worldX - dragOffsetX);
		setTopLeftY(worldY - dragOffsetY);
	}

	protected void onDragStarts() {
		getWorld2d().registerDraggedObject2d(this);
	}



}

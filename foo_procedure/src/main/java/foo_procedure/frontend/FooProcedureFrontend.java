/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package foo_procedure.frontend;

import ca.ntro.app.Ntro;
import common.messages.MsgMessageToUser;
import common_procedure.frontend.ProcedureFrontend;
import foo_procedure.frontend.views.FooCardsView;
import foo_procedure.frontend.views.FooProcedureDashboardView;
import foo_procedure.frontend.views.FooProcedureMessagesView;
import foo_procedure.frontend.views.FooProcedureRootView;
import foo_procedure.frontend.views.FooProcedureSettingsView;
import foo_procedure.frontend.views.FooReplayView;
import foo_procedure.frontend.views.FooSelectionsView;
import foo_procedure.frontend.views.FooVariablesView;
import foo_procedure.frontend.views.fragments.FooProcedureMessageFragment;
import foo_procedure.frontend.views.fragments.FooTestCaseFragment;
import foo_procedure.models.FooCardsModel;
import foo_procedure.models.FooProcedureDashboardModel;
import foo_procedure.models.FooProcedureSettingsModel;

public class FooProcedureFrontend<STUDENT_MODEL extends FooCardsModel>

       extends ProcedureFrontend<FooProcedureRootView,
                                 FooProcedureSettingsView, 
                                 FooCardsView, 
                                 FooProcedureDashboardView, 
                                 FooSelectionsView,
                                 FooTestCaseFragment,
                                 FooReplayView,
                                 FooVariablesView,
                                 FooProcedureMessagesView,
                                 FooProcedureMessageFragment,
                                 FooProcedureViewData, 
                                 STUDENT_MODEL, // CanvasModel
                                 FooProcedureDashboardModel, 
                                 FooProcedureSettingsModel> {


	@Override
	protected boolean isProd() {
		return false;
	}

	@Override
	protected Class<FooProcedureRootView> rootViewClass() {
		return FooProcedureRootView.class;
	}

	@Override
	protected Class<FooProcedureSettingsView> settingsViewClass() {
		return FooProcedureSettingsView.class;
	}

	@Override
	protected Class<FooCardsView> canvasViewClass() {
		return FooCardsView.class;
	}

	@Override
	protected Class<FooProcedureDashboardView> dashboardViewClass() {
		return FooProcedureDashboardView.class;
	}


	@Override
	protected Class<FooProcedureViewData> viewDataClass() {
		return FooProcedureViewData.class;
	}



	@Override
	protected Class<FooSelectionsView> selectionsViewClass() {
		return FooSelectionsView.class;
	}

	@Override
	protected Class<FooReplayView> replayControlsViewClass() {
		return FooReplayView.class;
	}

	@Override
	protected Class<FooVariablesView> variablesViewClass() {
		return FooVariablesView.class;
	}


	@Override
	protected Class<FooTestCaseFragment> testCaseFragmentClass() {
		return FooTestCaseFragment.class;
	}

	@Override
	protected Class<FooProcedureMessagesView> messagesViewClass() {
		return FooProcedureMessagesView.class;
	}

	@Override
	protected Class<FooProcedureMessageFragment> messageFragmentClass() {
		return FooProcedureMessageFragment.class;
	}



}

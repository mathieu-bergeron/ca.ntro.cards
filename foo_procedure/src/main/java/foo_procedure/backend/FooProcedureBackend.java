/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package foo_procedure.backend;



import ca.ntro.app.tasks.backend.BackendTasks;
import static ca.ntro.app.tasks.backend.BackendTasks.*;

import common_procedure.backend.ProcedureBackend;
import common_procedure.messages.ProcedureMsgAcceptManualModel;
import foo_procedure.messages.FooMsgAcceptManualModel;
import foo_procedure.models.FooCardsModel;
import foo_procedure.models.FooProcedureDashboardModel;
import foo_procedure.models.FooProcedureSettingsModel;
import foo_procedure.models.values.FooTestCase;
import foo_procedure.test_cases.FooTestCaseDatabase;
import foo_procedure.test_cases.execution_trace.FooExecutionTrace;

public class   FooProcedureBackend<STUDENT_MODEL extends FooCardsModel>


       extends ProcedureBackend<FooCardsModel,       // ExecutableModel
                                STUDENT_MODEL,
                                STUDENT_MODEL,        // CanvasModel
                                FooTestCase,
                                FooTestCaseDatabase,
                                FooExecutionTrace,
                                FooProcedureDashboardModel,
                                FooProcedureSettingsModel,
                                FooMsgAcceptManualModel> {

	
	


}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package foo_procedure;

import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import common_procedure.ProcedureApp;
import common_procedure.messages.ProcedureMsgAcceptManualModel;
import common_procedure.test_cases.descriptor.ProcedureTestCaseDescriptor;
import foo_procedure.backend.FooProcedureBackend;
import foo_procedure.frontend.FooProcedureFrontend;
import foo_procedure.frontend.FooProcedureViewData;
import foo_procedure.frontend.views.FooCardsView;
import foo_procedure.frontend.views.FooProcedureDashboardView;
import foo_procedure.frontend.views.FooProcedureMessagesView;
import foo_procedure.frontend.views.FooProcedureRootView;
import foo_procedure.frontend.views.FooProcedureSettingsView;
import foo_procedure.frontend.views.fragments.FooProcedureMessageFragment;
import foo_procedure.messages.FooMsgAcceptManualModel;
import foo_procedure.models.FooCardsModel;
import foo_procedure.models.FooProcedureDashboardModel;
import foo_procedure.models.FooProcedureSettingsModel;
import foo_procedure.models.values.FooTestCase;
import foo_procedure.test_cases.FooTestCaseDatabase;
import foo_procedure.test_cases.descriptor.FooTestCaseDescriptor;
import foo_procedure.test_cases.execution_trace.FooExecutionTrace;

public abstract class   FooProcedureApp<STUDENT_MODEL extends FooCardsModel>

                extends ProcedureApp<FooCardsModel,           // executable model
                                     STUDENT_MODEL,
                                     STUDENT_MODEL,     // canvas model
                                     FooTestCase,
                                     FooTestCaseDescriptor,
                                     FooTestCaseDatabase,
                                     FooExecutionTrace,
                                     FooProcedureDashboardModel,
                                     FooProcedureSettingsModel,
                                     FooMsgAcceptManualModel,
                                     FooProcedureBackend<STUDENT_MODEL>,
                                     FooProcedureRootView,
                                     FooCardsView,
                                     FooProcedureDashboardView,
                                     FooProcedureSettingsView,
                                     FooProcedureMessagesView,
                                     FooProcedureMessageFragment,
                                     FooProcedureViewData,
                                     FooProcedureFrontend<STUDENT_MODEL>> {

	@Override
	protected Class<FooCardsModel> executableModelClass() {
		return FooCardsModel.class;
	}

	// TODO: renommer
	protected abstract Class<STUDENT_MODEL> studentClass();

	@Override
	protected Class<FooTestCase> testCaseClass() {
		return FooTestCase.class;
	}

	@Override
	protected Class<FooTestCaseDatabase> testCasesModelClass() {
		return FooTestCaseDatabase.class;
	}


	@Override
	protected Class<FooProcedureDashboardModel> dashboardModelClass() {
		return FooProcedureDashboardModel.class;
	}


	@Override
	protected Class<FooProcedureSettingsModel> settingsModelClass() {
		return FooProcedureSettingsModel.class;
	}

	@Override
	protected FooProcedureFrontend createFrontend() {
		return new FooProcedureFrontend();
	}


	@Override
	protected FooProcedureBackend createBackend() {
		return new FooProcedureBackend();
	}




	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Class<STUDENT_MODEL> canvasModelClass() {
		return (Class<STUDENT_MODEL>) studentClass();
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return studentClass();
	}

	@Override
	protected Class<FooTestCaseDescriptor> testCaseDescriptorClass() {
		return FooTestCaseDescriptor.class;
	}

	@Override
	protected Class<FooMsgAcceptManualModel> msgAcceptManualModelClass() {
		return FooMsgAcceptManualModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}

}

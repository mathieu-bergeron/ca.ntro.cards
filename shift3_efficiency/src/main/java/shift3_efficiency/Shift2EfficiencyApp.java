/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_efficiency;

import shift3_efficiency.backend.Shift2EfficiencyBackend;
import shift3_efficiency.frontend.Shift2EfficiencyFrontend;
import shift3_efficiency.frontend.Shift2EfficiencyViewData;
import shift3_efficiency.frontend.views.Shift2EfficiencyDashboardView;
import shift3_efficiency.frontend.views.Shift2EfficiencyMessagesView;
import shift3_efficiency.frontend.views.Shift2EfficiencyRootView;
import shift3_efficiency.frontend.views.Shift2EfficiencySettingsView;
import shift3_efficiency.frontend.views.Shift2GraphsView;
import shift3_efficiency.frontend.views.fragments.Shift2EfficiencyMessageFragment;
import shift3_efficiency.models.Shift2EfficiencyDashboardModel;
import shift3_efficiency.models.Shift2EfficiencySettingsModel;
import shift3_efficiency.models.Shift2GraphsModel;
import shift3_procedure.models.Tableau;
import shift3_procedure.models.values.Shift3TestCase;
import shift3_procedure.test_cases.Shift3TestCaseDatabase;
import shift3_procedure.test_cases.execution_trace.Shift3ExecutionTrace;
import common.models.enums.Attempt;
import common_efficiency.EfficiencyApp;

public abstract class   Shift2EfficiencyApp<STUDENT_MODEL extends Tableau>

                extends EfficiencyApp<Tableau, 
                                      STUDENT_MODEL,
                                      Shift2GraphsModel,
                                      Shift3TestCase,
                                      Shift3TestCaseDatabase,
                                      Shift3ExecutionTrace,
                                      Shift2EfficiencyDashboardModel,
                                      Shift2EfficiencySettingsModel,
                                      Shift2EfficiencyBackend<STUDENT_MODEL>,
                                      Shift2EfficiencyRootView,
                                      Shift2GraphsView,
                                      Shift2EfficiencyDashboardView,
                                      Shift2EfficiencySettingsView,
                                      Shift2EfficiencyMessagesView,
                                      Shift2EfficiencyMessageFragment,
                                      Shift2EfficiencyViewData,
                                      Shift2EfficiencyFrontend> {

	@Override
	protected Shift2EfficiencyFrontend createFrontend() {
		return new Shift2EfficiencyFrontend();
	}

	@Override
	protected Shift2EfficiencyBackend createBackend() {
		return new Shift2EfficiencyBackend();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class<Tableau> executableModelClass() {
		return Tableau.class;
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return studentClass();
	}
	
	// TODO: renommer
	protected abstract Class<STUDENT_MODEL> studentClass();

	@Override
	protected Class<Shift2GraphsModel> canvasModelClass() {
		return Shift2GraphsModel.class;
	}

	@Override
	protected Class<Shift3TestCase> testCaseClass() {
		return Shift3TestCase.class;
	}

	@Override
	protected Class<Shift3TestCaseDatabase> testCasesModelClass() {
		return Shift3TestCaseDatabase.class;
	}

	@Override
	protected Class<Shift2EfficiencyDashboardModel> dashboardModelClass() {
		return Shift2EfficiencyDashboardModel.class;
	}

	@Override
	protected Class<Shift2EfficiencySettingsModel> settingsModelClass() {
		return Shift2EfficiencySettingsModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}

	@Override
	protected Attempt initialAttempt() {
		return Attempt.SOLUTION;
	}
}

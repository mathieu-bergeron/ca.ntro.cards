/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_efficiency.frontend;

import shift3_efficiency.frontend.views.Shift2EfficiencyDashboardView;
import shift3_efficiency.frontend.views.Shift2EfficiencyMessagesView;
import shift3_efficiency.frontend.views.Shift2EfficiencyRootView;
import shift3_efficiency.frontend.views.Shift2EfficiencySettingsView;
import shift3_efficiency.frontend.views.Shift2GraphsView;
import shift3_efficiency.frontend.views.fragments.Shift2EfficiencyMessageFragment;
import shift3_efficiency.models.Shift2EfficiencyDashboardModel;
import shift3_efficiency.models.Shift2EfficiencySettingsModel;
import shift3_efficiency.models.Shift2GraphsModel;
import ca.ntro.app.Ntro;
import common.messages.MsgMessageToUser;
import common_efficiency.frontend.EfficiencyFrontend;

public class Shift2EfficiencyFrontend 

       extends EfficiencyFrontend<Shift2EfficiencyRootView,
                                  Shift2EfficiencySettingsView, 
                                  Shift2GraphsView, 
                                  Shift2EfficiencyDashboardView, 
                                  Shift2EfficiencyMessagesView,
                                  Shift2EfficiencyMessageFragment,
                                  Shift2EfficiencyViewData, 
                                  Shift2GraphsModel, 
                                  Shift2EfficiencyDashboardModel, 
                                  Shift2EfficiencySettingsModel> {

	@Override
	protected boolean isProd() {
		return true;
	}

	@Override
	protected Class<Shift2EfficiencyRootView> rootViewClass() {
		return Shift2EfficiencyRootView.class;
	}

	@Override
	protected Class<Shift2EfficiencySettingsView> settingsViewClass() {
		return Shift2EfficiencySettingsView.class;
	}

	@Override
	protected Class<Shift2GraphsView> canvasViewClass() {
		return Shift2GraphsView.class;
	}

	@Override
	protected Class<Shift2EfficiencyDashboardView> dashboardViewClass() {
		return Shift2EfficiencyDashboardView.class;
	}


	@Override
	protected Class<Shift2EfficiencyViewData> viewDataClass() {
		return Shift2EfficiencyViewData.class;
	}


	@Override
	protected Class<Shift2EfficiencyMessagesView> messagesViewClass() {
		return Shift2EfficiencyMessagesView.class;
	}

	@Override
	protected Class<Shift2EfficiencyMessageFragment> messageFragmentClass() {
		return Shift2EfficiencyMessageFragment.class;
	}


}

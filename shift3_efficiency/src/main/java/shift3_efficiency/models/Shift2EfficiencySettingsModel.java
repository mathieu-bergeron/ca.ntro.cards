/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift3_efficiency.models;

import common_efficiency.models.EfficiencySettingsModel;
import shift3_efficiency.frontend.views.Shift2EfficiencySettingsView;
import shift3_efficiency.models.world2d.Shift2EfficiencyDrawingOptions;

public class Shift2EfficiencySettingsModel extends EfficiencySettingsModel<Shift2EfficiencySettingsView, 
                                                                         Shift2EfficiencyDrawingOptions> 

             implements Shift2EfficiencyDrawingOptions {

	@Override
	public Shift2EfficiencyDrawingOptions drawingOptions() {
		return this;
	}

	@Override
	public boolean useFourCardColors() {
		return getUseFourCardColors();
	}

	@Override
	public void displayOn(Shift2EfficiencySettingsView settingsView) {
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package naivesort_efficiency.models;

import common_efficiency.models.EfficiencyGraphsModel;
import naivesort_efficiency.frontend.NaivesortEfficiencyViewData;
import naivesort_efficiency.models.world2d.NaivesortEfficiencyDrawingOptions;
import naivesort_efficiency.models.world2d.NaivesortEfficiencyObject2d;
import naivesort_efficiency.models.world2d.NaivesortEfficiencyWorld2d;

public class   NaivesortGraphsModel 

       extends EfficiencyGraphsModel<NaivesortGraphsModel,
                                     NaivesortEfficiencyObject2d,
                                     NaivesortEfficiencyWorld2d,
                                     NaivesortEfficiencyDrawingOptions,
                                     NaivesortEfficiencyViewData>{

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateViewData(NaivesortEfficiencyViewData cardsViewData) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void copyDataFrom(NaivesortGraphsModel cardsModel) {
		// TODO Auto-generated method stub
		
	}

}

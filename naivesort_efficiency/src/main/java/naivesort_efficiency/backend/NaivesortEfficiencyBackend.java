/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package naivesort_efficiency.backend;

import ca.ntro.app.tasks.backend.BackendTasks;
import common_efficiency.backend.EfficiencyBackend;
import common_procedure.test_cases.execution_trace.ProcedureExecutionTrace;
import naivesort_efficiency.models.NaivesortEfficiencyDashboardModel;
import naivesort_efficiency.models.NaivesortEfficiencySettingsModel;
import naivesort_efficiency.models.NaivesortGraphsModel;
import naivesort_procedure.models.TriNaif;
import naivesort_procedure.models.values.NaivesortTestCase;
import naivesort_procedure.test_cases.NaivesortTestCaseDatabase;
import naivesort_procedure.test_cases.execution_trace.NaivesortExecutionTrace;

public class NaivesortEfficiencyBackend<STUDENT_MODEL extends TriNaif>

       extends EfficiencyBackend<TriNaif, 
                                 STUDENT_MODEL,
                                 NaivesortGraphsModel,            // CanvasModel
                                 NaivesortTestCase,
                                 NaivesortTestCaseDatabase,
                                 NaivesortExecutionTrace,
                                 NaivesortEfficiencyDashboardModel,
                                 NaivesortEfficiencySettingsModel> {



	@Override
	protected void addSubTasksToModifyCanvasModel(BackendTasks subTasks) {
		
	}

	@Override
	protected void addSubTasksToModifyDashboardModel(BackendTasks subTasks) {
		
	}

	@Override
	protected void addSubTasksToModifySettingsModel(BackendTasks subTasks) {
		
	}


	@Override
	protected void createAdditionalTasks(BackendTasks tasks) {
		
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package naivesort_efficiency;


import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import common_efficiency.EfficiencyApp;
import common_procedure.test_cases.execution_trace.ProcedureExecutionTrace;
import naivesort_efficiency.backend.NaivesortEfficiencyBackend;
import naivesort_efficiency.frontend.NaivesortEfficiencyFrontend;
import naivesort_efficiency.frontend.NaivesortEfficiencyViewData;
import naivesort_efficiency.frontend.views.NaivesortEfficiencyDashboardView;
import naivesort_efficiency.frontend.views.NaivesortEfficiencyMessagesView;
import naivesort_efficiency.frontend.views.NaivesortEfficiencyRootView;
import naivesort_efficiency.frontend.views.NaivesortEfficiencySettingsView;
import naivesort_efficiency.frontend.views.NaivesortGraphsView;
import naivesort_efficiency.frontend.views.fragments.NaivesortEfficiencyMessageFragment;
import naivesort_efficiency.models.NaivesortEfficiencyDashboardModel;
import naivesort_efficiency.models.NaivesortEfficiencySettingsModel;
import naivesort_efficiency.models.NaivesortGraphsModel;
import naivesort_procedure.models.TriNaif;
import naivesort_procedure.models.values.NaivesortTestCase;
import naivesort_procedure.test_cases.NaivesortTestCaseDatabase;
import naivesort_procedure.test_cases.execution_trace.NaivesortExecutionTrace;

public abstract class   NaivesortEfficiencyApp<STUDENT_MODEL extends TriNaif>

                extends EfficiencyApp<TriNaif, 
                                      STUDENT_MODEL,
                                      NaivesortGraphsModel,
                                      NaivesortTestCase,
                                      NaivesortTestCaseDatabase,
                                      NaivesortExecutionTrace,
                                      NaivesortEfficiencyDashboardModel,
                                      NaivesortEfficiencySettingsModel,
                                      NaivesortEfficiencyBackend<STUDENT_MODEL>,
                                      NaivesortEfficiencyRootView,
                                      NaivesortGraphsView,
                                      NaivesortEfficiencyDashboardView,
                                      NaivesortEfficiencySettingsView,
                                      NaivesortEfficiencyMessagesView,
                                      NaivesortEfficiencyMessageFragment,
                                      NaivesortEfficiencyViewData,
                                      NaivesortEfficiencyFrontend> {

	@Override
	protected NaivesortEfficiencyFrontend createFrontend() {
		return new NaivesortEfficiencyFrontend();
	}

	@Override
	protected NaivesortEfficiencyBackend createBackend() {
		return new NaivesortEfficiencyBackend();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class<TriNaif> executableModelClass() {
		return TriNaif.class;
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return classeTriNaif();
	}
	
	protected abstract Class<STUDENT_MODEL> classeTriNaif();

	@Override
	protected Class<NaivesortGraphsModel> canvasModelClass() {
		return NaivesortGraphsModel.class;
	}

	@Override
	protected Class<NaivesortTestCase> testCaseClass() {
		return NaivesortTestCase.class;
	}

	@Override
	protected Class<NaivesortTestCaseDatabase> testCasesModelClass() {
		return NaivesortTestCaseDatabase.class;
	}

	@Override
	protected Class<NaivesortEfficiencyDashboardModel> dashboardModelClass() {
		return NaivesortEfficiencyDashboardModel.class;
	}

	@Override
	protected Class<NaivesortEfficiencySettingsModel> settingsModelClass() {
		return NaivesortEfficiencySettingsModel.class;
	}

	@Override
	protected void registerAdditionnalModels(ModelRegistrar registrar) {
		
	}

	@Override
	protected void registerAdditionnalMessages(MessageRegistrar registrar) {
		
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}
}

package common.test_cases;

import common.test_cases.descriptor.AbstractTestCaseDescriptor;

public interface OnTestCaseLoaded {
	
	void onTestCaseLoaded(AbstractTestCaseDescriptor testCaseDescriptor);

}

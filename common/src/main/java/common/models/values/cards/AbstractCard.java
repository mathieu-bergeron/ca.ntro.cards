/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common.models.values.cards;

import java.io.Serializable;

import ca.ntro.app.models.ModelValue;
import common.models.world2d.CommonDrawingOptions;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class AbstractCard<OPTIONS extends CommonDrawingOptions> 


       implements ModelValue, Comparable<AbstractCard>, Serializable {

	private static final long serialVersionUID = 7116211713513539219L;

	public void drawFaceDown(GraphicsContext gc, 
			                    double topLeftX, 
			                    double topLeftY, 
			                    double width, 
			                    double height, 
			                    int levelOfDetails) {

		//if(levelOfDetails > 5) {

			drawFaceDownHighDetails(gc, topLeftX, topLeftY, width, height);

		//}else {

			//drawFaceDownLowDetails(gc, topLeftX, topLeftY, width, height, options);

		//}
	}


	private void drawFaceDownLowDetails(GraphicsContext gc, 
			                            double topLeftX, 
			                            double topLeftY, 
			                            double width, 
			                            double height) {
		
		gc.setFill(Color.web("#aaaaaa"));
		gc.fillRect(topLeftX, 
					topLeftY,
					width, 
					height);
	}


	private void drawFaceDownHighDetails(GraphicsContext gc, 
			                  double topLeftX, 
			                  double topLeftY, 
			                  double width, 
			                  double height) {
		
		gc.setFill(Color.web("#999999"));
		gc.fillRect(topLeftX, 
					topLeftY,
					width, 
					height);

		gc.setStroke(Color.web("#000000"));
		gc.strokeRect(topLeftX, 
					  topLeftY,
					  width, 
					  height);
	}


	public abstract void drawFaceUp(GraphicsContext gc, 
			                           double topLeftX, 
			                           double topLeftY, 
			                           double width, 
			                           double height, 
			                           int levelOfDetails);

	public abstract boolean isNullCard();

	public abstract String id();

	public boolean hasId(String id) {
		return String.valueOf(this.id()).equals(id);
	}

	public abstract void format(StringBuilder builder);

}

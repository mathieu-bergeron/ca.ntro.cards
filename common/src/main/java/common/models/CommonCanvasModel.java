/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common.models;


import ca.ntro.app.models.Model;
import ca.ntro.ntro_core_impl.reflection.object_graph.Initialize;
import common.frontend.CommonViewData;
import common.models.world2d.CommonDrawingOptions;
import common.models.world2d.CommonObject2d;
import common.models.world2d.CommonWorld2d;

public abstract class CommonCanvasModel<CANVAS_MODEL extends CommonCanvasModel, 
                                        OBJECT2D     extends CommonObject2d<WORLD2D>,
                                        WORLD2D      extends CommonWorld2d,
                                        OPTIONS      extends CommonDrawingOptions,
                                        VIEW_DATA    extends CommonViewData<OBJECT2D, WORLD2D, OPTIONS>>

       implements     Model, Initialize {

	public abstract void updateViewData(VIEW_DATA cardsViewData);
	
	public abstract void copyDataFrom(CANVAS_MODEL otherModel);


}

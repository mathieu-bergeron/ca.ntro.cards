/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common.models.world2d;


import ca.ntro.app.Ntro;
import ca.ntro.app.fx.controls.World2dMouseEventFx;
import ca.ntro.app.world2d.World2dFx;
import common.frontend.events.EvtMoveViewport;
import javafx.scene.input.MouseEvent;

public abstract class   CommonWorld2d

                extends World2dFx {
	
	private double anchorX;
	private double anchorY;
	private EvtMoveViewport evtMoveViewport = Ntro.newEvent(EvtMoveViewport.class);

	@Override
	protected void initialize() {
		setClipAtViewport(false);
	}

	@Override
	protected void onMouseEventNotConsumed(World2dMouseEventFx mouseEvent) {
		MouseEvent evtFx = mouseEvent.mouseEventFx();
		double worldX = mouseEvent.worldX();
		double worldY = mouseEvent.worldY();
	}


}

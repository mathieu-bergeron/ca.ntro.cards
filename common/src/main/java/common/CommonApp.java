/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common;

import ca.ntro.app.NtroAppFx;
import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.app.frontend.FrontendRegistrarFx;
import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_impl.backend.BackendRegistrarNtro;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import common.backend.CommonBackend;
import common.commands.AddCommand;
import common.commands.ClearCommand;
import common.commands.DeleteCommand;
import common.commands.GetCommand;
import common.commands.InsertCommand;
import common.frontend.CommonFrontend;
import common.frontend.CommonSession;
import common.frontend.CommonViewData;
import common.frontend.views.CommonCanvasView;
import common.frontend.views.CommonDashboardView;
import common.frontend.views.CommonMessagesView;
import common.frontend.views.CommonRootView;
import common.frontend.views.CommonSettingsView;
import common.frontend.views.fragments.CommonMessageFragment;
import common.messages.MsgMessageToUser;
import common.messages.MsgRefreshDashboard;
import common.messages.MsgStartExecutionEngine;
import common.messages.MsgStopExecutionReplay;
import common.messages.MsgToggleUseFourCardColors;
import common.models.CommonCanvasModel;
import common.models.CommonDashboardModel;
import common.models.CommonExecutableModel;
import common.models.CommonSettingsModel;
import common.models.enums.Attempt;
import common.models.values.cards.AbstractCard;
import common.models.values.cards.Carte;
import common.test_cases.CommonTestCase;
import common.test_cases.CommonTestCaseDatabase;
import common.test_cases.descriptor.CommonTestCaseDescriptor;
import common.test_cases.execution_trace.CommonExecutionTrace;
import common.test_cases.execution_trace.CommonExecutionTraceFull;
import common.test_cases.execution_trace.CommonExecutionTraceSizeOnly;
import common.test_cases.indexing.TestCaseById;
import common.test_cases.indexing.TestCasesByCategory;
import common.test_cases.indexing.TestCasesBySize;

public abstract class CommonApp<EXECUTABLE_MODEL   extends CommonExecutableModel,
                                STUDENT_MODEL      extends EXECUTABLE_MODEL,
                                CANVAS_MODEL       extends CommonCanvasModel,
                                TEST_CASE          extends CommonTestCase,
                                TEST_CASE_DATABASE extends CommonTestCaseDatabase,
                                EXECUTION_TRACE    extends CommonExecutionTrace,
                                DASHBOARD_MODEL    extends CommonDashboardModel,
                                SETTINGS_MODEL     extends CommonSettingsModel,
                                                                                                      
                                BACKEND extends CommonBackend<EXECUTABLE_MODEL, 
                                                              STUDENT_MODEL,
                                                              CANVAS_MODEL,
                                                              TEST_CASE, 
                                                              TEST_CASE_DATABASE, 
                                                              EXECUTION_TRACE,
                                                              DASHBOARD_MODEL, 
                                                              SETTINGS_MODEL>,
                                   
                                ROOT_VIEW        extends CommonRootView, 
                                CARDS_VIEW       extends CommonCanvasView, 
                                DASHBOARD_VIEW   extends CommonDashboardView,
                                SETTINGS_VIEW    extends CommonSettingsView,
                                MESSAGES_VIEW    extends CommonMessagesView,
                                MESSAGE_FRAGMENT extends CommonMessageFragment,
                                VIEW_DATA        extends CommonViewData,
                                     
                                FRONTEND extends CommonFrontend<ROOT_VIEW, 
                                                                SETTINGS_VIEW, 
                                                                CARDS_VIEW, 
                                                                DASHBOARD_VIEW, 
                                                                MESSAGES_VIEW,
                                                                MESSAGE_FRAGMENT,
                                                                VIEW_DATA,
                                                                CANVAS_MODEL,
                                                                DASHBOARD_MODEL,
                                                                SETTINGS_MODEL>>

                implements NtroAppFx {

	static {

    	
    	NtroAppFx.setOptions(options -> {
    		
    		options.useJarResources(true);

    		options.setCoreOptions(coreOptions -> {
    			coreOptions.displayTasks(false);
    			coreOptions.displayWarnings(false);
    		});
    	});
    }

                                                            	  
    public CommonApp() {

    }

	@Override
	public void registerModels(ModelRegistrar registrar) {
		registrar.registerValue(executableModelClass());
		registrar.registerValue(studentModelClass());
		registrar.registerModel(canvasModelClass());

		registrar.registerModel(dashboardModelClass());
		registrar.registerModel(settingsModelClass());

		registrar.registerValue(testCasesModelClass());
		registrar.registerValue(testCaseClass());

		registrar.registerValue(Carte.class);
		registrar.registerValue(AbstractCard.class);

		registrar.registerValue(CommonTestCaseDescriptor.class);
		registrar.registerValue(TestCaseById.class);
		registrar.registerValue(TestCasesByCategory.class);
		registrar.registerValue(TestCasesBySize.class);

		registrar.registerValue(CommonExecutionTraceFull.class);
		registrar.registerValue(CommonExecutionTraceSizeOnly.class);
		
		registrar.registerValue(AddCommand.class);
		registrar.registerValue(ClearCommand.class);
		registrar.registerValue(DeleteCommand.class);
		registrar.registerValue(GetCommand.class);
		registrar.registerValue(InsertCommand.class);
		
		registerAdditionnalModels(registrar);
	}


	@Override
	public void registerMessages(MessageRegistrar registrar) {
		registrar.registerMessage(MsgToggleUseFourCardColors.class);
		registrar.registerMessage(MsgRefreshDashboard.class);
		registrar.registerMessage(MsgStopExecutionReplay.class);
		registrar.registerMessage(MsgStartExecutionEngine.class);
		registrar.registerMessage(MsgMessageToUser.class);

		registerAdditionnalMessages(registrar);
	}

	@Override
	public void registerFrontend(FrontendRegistrarFx registrar) {
		FRONTEND frontend = createFrontend();
		
		frontend.setCanvasModelClass(canvasModelClass());
		frontend.setDashboardModelClass(dashboardModelClass());
		frontend.setSettingsModelClass(settingsModelClass());
		
		registrar.registerFrontendObject(frontend);
	}



	@Override
	public void registerBackend(BackendRegistrar registrar) {
		BackendRegistrarNtro registrarNtro = (BackendRegistrarNtro) registrar;

		BACKEND backend = createBackend();
		
		backend.setExecutableModelClass(executableModelClass());
		backend.setStudentModelClass(studentModelClass());
		backend.setCanvasModelClass(canvasModelClass());
		backend.setTestCaseClass(testCaseClass());
		backend.setTestCasesModelClass(testCasesModelClass());
		backend.setDashboardModelClass(dashboardModelClass());
		backend.setSettingsModelClass(settingsModelClass());
		backend.setExecutionTraceFullClass(executionTraceClass());
		
		backend.initializeTestCaseDatabase();
		backend.earlyModelInitialization(initialTestCaseId(), initialAttempt());
		
		additionnalBackendInitialization(backend);
		
		registrarNtro.registerBackendObject(backend);
	}

	protected abstract void additionnalBackendInitialization(BACKEND backend);

	protected abstract Class<EXECUTABLE_MODEL> executableModelClass();
	protected abstract Class<STUDENT_MODEL> studentModelClass();
	protected abstract Class<CANVAS_MODEL> canvasModelClass();
	protected abstract Class<TEST_CASE> testCaseClass();
	protected abstract Class<TEST_CASE_DATABASE> testCasesModelClass();
	protected abstract Class<DASHBOARD_MODEL> dashboardModelClass();
	protected abstract Class<SETTINGS_MODEL> settingsModelClass();

	protected abstract Class<? extends EXECUTION_TRACE> executionTraceClass();

	protected abstract void registerAdditionnalModels(ModelRegistrar registrar);
	protected abstract void registerAdditionnalMessages(MessageRegistrar registrar);

	protected abstract FRONTEND createFrontend();
	protected abstract BACKEND createBackend();
	
	protected abstract String initialTestCaseId();
	protected abstract Attempt initialAttempt();


}

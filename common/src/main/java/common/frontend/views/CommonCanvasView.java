/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common.frontend.views;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewFx;
import ca.ntro.app.fx.controls.ResizableWorld2dCanvasFx;
import ca.ntro.app.fx.controls.World2dMouseEventFx;
import common.frontend.events.EvtMoveViewport;
import common.frontend.events.EvtResizeViewport;
import common.frontend.events.MouseEvtOnMainCanvas;
import common.models.world2d.CommonDrawingOptions;
import common.models.world2d.CommonWorld2d;
import javafx.application.Platform;
import javafx.scene.control.SplitPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;

public abstract class CommonCanvasView extends ViewFx {
	
	protected abstract ResizableWorld2dCanvasFx mainCanvas();

	protected abstract Pane dashboardContainer();

	protected abstract Pane cardsViewContainer();

	protected abstract SplitPane mainSplitPane();
	
	protected abstract double initialWorldHeight();
	protected abstract double initialWorldWidth();
	
	private double viewportScaleFactor = 1.0;
	private double viewportOffsetX = 0;
	private double viewportOffsetY = 0;
	
	protected double worldWidth() {
		return mainCanvas().getWorldWidth();
	}

	protected double worldHeight() {
		return mainCanvas().getWorldHeight();
	}
	
	@Override
	public void initialize() {
		if(mainSplitPane() != null) {
			mainSplitPane().getDividers().get(0).setPosition(0.8);
		}

		if(mainCanvas() != null) {
			initializeMainCanvas();
			installMouseEvtOnMainCanvas();
			installEvtResizeViewport();
		}
	
	}

	private void initializeMainCanvas() {
		mainCanvas().setFocusTraversable(true);

		Platform.runLater(() -> {
			mainCanvas().requestFocus();
		});
		
		mainCanvas().setWorldWidth(initialWorldWidth());
		mainCanvas().setWorldHeight(initialWorldHeight());
	}

	private void installMouseEvtOnMainCanvas() {
		MouseEvtOnMainCanvas mouseEvtOnViewer = Ntro.newEvent(MouseEvtOnMainCanvas.class);
		
		mainCanvas().onMouseEvent(world2dMouseEventFx -> {
			mouseEvtOnViewer.setMouseEvent(world2dMouseEventFx);
			mouseEvtOnViewer.trigger();
		});
	}

	private void installEvtResizeViewport() {
		EvtResizeViewport evtResizeViewport = Ntro.newEvent(EvtResizeViewport.class);

		mainCanvas().addEventFilter(ScrollEvent.ANY, evtFx -> {
			if(evtFx.getDeltaY() > 0) {

				evtResizeViewport.setFactor(0.9);
				evtResizeViewport.trigger();
				
			}else if(evtFx.getDeltaY() < 0) {

				evtResizeViewport.setFactor(1.1);
				evtResizeViewport.trigger();

			}
		});
	}

	public void drawWorld2d(CommonWorld2d world2d, CommonDrawingOptions options) {
		if(mainCanvas() != null) {
			world2d.drawOn(mainCanvas());
		}
	}

	public void clearCanvas() {
		if(mainCanvas() != null) {
			mainCanvas().clearCanvas();
		}
	}

	public void resizeViewport(double factor) {
		viewportScaleFactor *= factor;

		resizeRelocateViewport();
	}

	public void resetViewport() {
		viewportScaleFactor = 1;
		viewportOffsetX = 0;
		viewportOffsetY = 0;

		resizeRelocateViewport();
	}

	public void moveViewport(double incrementX, double incrementY) {
		viewportOffsetX += incrementX;
		viewportOffsetY += incrementY;

		resizeRelocateViewport();
	}


	public void displayDashboardView(CommonDashboardView dashboardView) {
		if(dashboardContainer() != null) {
			dashboardContainer().getChildren().clear();
			dashboardContainer().getChildren().add(dashboardView.rootNode());
		}
	}

	public void mouseEvtOnTabletop(World2dMouseEventFx world2dMouseEventFx) {
		MouseEvent evtFx = world2dMouseEventFx.mouseEventFx();
		double worldX = world2dMouseEventFx.worldX();
		double worldY = world2dMouseEventFx.worldY();
		
		if(evtFx.getEventType().equals(MouseEvent.MOUSE_CLICKED)
				|| evtFx.getEventType().equals(MouseEvent.MOUSE_DRAGGED)) {
			
			mainCanvas().relocateViewport(worldX - mainCanvas().getViewportWidth() / 2, 
					                      worldY - mainCanvas().getViewportHeight() / 2);
		}
	}

	public abstract void drawViewport();
	
	private void resizeRelocateViewport() {
		mainCanvas().relocateResizeViewport(viewportOffsetX, 
											viewportOffsetY, 
											mainCanvas().getWorldWidth() * viewportScaleFactor, 
											mainCanvas().getWorldHeight() * viewportScaleFactor);
	}

	public void resizeWorld2dRelocateViewport(double width, double height) {
		if(mainCanvas() != null) {
			mainCanvas().setWorldWidth(width);
			mainCanvas().setWorldHeight(height);

			resizeRelocateViewport();
		}
	}

	public void displayFps(String fps) {
		if(mainCanvas() != null) {
			mainCanvas().drawOnCanvas(gc -> {
				gc.fillText(fps, mainCanvas().getWidth() - 54, mainCanvas().getHeight() - 12);
			});
		}
	}
}

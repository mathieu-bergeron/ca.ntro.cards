/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common.frontend.views;


import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewFx;
import common.frontend.events.EvtMoveViewport;
import common.frontend.events.EvtResetViewport;
import common.frontend.events.EvtResizeViewport;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public abstract class CommonRootView extends ViewFx {
	
	private CommonSettingsView menuView;
	private CommonCanvasView canvasView;
	private CommonMessagesView messagesView;

	private EvtMoveViewport   evtMoveViewport   = Ntro.newEvent(EvtMoveViewport.class);
	private EvtResizeViewport evtResizeViewport = Ntro.newEvent(EvtResizeViewport.class);
	private EvtResetViewport  evtResetViewport  = Ntro.newEvent(EvtResetViewport.class);

	@Override
	public void initialize() {
		installRootKeyListener();
	}

	private void installRootKeyListener() {
		rootNode().addEventFilter(KeyEvent.ANY, keyEvent -> {
			if(keyEvent.getEventType().equals(KeyEvent.KEY_PRESSED)) {

				installerRootKeyPressedListener(keyEvent);
				
			}else {

				keyEvent.consume();

			}
		});
	}

	private void installerRootKeyPressedListener(KeyEvent keyEvent) {
		boolean consumed = false;
		
		consumed = triggerEvtMoveViewportIfNeeded(evtMoveViewport, keyEvent);
		
		if(!consumed) {
			consumed = triggerEvtResizeViewportIfNeeded(evtResizeViewport, keyEvent);
		}

		if(!consumed) {
			consumed = triggerEvtResetViewportIfNeeded(evtResetViewport, keyEvent);
		}
		
		
		consumed = onKeyEventNotConsumed(keyEvent);
		
		if(consumed) {
			keyEvent.consume();
		}
	}
	
	protected abstract boolean onKeyEventNotConsumed(KeyEvent keyEvent);


	private boolean triggerEvtResetViewportIfNeeded(EvtResetViewport evtResetViewport, KeyEvent keyEvent) {
		boolean consumed = false;

		if(keyEvent.getCode().equals(KeyCode.DIGIT0)) {

			evtResetViewport.trigger();
			consumed = true;
		}

		return consumed;
	}

	private boolean triggerEvtResizeViewportIfNeeded(EvtResizeViewport evtResizeViewport, KeyEvent keyEvent) {
		boolean consumed = false;

		if(keyEvent.getCode().equals(KeyCode.PLUS)
				|| keyEvent.getCode().equals(KeyCode.EQUALS)
				|| keyEvent.getCode().equals(KeyCode.Q)) {

			evtResizeViewport.setFactor(0.9);
			evtResizeViewport.trigger();
			consumed = true;
			
		} else if(keyEvent.getCode().equals(KeyCode.MINUS)
				|| keyEvent.getCode().equals(KeyCode.E)) {

			evtResizeViewport.setFactor(1.1);
			evtResizeViewport.trigger();
			consumed = true;
		}
		
		return consumed;
	}

	private boolean triggerEvtMoveViewportIfNeeded(EvtMoveViewport evtMoveViewport, KeyEvent keyEvent) {
		boolean consumed = false;

		if(keyEvent.getCode().equals(KeyCode.W)) {
			
			evtMoveViewport.setIncrementX(0);
			evtMoveViewport.setIncrementY(+10);
			evtMoveViewport.trigger();
			consumed = true;

		} else if(keyEvent.getCode().equals(KeyCode.S)) {
			
			evtMoveViewport.setIncrementX(0);
			evtMoveViewport.setIncrementY(-10);
			evtMoveViewport.trigger();
			consumed = true;

		} else if(keyEvent.getCode().equals(KeyCode.A)) {
			
			evtMoveViewport.setIncrementX(+10);
			evtMoveViewport.setIncrementY(0);
			evtMoveViewport.trigger();
			consumed = true;

		} else if(keyEvent.getCode().equals(KeyCode.D)) {
			
			evtMoveViewport.setIncrementX(-10);
			evtMoveViewport.setIncrementY(0);
			evtMoveViewport.trigger();
			consumed = true;
		}
		
		return consumed;
	}

	public void registerSettingsView(CommonSettingsView menuView) {
		this.menuView = menuView;
	}
	
	public void registerCanvasView(CommonCanvasView canvasView) {
		this.canvasView = canvasView;
	}
	
	public void installSubViews() {
		rootNode().getChildren().add(canvasView.rootNode());
		rootNode().getChildren().add(menuView.rootNode());
		rootNode().getChildren().add(messagesView.rootNode());

		hideMenu();
		hideMessages();
	}

	public void showMenu() {
		menuView.rootNode().setVisible(true);
	}

	public void hideMenu() {
		menuView.rootNode().setVisible(false);
	}

	public void showMessages() {
		messagesView.rootNode().setVisible(true);
	}

	public void hideMessages() {
		messagesView.rootNode().setVisible(false);
	}

	public void registerMessagesView(CommonMessagesView messagesView) {
		this.messagesView = messagesView;
	}

}

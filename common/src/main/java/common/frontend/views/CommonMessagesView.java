/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common.frontend.views;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewFx;
import ca.ntro.core.stream.Stream;
import common.frontend.events.EvtHideMessages;
import common.frontend.views.fragments.CommonMessageFragment;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public abstract class CommonMessagesView extends ViewFx {
	
	protected abstract Stream<Pane> spaces();

	protected abstract Pane messagesContainer();

	@Override
	public void initialize() {

		initializeSpaces();
	}

	private void initializeSpaces() {
		EvtHideMessages evtHideMessages = Ntro.newEvent(EvtHideMessages.class);
		
		if(spaces() != null) {
			spaces().forEach(space -> {
				space.addEventFilter(MouseEvent.MOUSE_CLICKED, evtFx -> {
					evtHideMessages.trigger();
				});
			});
		}
	}

	public void addMessage(CommonMessageFragment messageFragment) {
		messagesContainer().getChildren().add(messageFragment.rootNode());
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common.frontend;

import ca.ntro.app.Ntro;
import ca.ntro.app.events.EventRegistrar;
import ca.ntro.app.frontend.FrontendFx;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.session.SessionRegistrar;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import common.frontend.events.EvtHideMenu;
import common.frontend.events.EvtHideMessages;
import common.frontend.events.EvtMoveViewport;
import common.frontend.events.EvtQuit;
import common.frontend.events.EvtResetViewport;
import common.frontend.events.EvtResizeViewport;
import common.frontend.events.EvtShowMenu;
import common.frontend.events.EvtShowMessages;
import common.frontend.events.MouseEvtOnMainCanvas;
import common.frontend.tasks.Dashboard;
import common.frontend.tasks.Initialization;
import common.frontend.tasks.Messages;
import common.frontend.tasks.Navigation;
import common.frontend.tasks.Settings;
import common.frontend.tasks.ViewData;
import common.frontend.tasks.ViewDataInitializer;
import common.frontend.views.CommonCanvasView;
import common.frontend.views.CommonDashboardView;
import common.frontend.views.CommonMessagesView;
import common.frontend.views.CommonRootView;
import common.frontend.views.CommonSettingsView;
import common.frontend.views.fragments.CommonMessageFragment;
import common.models.CommonCanvasModel;
import common.models.CommonDashboardModel;
import common.models.CommonSettingsModel;

public abstract class CommonFrontend<ROOT_VIEW        extends CommonRootView, 
                                     SETTINGS_VIEW    extends CommonSettingsView,
                                     CANVAS_VIEW      extends CommonCanvasView, 
                                     DASHBOARD_VIEW   extends CommonDashboardView,
                                     MESSAGES_VIEW    extends CommonMessagesView,
                                     MESSAGE_FRAGMENT extends CommonMessageFragment,
                                     VIEW_DATA        extends CommonViewData,
                                     CANVAS_MODEL     extends CommonCanvasModel,
                                     DASHBOARD_MODEL  extends CommonDashboardModel,
                                     SETTINGS_MODEL   extends CommonSettingsModel>

                 implements FrontendFx {

	private Class<CANVAS_MODEL>    canvasModelClass;
	private Class<DASHBOARD_MODEL> dashboardModelClass;
	private Class<SETTINGS_MODEL>  settingsModelClass;

	public Class<CANVAS_MODEL> getCanvasModelClass() {
		return canvasModelClass;
	}

	public void setCanvasModelClass(Class<CANVAS_MODEL> canvasModelClass) {
		this.canvasModelClass = canvasModelClass;
	}

	public Class<DASHBOARD_MODEL> getDashboardModelClass() {
		return dashboardModelClass;
	}

	public void setDashboardModelClass(Class<DASHBOARD_MODEL> dashboardModelClass) {
		this.dashboardModelClass = dashboardModelClass;
	}

	public Class<SETTINGS_MODEL> getSettingsModelClass() {
		return settingsModelClass;
	}

	public void setSettingsModelClass(Class<SETTINGS_MODEL> settingsModelClass) {
		this.settingsModelClass = settingsModelClass;
	}

	@Override
	public void registerEvents(EventRegistrar registrar) {
		registrar.registerEvent(EvtMoveViewport.class);
		registrar.registerEvent(EvtResizeViewport.class);
		registrar.registerEvent(EvtResetViewport.class);
		registrar.registerEvent(MouseEvtOnMainCanvas.class);

		registrar.registerEvent(EvtShowMenu.class);
		registrar.registerEvent(EvtHideMenu.class);
		registrar.registerEvent(EvtShowMessages.class);
		registrar.registerEvent(EvtHideMessages.class);
		registrar.registerEvent(EvtQuit.class);
		

		registerAdditionnalEvents(registrar);
	}

	protected abstract void registerAdditionnalEvents(EventRegistrar registrar);

	protected abstract boolean isProd();
	
	protected abstract String stringsFrPath();
	protected abstract String stringsEnPath();

	protected abstract String cssProdPath();
	protected abstract String cssDevPath();
	
	protected abstract Class<ROOT_VIEW>      rootViewClass();
	protected abstract Class<SETTINGS_VIEW>  settingsViewClass();
	protected abstract Class<CANVAS_VIEW>    canvasViewClass();
	protected abstract Class<DASHBOARD_VIEW> dashboardViewClass();
	protected abstract Class<MESSAGES_VIEW>   messagesViewClass();
	protected abstract Class<MESSAGE_FRAGMENT> messageFragmentClass();
	
	protected abstract String rootViewXmlPath();
	protected abstract String settingsViewXmlPath();
	protected abstract String canvasViewXmlPath();
	protected abstract String dashboardViewXmlPath();
	protected abstract String messagesViewXmlPath();
	protected abstract String messageFragmentXmlPath();

	@Override
	public void registerViews(ViewRegistrarFx registrar) {
		registrar.registerDefaultLocale(Ntro.buildLocale("fr"));
		registrar.registerTranslations(Ntro.buildLocale("fr"), stringsFrPath());
		registrar.registerTranslations(Ntro.buildLocale("en"), stringsEnPath());
		
		if(isProd()) {
			registrar.registerStylesheet(cssDevPath());
		}else {
		    registrar.registerStylesheet(cssProdPath());
		}

		registrar.registerView(rootViewClass(), rootViewXmlPath());
		registrar.registerView(settingsViewClass(), settingsViewXmlPath());
		registrar.registerView(canvasViewClass(), canvasViewXmlPath());
		registrar.registerView(dashboardViewClass(), dashboardViewXmlPath());
		registrar.registerView(messagesViewClass(), messagesViewXmlPath());
		registrar.registerView(messageFragmentClass(), messageFragmentXmlPath());

		registerAdditionnalViews(registrar);
	}

	protected abstract void registerAdditionnalViews(ViewRegistrarFx registrar);
	
	protected abstract Class<VIEW_DATA> viewDataClass();

	@Override
	public void createTasks(FrontendTasks tasks) {

		Initialization.createTasks(tasks,
				                   rootViewClass(),
				                   canvasViewClass(),
				                   settingsViewClass(),
				                   messagesViewClass(),
				                   dashboardViewClass(),
				                   subTasks -> {
				                	   
				                	   addDashboardSubViewLoaders(subTasks);
				                	   
				                   },
				                   taskCreator -> {
				                	   
				                	   installDashboardSubViews(taskCreator);
				                	   
				                   },
				                   subTasks -> {

				                	   addSubTasksToInitialization(subTasks);
				                	   
				                   });

		ViewData.createTasks(tasks, 
				             canvasViewClass(),
				             viewDataClass(),
				             canvasModelClass,
				             settingsModelClass,
				             dashboardViewClass(),
				             viewData -> {
				            	 initializeViewData(viewData);
				             },
				             subTasks -> {
								  addSubTasksToViewData(subTasks);
				             });
				        	  

		Navigation.createTasks(tasks, 
				               rootViewClass(),
				               subTasks -> {
				            	   
				            	   addSubTasksToNavigation(subTasks);
				            	   
				               });

		Dashboard.createTasks(tasks, 
				              dashboardViewClass(),
				              dashboardModelClass,

				             subTasks -> {
				            	 
				            	 addSubTasksToDashboard(subTasks);
				            	 
				             });
		
		Settings.createTasks(tasks, 
				             settingsViewClass(), 
				             settingsModelClass,
				             subTasks -> {
				            	 
				            	 addSubTasksToSettings(subTasks);
				            	 
				             });

		Messages.createTasks(tasks, 
				             messagesViewClass(), 
				             messageFragmentClass(),
				             subTasks -> {
				            	 
				            	 addSubTasksToMessages(subTasks);
				            	 
				             });
		
		createAdditionnalTasks(tasks);

	}

	protected abstract void initializeViewData(VIEW_DATA viewData);

	protected abstract void addDashboardSubViewLoaders(FrontendTasks subTasks);

	protected abstract void installDashboardSubViews(SimpleTaskCreator<?> taskCreator);

	protected abstract void addSubTasksToInitialization(FrontendTasks subTasks);

	protected abstract void addSubTasksToViewData(FrontendTasks subTasks);

	protected abstract void addSubTasksToNavigation(FrontendTasks subTasks);

	protected abstract void addSubTasksToSettings(FrontendTasks subTasks);

	protected abstract void addSubTasksToMessages(FrontendTasks subTasks);

	protected abstract void addSubTasksToDashboard(FrontendTasks subTasks);

	protected abstract void createAdditionnalTasks(FrontendTasks tasks);

	protected abstract Class<? extends CommonSession> sessionClass();


	@Override
	public void registerSessionClass(SessionRegistrar registrar) {
		registrar.registerSessionClass(sessionClass());
	}

}

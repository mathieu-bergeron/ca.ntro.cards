package common.frontend;
import ca.ntro.app.session.Session;
import common.models.enums.Attempt;

public class CommonSession<S extends CommonSession<?>> extends Session<S> {
	
	public static String defaultCanvasModelId(String testCaseId, Attempt attempt) {
		String modelId = testCaseId;
		
		if(modelId != null
				&& attempt != null) {
			
			modelId += "-" + attempt.name().toLowerCase();
			
		}
		
		return modelId;
	}
	

}

package common.frontend.tasks;

import common.frontend.CommonViewData;

public interface ViewDataInitializer<VIEW_DATA extends CommonViewData> {
	
	void initializeViewData(VIEW_DATA viewData);

}

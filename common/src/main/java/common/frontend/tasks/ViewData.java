/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package common.frontend.tasks;

import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.core.NtroCore;
import common.frontend.CommonViewData;
import common.frontend.events.EvtMoveViewport;
import common.frontend.events.EvtResetViewport;
import common.frontend.events.EvtResizeViewport;
import common.frontend.events.MouseEvtOnMainCanvas;
import common.frontend.views.CommonCanvasView;
import common.frontend.views.CommonDashboardView;
import common.models.CommonCanvasModel;
import common.models.CommonSettingsModel;
import common.models.world2d.CommonDrawingOptions;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import ca.ntro.app.frontend.Tick;
import ca.ntro.app.modified.Modified;
import ca.ntro.app.tasks.SubTasksLambda;

public class ViewData {

	public static <CANVAS_VIEW    extends CommonCanvasView,
	               VIEW_DATA      extends CommonViewData,
	               CARDS_MODEL    extends CommonCanvasModel,
	               SETTINGS_MODEL extends CommonSettingsModel,
	               DASHBOARD_VIEW extends CommonDashboardView> 
	
	       void createTasks(FrontendTasks tasks, 
	    		            Class<CANVAS_VIEW>             canvasViewClass,
	    		            Class<VIEW_DATA>               viewDataClass,
	    		            Class<CARDS_MODEL>             cardsModelClass,
	    		            Class<SETTINGS_MODEL>          settingsModelClass,
	    		            Class<DASHBOARD_VIEW>          dashboardViewClass,
	    		            ViewDataInitializer<VIEW_DATA> viewDataInitializer,
	    		            SubTasksLambda<FrontendTasks>  subTaskLambda) {

		createViewData(tasks, viewDataClass, viewDataInitializer);
		
		tasks.taskGroup("ViewData")

		     .waitsFor(created(viewDataClass))

		     .contains(subTasks -> {

		    	 timePasses(subTasks,
		    			    viewDataClass,
		    			    canvasViewClass,
		    			    dashboardViewClass);

		    	 observeSettings(subTasks,
		    			         viewDataClass,
		    			         settingsModelClass);
		    	 
		    	 moveViewport(subTasks,
		    			      canvasViewClass);

		    	 resizeViewport(subTasks,
		    			        canvasViewClass);

		    	 resetViewport(subTasks,
		    			       canvasViewClass);

		    	 mouseEvtOnViewer(subTasks,
		    			          viewDataClass);

		    	 mouseEvtOnTabletop(subTasks,
		    			            canvasViewClass);
		    	 
		    	 observeCanvasModel(subTasks,
		    			            viewDataClass,
		    			            cardsModelClass);
		    	 
		    	 subTaskLambda.createSubTasks(subTasks);

		     });
	}

	private  static <VIEW_DATA extends CommonViewData> void createViewData(FrontendTasks tasks, 
			                                                               Class<VIEW_DATA> cardsViewDataClass,
			                                                               ViewDataInitializer<VIEW_DATA> initializer) {

		tasks.task(create(cardsViewDataClass))
		
		     .waitsFor("Initialization")
		
		     .executesAndReturnsValue(inputs -> {
		    	 
		    	 VIEW_DATA viewData = NtroCore.factory().newInstance(cardsViewDataClass);
		    	 
		    	 initializer.initializeViewData(viewData);

		    	 return viewData;
		     });
	}
	
	private static <VIEW_DATA extends CommonViewData,
	                CARDS_VIEW extends CommonCanvasView,
	                DASHBOARD_VIEW extends CommonDashboardView> 
	
	        void timePasses(FrontendTasks tasks,
	        		              Class<VIEW_DATA>      viewDataClass,
	        		              Class<CARDS_VIEW>     cardsViewClass,
	        		              Class<DASHBOARD_VIEW> dashboardViewClass) {

		tasks.task("timePasses")
		
		      .waitsFor(clock().nextTick())

		      .waitsFor(created(viewDataClass))
		      .waitsFor(created(cardsViewClass))
		      .waitsFor(created(dashboardViewClass))
		      
		      .executes(inputs -> {
		    	  
		    	  Tick            tick          = inputs.get(clock().nextTick());
		    	  VIEW_DATA       viewData      = inputs.get(created(viewDataClass));
		    	  CARDS_VIEW      cardsView     = inputs.get(created(cardsViewClass));
		    	  DASHBOARD_VIEW  dashboardView = inputs.get(created(dashboardViewClass));
		    	  
		    	  viewData.onTimePasses(tick.elapsedTime());
		    	  viewData.displayOn(cardsView, dashboardView);

		      });
	}


	private static <VIEW_DATA extends CommonViewData,
	                SETTINGS_MODEL extends CommonSettingsModel,
	                OPTIONS extends CommonDrawingOptions> 
	
	        void observeSettings(FrontendTasks tasks,
	        		             Class<VIEW_DATA> cardsViewDataClass,
	        		             Class<SETTINGS_MODEL> settingsModelClass) {

		tasks.task("observeSettings")
		
		      .waitsFor(modified(settingsModelClass))

		      .waitsFor(created(cardsViewDataClass))
		      
		      .executes(inputs -> {
		    	  
		    	  VIEW_DATA                 viewData         = inputs.get(created(cardsViewDataClass));
		    	  Modified<SETTINGS_MODEL>  modifiedSettings = inputs.get(modified(settingsModelClass));
		    	  
		    	  viewData.setDrawingOptions(modifiedSettings.currentValue().drawingOptions());

		      });
	}

	private static <CARDS_VIEW extends CommonCanvasView> void moveViewport(FrontendTasks tasks,
			                                                        Class<CARDS_VIEW> cardsViewClass) {

		tasks.task("moveViewport")
		
		      .waitsFor(event(EvtMoveViewport.class))

		      .waitsFor(created(cardsViewClass))
		      
		      .executes(inputs -> {
		    	  
		    	  EvtMoveViewport evtMoveViewport = inputs.get(event(EvtMoveViewport.class));
		    	  CARDS_VIEW      cardsView       = inputs.get(created(cardsViewClass));
		    	  
		    	  evtMoveViewport.applyTo(cardsView);
		      });
	}

	private static <CARDS_VIEW extends CommonCanvasView> void resizeViewport(FrontendTasks tasks,
			                                                          Class<CARDS_VIEW> cardsViewClass) {
		tasks.task("resizeViewport")
		
		      .waitsFor(event(EvtResizeViewport.class))

		      .waitsFor(created(cardsViewClass))
		      
		      .executes(inputs -> {
		    	  
		    	  EvtResizeViewport evtResizeViewport = inputs.get(event(EvtResizeViewport.class));
		    	  CARDS_VIEW        cardsView         = inputs.get(created(cardsViewClass));
		    	  
		    	  evtResizeViewport.applyTo(cardsView);
		      });
	}

	private static <CARDS_VIEW extends CommonCanvasView> void resetViewport(FrontendTasks tasks,
			                                                                Class<CARDS_VIEW> cardsViewClass) {
		tasks.task("resetViewport")
		
		      .waitsFor(event(EvtResetViewport.class))

		      .waitsFor(created(cardsViewClass))
		      
		      .executes(inputs -> {
		    	  
		    	  EvtResetViewport evtResetViewport = inputs.get(event(EvtResetViewport.class));
		    	  CARDS_VIEW       cardsView        = inputs.get(created(cardsViewClass));
		    	  
		    	  evtResetViewport.applyTo(cardsView);
		      });
	}

	private static <CARDS_VIEW_DATA extends CommonViewData> void mouseEvtOnViewer(FrontendTasks tasks,
			                                                                     Class<CARDS_VIEW_DATA> cardsViewDataClass) {
		tasks.task("mouseEvtOnViewer")
		
		      .waitsFor(event(MouseEvtOnMainCanvas.class))
		      
		      .waitsFor(created(cardsViewDataClass))
		          
		      .executes(inputs -> {
		    	  
		    	  MouseEvtOnMainCanvas mouseEvtOnViewer = inputs.get(event(MouseEvtOnMainCanvas.class));
		    	  CARDS_VIEW_DATA      cardsViewData    = inputs.get(created(cardsViewDataClass));
		    	  
		    	  mouseEvtOnViewer.applyTo(cardsViewData);
		      });
	}


	private static <CARDS_VIEW extends CommonCanvasView> void mouseEvtOnTabletop(FrontendTasks tasks,
			                                                              Class<CARDS_VIEW> cardsViewClass) {
	}


	private static <VIEW_DATA    extends CommonViewData,
	                CANVAS_MODEL extends CommonCanvasModel> 
	
	        void observeCanvasModel(FrontendTasks tasks,
	        		                Class<VIEW_DATA> cardsViewDataClass,
	        		                Class<CANVAS_MODEL> canvasModelClass) {

		tasks.task("observeCanvasModel")
		
		      .waitsFor(modified(canvasModelClass))
		      
		      .waitsFor(created(cardsViewDataClass))
		      
		      .executes(inputs -> {
		    	  
		    	  VIEW_DATA              viewData      = inputs.get(created(cardsViewDataClass));
		    	  Modified<CANVAS_MODEL> modifiedModel = inputs.get(modified(canvasModelClass));

		    	  CANVAS_MODEL canvasModel = modifiedModel.currentValue();
		    	  
		    	  canvasModel.updateViewData(viewData);

		      });
	}
}

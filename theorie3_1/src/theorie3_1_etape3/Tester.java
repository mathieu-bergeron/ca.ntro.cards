/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package theorie3_1_etape3;

public class Tester {
	
	public static void main(String[] args) {
		
		/*
		Equipe equipe = new MonEquipe();
		
		equipe.ajouterAthlete(new MonAthlete("Walid"));
		equipe.ajouterAthlete(new MonAthlete("Rosa"));
		equipe.ajouterAthlete(new MonAthlete("Robson"));
		equipe.ajouterAthlete(new MonAthlete("Lili"));
		
		System.out.println("plusUtile?: " + equipe.plusUtile());
		*/
		
		EquipeHockey equipeHockey = new MonEquipeHockey();
		equipeHockey.ajouterAthlete(new MonAthleteHockey("Bob", 120));
		equipeHockey.ajouterAthlete(new MonAthleteHockey("Lise", 2));
		//equipeHockey.ajouterAthlete(new MonAthleteSoccer("Lili", 3));
		System.out.println("plusUtile hockey: " + equipeHockey.plusUtile());
		
		EquipeSoccer equipeSoccer = new MonEquipeSoccer();
		equipeSoccer.ajouterAthlete(new MonAthleteSoccer("Roland", 40));
		equipeSoccer.ajouterAthlete(new MonAthleteSoccer("Jeff", 1));
		//equipeSoccer.ajouterAthlete(new MonAthleteHockey("Lili", 0));
		System.out.println("plusUtile soccer: " + equipeSoccer.plusUtile());
		
		
		
	}

}

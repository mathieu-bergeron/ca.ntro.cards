/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package fibonacci_efficiency;

import common.models.enums.Attempt;
import common_efficiency.EfficiencyApp;
import fibonacci_efficiency.backend.FibonacciEfficiencyBackend;
import fibonacci_efficiency.frontend.FibonacciEfficiencyFrontend;
import fibonacci_efficiency.frontend.FibonacciEfficiencyViewData;
import fibonacci_efficiency.frontend.views.FibonacciEfficiencyDashboardView;
import fibonacci_efficiency.frontend.views.FibonacciEfficiencyMessagesView;
import fibonacci_efficiency.frontend.views.FibonacciEfficiencyRootView;
import fibonacci_efficiency.frontend.views.FibonacciEfficiencySettingsView;
import fibonacci_efficiency.frontend.views.FibonacciGraphsView;
import fibonacci_efficiency.frontend.views.fragments.FibonacciEfficiencyMessageFragment;
import fibonacci_efficiency.models.FibonacciEfficiencyDashboardModel;
import fibonacci_efficiency.models.FibonacciEfficiencySettingsModel;
import fibonacci_efficiency.models.FibonacciGraphsModel;
import fibonacci_procedure.models.Calculateur;
import fibonacci_procedure.models.values.FibonacciTestCase;
import fibonacci_procedure.test_cases.FibonacciTestCaseDatabase;
import fibonacci_procedure.test_cases.execution_trace.FibonacciExecutionTrace;

public abstract class   FibonacciEfficiencyApp<STUDENT_MODEL extends Calculateur>

                extends EfficiencyApp<Calculateur, 
                                      STUDENT_MODEL,
                                      FibonacciGraphsModel,
                                      FibonacciTestCase,
                                      FibonacciTestCaseDatabase,
                                      FibonacciExecutionTrace,
                                      FibonacciEfficiencyDashboardModel,
                                      FibonacciEfficiencySettingsModel,
                                      FibonacciEfficiencyBackend<STUDENT_MODEL>,
                                      FibonacciEfficiencyRootView,
                                      FibonacciGraphsView,
                                      FibonacciEfficiencyDashboardView,
                                      FibonacciEfficiencySettingsView,
                                      FibonacciEfficiencyMessagesView,
                                      FibonacciEfficiencyMessageFragment,
                                      FibonacciEfficiencyViewData,
                                      FibonacciEfficiencyFrontend> {

	@Override
	protected FibonacciEfficiencyFrontend createFrontend() {
		return new FibonacciEfficiencyFrontend();
	}

	@Override
	protected FibonacciEfficiencyBackend createBackend() {
		return new FibonacciEfficiencyBackend();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class<Calculateur> executableModelClass() {
		return Calculateur.class;
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return studentClass();
	}
	
	// TODO: renommer
	protected abstract Class<STUDENT_MODEL> studentClass();

	@Override
	protected Class<FibonacciGraphsModel> canvasModelClass() {
		return FibonacciGraphsModel.class;
	}

	@Override
	protected Class<FibonacciTestCase> testCaseClass() {
		return FibonacciTestCase.class;
	}

	@Override
	protected Class<FibonacciTestCaseDatabase> testCasesModelClass() {
		return FibonacciTestCaseDatabase.class;
	}

	@Override
	protected Class<FibonacciEfficiencyDashboardModel> dashboardModelClass() {
		return FibonacciEfficiencyDashboardModel.class;
	}

	@Override
	protected Class<FibonacciEfficiencySettingsModel> settingsModelClass() {
		return FibonacciEfficiencySettingsModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}

	@Override
	protected Attempt initialAttempt() {
		return Attempt.SOLUTION;
	}
}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package factorial_procedure;

import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import common_procedure.ProcedureApp;
import common_procedure.messages.ProcedureMsgAcceptManualModel;
import common_procedure.test_cases.descriptor.ProcedureTestCaseDescriptor;
import factorial_procedure.backend.FactorialProcedureBackend;
import factorial_procedure.frontend.FactorialProcedureFrontend;
import factorial_procedure.frontend.FactorialProcedureViewData;
import factorial_procedure.frontend.views.FactorialCardsView;
import factorial_procedure.frontend.views.FactorialProcedureDashboardView;
import factorial_procedure.frontend.views.FactorialProcedureMessagesView;
import factorial_procedure.frontend.views.FactorialProcedureRootView;
import factorial_procedure.frontend.views.FactorialProcedureSettingsView;
import factorial_procedure.frontend.views.fragments.FactorialProcedureMessageFragment;
import factorial_procedure.messages.FactorialMsgAcceptManualModel;
import factorial_procedure.models.Calculateur;
import factorial_procedure.models.FactorialProcedureDashboardModel;
import factorial_procedure.models.FactorialProcedureSettingsModel;
import factorial_procedure.models.Factoriel;
import factorial_procedure.models.values.FactorialTestCase;
import factorial_procedure.test_cases.FactorialTestCaseDatabase;
import factorial_procedure.test_cases.descriptor.FactorialTestCaseDescriptor;
import factorial_procedure.test_cases.execution_trace.FactorialExecutionTrace;

public abstract class   FactorialProcedureApp<STUDENT_MODEL extends Calculateur,
                                              STUDENT_FACTORIAL extends Factoriel>

                extends ProcedureApp<Calculateur,           // executable model
                                     STUDENT_MODEL,
                                     STUDENT_MODEL,     // canvas model
                                     FactorialTestCase,
                                     FactorialTestCaseDescriptor,
                                     FactorialTestCaseDatabase,
                                     FactorialExecutionTrace,
                                     FactorialProcedureDashboardModel,
                                     FactorialProcedureSettingsModel,
                                     FactorialMsgAcceptManualModel,
                                     FactorialProcedureBackend<STUDENT_MODEL>,
                                     FactorialProcedureRootView,
                                     FactorialCardsView,
                                     FactorialProcedureDashboardView,
                                     FactorialProcedureSettingsView,
                                     FactorialProcedureMessagesView,
                                     FactorialProcedureMessageFragment,
                                     FactorialProcedureViewData,
                                     FactorialProcedureFrontend<STUDENT_MODEL>> {

	@Override
	protected Class<Calculateur> executableModelClass() {
		return Calculateur.class;
	}

	protected abstract Class<STUDENT_MODEL> classeMonCalculateur();
	protected abstract Class<STUDENT_FACTORIAL> classeMonFactoriel();

	@Override
	protected Class<FactorialTestCase> testCaseClass() {
		return FactorialTestCase.class;
	}

	@Override
	protected Class<FactorialTestCaseDatabase> testCasesModelClass() {
		return FactorialTestCaseDatabase.class;
	}


	@Override
	protected Class<FactorialProcedureDashboardModel> dashboardModelClass() {
		return FactorialProcedureDashboardModel.class;
	}


	@Override
	protected Class<FactorialProcedureSettingsModel> settingsModelClass() {
		return FactorialProcedureSettingsModel.class;
	}

	@Override
	protected FactorialProcedureFrontend createFrontend() {
		return new FactorialProcedureFrontend();
	}


	@Override
	protected FactorialProcedureBackend createBackend() {
		return new FactorialProcedureBackend();
	}




	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Class<STUDENT_MODEL> canvasModelClass() {
		return (Class<STUDENT_MODEL>) classeMonCalculateur();
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return classeMonCalculateur();
	}

	@Override
	protected Class<FactorialTestCaseDescriptor> testCaseDescriptorClass() {
		return FactorialTestCaseDescriptor.class;
	}

	@Override
	protected Class<FactorialMsgAcceptManualModel> msgAcceptManualModelClass() {
		return FactorialMsgAcceptManualModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "rec01";
	}

	@Override
	protected void registerAdditionnalModels(ModelRegistrar registrar) {
		super.registerAdditionnalModels(registrar);
		
		registrar.registerValue(classeMonFactoriel());
		registrar.registerValue(Factoriel.class);
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package factorial_procedure.frontend;

import ca.ntro.app.Ntro;
import common.messages.MsgMessageToUser;
import common_procedure.frontend.ProcedureFrontend;
import factorial_procedure.frontend.views.FactorialCardsView;
import factorial_procedure.frontend.views.FactorialProcedureDashboardView;
import factorial_procedure.frontend.views.FactorialProcedureMessagesView;
import factorial_procedure.frontend.views.FactorialProcedureRootView;
import factorial_procedure.frontend.views.FactorialProcedureSettingsView;
import factorial_procedure.frontend.views.FactorialReplayView;
import factorial_procedure.frontend.views.FactorialSelectionsView;
import factorial_procedure.frontend.views.FactorialVariablesView;
import factorial_procedure.frontend.views.fragments.FactorialProcedureMessageFragment;
import factorial_procedure.frontend.views.fragments.FactorialTestCaseFragment;
import factorial_procedure.models.Calculateur;
import factorial_procedure.models.FactorialProcedureDashboardModel;
import factorial_procedure.models.FactorialProcedureSettingsModel;

public class FactorialProcedureFrontend<STUDENT_MODEL extends Calculateur>

       extends ProcedureFrontend<FactorialProcedureRootView,
                                 FactorialProcedureSettingsView, 
                                 FactorialCardsView, 
                                 FactorialProcedureDashboardView, 
                                 FactorialSelectionsView,
                                 FactorialTestCaseFragment,
                                 FactorialReplayView,
                                 FactorialVariablesView,
                                 FactorialProcedureMessagesView,
                                 FactorialProcedureMessageFragment,
                                 FactorialProcedureViewData, 
                                 STUDENT_MODEL, // CanvasModel
                                 FactorialProcedureDashboardModel, 
                                 FactorialProcedureSettingsModel> {


	@Override
	protected boolean isProd() {
		return false;
	}

	@Override
	protected Class<FactorialProcedureRootView> rootViewClass() {
		return FactorialProcedureRootView.class;
	}

	@Override
	protected Class<FactorialProcedureSettingsView> settingsViewClass() {
		return FactorialProcedureSettingsView.class;
	}

	@Override
	protected Class<FactorialCardsView> canvasViewClass() {
		return FactorialCardsView.class;
	}

	@Override
	protected Class<FactorialProcedureDashboardView> dashboardViewClass() {
		return FactorialProcedureDashboardView.class;
	}


	@Override
	protected Class<FactorialProcedureViewData> viewDataClass() {
		return FactorialProcedureViewData.class;
	}



	@Override
	protected Class<FactorialSelectionsView> selectionsViewClass() {
		return FactorialSelectionsView.class;
	}

	@Override
	protected Class<FactorialReplayView> replayControlsViewClass() {
		return FactorialReplayView.class;
	}

	@Override
	protected Class<FactorialVariablesView> variablesViewClass() {
		return FactorialVariablesView.class;
	}


	@Override
	protected Class<FactorialTestCaseFragment> testCaseFragmentClass() {
		return FactorialTestCaseFragment.class;
	}

	@Override
	protected Class<FactorialProcedureMessagesView> messagesViewClass() {
		return FactorialProcedureMessagesView.class;
	}

	@Override
	protected Class<FactorialProcedureMessageFragment> messageFragmentClass() {
		return FactorialProcedureMessageFragment.class;
	}


}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package factorial_procedure.frontend;

import common.models.values.cards.AbstractCard;
import common_procedure.frontend.ProcedureViewData;
import common_procedure.models.world2d.ProcedureCard2d;
import factorial_procedure.models.world2d.FactorialCard2d;
import factorial_procedure.models.world2d.FactorialProcedureDrawingOptions;
import factorial_procedure.models.world2d.FactorialProcedureWorld2d;

public class FactorialProcedureViewData extends ProcedureViewData<FactorialProcedureWorld2d, FactorialProcedureDrawingOptions> {

	@Override
	protected FactorialProcedureWorld2d newWorld2d() {
		return new FactorialProcedureWorld2d();
	}

	@Override
	protected ProcedureCard2d newCard2d(String card2dId, AbstractCard card) {
		return new FactorialCard2d(card2dId, card);
	}

	@Override
	protected FactorialProcedureDrawingOptions defaultDrawingOptions() {
		return new FactorialProcedureDrawingOptions() {
			@Override
			public boolean useFourCardColors() {
				return true;
			}
		};
	}


}

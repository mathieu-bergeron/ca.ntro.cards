/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package factorial_procedure.models;


import java.util.HashSet;
import java.util.Set;

import ca.ntro.core.stream.Stream;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.stream.StreamNtro;
import ca.ntro.core.stream.Visitor;
import common.models.enums.Sorte;
import common.models.values.cards.AbstractCard;
import common.models.values.cards.CardHeap;
import common.models.values.cards.Carte;
import common.models.values.cards.NullCard;
import common.test_cases.descriptor.AbstractTestCaseDescriptor;
import common_procedure.models.ProcedureCardsModel;
import common_procedure.models.values.ComparisonReport;
import factorial_procedure.FactorialConstants;
import factorial_procedure.frontend.FactorialProcedureViewData;
import factorial_procedure.frontend.views.FactorialVariablesView;
import factorial_procedure.models.world2d.FactorialProcedureDrawingOptions;
import factorial_procedure.models.world2d.FactorialProcedureWorld2d;

public class   Calculateur<C extends Comparable<C>> 

       extends ProcedureCardsModel<Calculateur, 
                                   FactorialProcedureWorld2d, 
                                   FactorialProcedureDrawingOptions, 
                                   FactorialProcedureViewData,
                                   FactorialVariablesView> { 

	public static final int MARGIN_LEFT = 10;
	public static final double EPSILON = 0.001;

	protected boolean siRecursif = true;
	protected int n;
	protected Factoriel tete;

    @Override
    public void copyDataFrom(Calculateur other) {
    	this.siRecursif = other.siRecursif;
    	this.n = other.n;
    	if(other.tete != null) {
    		this.tete = (Factoriel) NtroCoreImpl.reflection().clone(other.tete);
    	}
    }

	@Override
	public boolean isValidNextStep(Calculateur manualModel) {
		boolean modified = false;

		// TODO: accepter ou rejeter les modifications manuelles
		//       retourner faux si c'est rejeté

		return modified;
	}

	@Override
	public ComparisonReport compareToSolution(Calculateur solution) {
		
		ComparisonReport report = ComparisonReport.emptyReport();
		
		// TODO: compare to a solution and report every error
		//       the testcase is passed if there is no error
		//       to report

		return report;
	}


    @Override
    protected void updateViewDataImpl(CardHeap cardHeap, FactorialProcedureViewData cardsViewData) {
    	double cardWidth = FactorialConstants.INITIAL_CARD_WIDTH_MILIMETERS;
		double cardHeight = FactorialConstants.INITIAL_CARD_HEIGHT_MILIMETERS;

		double maxX = MARGIN_LEFT + cardWidth + cardWidth * 3 * (n + 2) / 2;
		double maxY = 3 * cardHeight;

		int i = 0;
		Factoriel cursor = tete;
		Set<Factoriel> visited = new HashSet<>();

		while(cursor != null) {
			visited.add(cursor);

			AbstractCard card = null;

			if(cursor.reponse != null) {
				
				if(cursor.moinsUn != null
						&& cursor.reponse.equals(cursor.moinsUn.reponse)) {

					card = new Carte(cursor.reponse, Sorte.CARREAU);
					
				}else {

					card = new Carte(cursor.reponse, Sorte.COEUR);
				}
				
				
			}else {

				card = new NullCard();
			}

			double targetTopLeftX = MARGIN_LEFT + cardWidth + cardWidth / 2 + i * cardWidth * 3 / 2;
			double targetTopLeftY = cardHeight * 2;

			card = cardHeap.firstInstanceOf(card);
			String card2dId = cardHeap.newCard2dId(card);
			
			cardsViewData.addOrUpdateCard(card2dId,
										  card,
										  targetTopLeftX,
										  targetTopLeftY);
			
			cardsViewData.displayCardFaceUp(card2dId);

			if((targetTopLeftX + cardWidth) > maxX) {
				maxX = targetTopLeftX + cardWidth;
			}
			
			if((targetTopLeftY + cardHeight) > maxY) {
				maxY = targetTopLeftY + cardHeight;
			}

			if(!visited.contains(cursor.getMoinsUn())) {
				i++;
				cursor = cursor.getMoinsUn();
			}else {
				cursor = null;
			}
		}

		cardsViewData.resizeWorld2d(maxX + MARGIN_LEFT, maxY + MARGIN_LEFT);

        // TODO: créer des Carte2d pour afficher les cartes du modèle
    }

    @Override
    public void initializeAsTestCase(AbstractTestCaseDescriptor descriptor) {
    	n = descriptor.inputSize();
    	if(descriptor.category().equals("recursif")) {
    		siRecursif = true;
    	}else {
    		siRecursif = false;
    	}
    }

    @Override
    public int testCaseSize() {
    	return this.n;
    }
    
    @Override
    protected Stream<Carte> cards() {
        return new StreamNtro<Carte>() {
            @Override
            public void forEach_(Visitor<Carte> visitor) throws Throwable {
                // TODO: visiter chaque carte
            }
        };
    }


    @Override
    public void run() {
        calculerFactoriel();
    }

    public void calculerFactoriel() {
    }

    @Override
    public void displayOn(FactorialVariablesView variablesView) {
    }




}

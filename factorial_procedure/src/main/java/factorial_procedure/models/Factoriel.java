package factorial_procedure.models;

import java.io.Serializable;

import ca.ntro.app.models.ModelValue;
import common.test_cases.execution.Execution;

@SuppressWarnings("serial")
public class Factoriel implements ModelValue, Serializable {
	
	protected Factoriel moinsUn;
	protected int n;
	protected Long reponse;
	
	public Factoriel() {
	}

	public Factoriel(int n) {
		setN(n);
	}

	public Factoriel getMoinsUn() {
		return moinsUn;
	}

	public void setMoinsUn(Factoriel moinsUn) {
		this.moinsUn = moinsUn;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public Long getReponse() {
		return reponse;
	}

	public void setReponse(Long reponse) {
		this.reponse = reponse;
	}

	public void calculRecursif() {
		if(n == 0) {

			reponse = 1l;
			Execution.ajouterEtape();

		}else {
			
			moinsUn = new Factoriel(n-1);
			Execution.ajouterEtape();
			moinsUn.calculRecursif();

			reponse = moinsUn.getReponse() * n;
			Execution.ajouterEtape();
		}
	}

	public void calculDynamique() {
		if(n == 0) {

			reponse = 1l;

		}else {
			
			reponse = moinsUn.getReponse() * n;
			
		}
		
	}

}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package factorial_procedure.models.world2d;

import java.util.ArrayList;
import java.util.List;

import ca.ntro.app.Ntro;
import common.CommonConstants;
import common.models.values.cards.Carte;
import common_procedure.models.world2d.ProcedureWorld2d;
import factorial_procedure.FactorialConstants;
import factorial_procedure.messages.FactorialMsgAcceptManualModel;
import factorial_procedure.models.Calculateur;

public class FactorialProcedureWorld2d extends ProcedureWorld2d<FactorialProcedureWorld2d, FactorialProcedureDrawingOptions> {
    
    
    @Override
    public void buildAndSendManualModel() {

        // TODO: analyser les cartes2d existantes et créer un CardsModel 
        
        Calculateur manualModel = new Calculateur();

        Ntro.newMessage(FactorialMsgAcceptManualModel.class)
        	.setManualModel(manualModel)
        	.send();
    }


}

/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package shift_procedure.backend;



import ca.ntro.app.tasks.backend.BackendTasks;
import static ca.ntro.app.tasks.backend.BackendTasks.*;

import shift_procedure.messages.ShiftMsgAcceptManualModel;
import shift_procedure.models.ShiftProcedureDashboardModel;
import shift_procedure.models.ShiftProcedureSettingsModel;
import shift_procedure.models.Tableau;
import shift_procedure.models.values.ShiftTestCase;
import shift_procedure.test_cases.ShiftTestCaseDatabase;
import shift_procedure.test_cases.execution_trace.ShiftExecutionTrace;
import common_procedure.backend.ProcedureBackend;
import common_procedure.messages.ProcedureMsgAcceptManualModel;

public class   ShiftProcedureBackend<STUDENT_MODEL extends Tableau>


       extends ProcedureBackend<Tableau,       // ExecutableModel
                                STUDENT_MODEL,
                                STUDENT_MODEL,        // CanvasModel
                                ShiftTestCase,
                                ShiftTestCaseDatabase,
                                ShiftExecutionTrace,
                                ShiftProcedureDashboardModel,
                                ShiftProcedureSettingsModel,
                                ShiftMsgAcceptManualModel> {

	
	


}

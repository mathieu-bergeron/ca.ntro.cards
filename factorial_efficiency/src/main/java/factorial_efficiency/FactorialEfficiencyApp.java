/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package factorial_efficiency;

import factorial_efficiency.backend.FactorialEfficiencyBackend;
import factorial_efficiency.frontend.FactorialEfficiencyFrontend;
import factorial_efficiency.frontend.FactorialEfficiencyViewData;
import factorial_efficiency.frontend.views.FactorialEfficiencyDashboardView;
import factorial_efficiency.frontend.views.FactorialEfficiencyMessagesView;
import factorial_efficiency.frontend.views.FactorialEfficiencyRootView;
import factorial_efficiency.frontend.views.FactorialEfficiencySettingsView;
import factorial_efficiency.frontend.views.FactorialGraphsView;
import factorial_efficiency.frontend.views.fragments.FactorialEfficiencyMessageFragment;
import factorial_efficiency.models.FactorialEfficiencyDashboardModel;
import factorial_efficiency.models.FactorialEfficiencySettingsModel;
import factorial_efficiency.models.FactorialGraphsModel;
import factorial_procedure.models.Calculateur;
import factorial_procedure.models.values.FactorialTestCase;
import factorial_procedure.test_cases.FactorialTestCaseDatabase;
import factorial_procedure.test_cases.execution_trace.FactorialExecutionTrace;
import common.models.enums.Attempt;
import common_efficiency.EfficiencyApp;

public abstract class   FactorialEfficiencyApp<STUDENT_MODEL extends Calculateur>

                extends EfficiencyApp<Calculateur, 
                                      STUDENT_MODEL,
                                      FactorialGraphsModel,
                                      FactorialTestCase,
                                      FactorialTestCaseDatabase,
                                      FactorialExecutionTrace,
                                      FactorialEfficiencyDashboardModel,
                                      FactorialEfficiencySettingsModel,
                                      FactorialEfficiencyBackend<STUDENT_MODEL>,
                                      FactorialEfficiencyRootView,
                                      FactorialGraphsView,
                                      FactorialEfficiencyDashboardView,
                                      FactorialEfficiencySettingsView,
                                      FactorialEfficiencyMessagesView,
                                      FactorialEfficiencyMessageFragment,
                                      FactorialEfficiencyViewData,
                                      FactorialEfficiencyFrontend> {

	@Override
	protected FactorialEfficiencyFrontend createFrontend() {
		return new FactorialEfficiencyFrontend();
	}

	@Override
	protected FactorialEfficiencyBackend createBackend() {
		return new FactorialEfficiencyBackend();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class<Calculateur> executableModelClass() {
		return Calculateur.class;
	}

	@Override
	protected Class<STUDENT_MODEL> studentModelClass() {
		return studentClass();
	}
	
	// TODO: renommer
	protected abstract Class<STUDENT_MODEL> studentClass();

	@Override
	protected Class<FactorialGraphsModel> canvasModelClass() {
		return FactorialGraphsModel.class;
	}

	@Override
	protected Class<FactorialTestCase> testCaseClass() {
		return FactorialTestCase.class;
	}

	@Override
	protected Class<FactorialTestCaseDatabase> testCasesModelClass() {
		return FactorialTestCaseDatabase.class;
	}

	@Override
	protected Class<FactorialEfficiencyDashboardModel> dashboardModelClass() {
		return FactorialEfficiencyDashboardModel.class;
	}

	@Override
	protected Class<FactorialEfficiencySettingsModel> settingsModelClass() {
		return FactorialEfficiencySettingsModel.class;
	}

	@Override
	protected String initialTestCaseId() {
		return "ex01";
	}

	@Override
	protected Attempt initialAttempt() {
		return Attempt.SOLUTION;
	}
}

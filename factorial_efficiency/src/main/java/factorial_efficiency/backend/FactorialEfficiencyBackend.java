/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package factorial_efficiency.backend;

import common_efficiency.backend.EfficiencyBackend;
import factorial_efficiency.models.FactorialEfficiencyDashboardModel;
import factorial_efficiency.models.FactorialEfficiencySettingsModel;
import factorial_efficiency.models.FactorialGraphsModel;
import factorial_procedure.models.Calculateur;
import factorial_procedure.models.values.FactorialTestCase;
import factorial_procedure.test_cases.FactorialTestCaseDatabase;
import factorial_procedure.test_cases.execution_trace.FactorialExecutionTrace;

public class FactorialEfficiencyBackend<STUDENT_MODEL extends Calculateur>

       extends EfficiencyBackend<Calculateur, 
                                 STUDENT_MODEL,
                                 FactorialGraphsModel,            // CanvasModel
                                 FactorialTestCase,
                                 FactorialTestCaseDatabase,
                                 FactorialExecutionTrace,
                                 FactorialEfficiencyDashboardModel,
                                 FactorialEfficiencySettingsModel> {




}

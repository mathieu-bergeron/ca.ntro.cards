/*
Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of "Cartes Java", teaching tools made for https://cartesjava.github.io/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package factorial_efficiency.frontend;

import factorial_efficiency.frontend.views.FactorialEfficiencyDashboardView;
import factorial_efficiency.frontend.views.FactorialEfficiencyMessagesView;
import factorial_efficiency.frontend.views.FactorialEfficiencyRootView;
import factorial_efficiency.frontend.views.FactorialEfficiencySettingsView;
import factorial_efficiency.frontend.views.FactorialGraphsView;
import factorial_efficiency.frontend.views.fragments.FactorialEfficiencyMessageFragment;
import factorial_efficiency.models.FactorialEfficiencyDashboardModel;
import factorial_efficiency.models.FactorialEfficiencySettingsModel;
import factorial_efficiency.models.FactorialGraphsModel;
import ca.ntro.app.Ntro;
import common.messages.MsgMessageToUser;
import common_efficiency.frontend.EfficiencyFrontend;

public class FactorialEfficiencyFrontend 

       extends EfficiencyFrontend<FactorialEfficiencyRootView,
                                  FactorialEfficiencySettingsView, 
                                  FactorialGraphsView, 
                                  FactorialEfficiencyDashboardView, 
                                  FactorialEfficiencyMessagesView,
                                  FactorialEfficiencyMessageFragment,
                                  FactorialEfficiencyViewData, 
                                  FactorialGraphsModel, 
                                  FactorialEfficiencyDashboardModel, 
                                  FactorialEfficiencySettingsModel> {

	@Override
	protected boolean isProd() {
		return true;
	}

	@Override
	protected Class<FactorialEfficiencyRootView> rootViewClass() {
		return FactorialEfficiencyRootView.class;
	}

	@Override
	protected Class<FactorialEfficiencySettingsView> settingsViewClass() {
		return FactorialEfficiencySettingsView.class;
	}

	@Override
	protected Class<FactorialGraphsView> canvasViewClass() {
		return FactorialGraphsView.class;
	}

	@Override
	protected Class<FactorialEfficiencyDashboardView> dashboardViewClass() {
		return FactorialEfficiencyDashboardView.class;
	}


	@Override
	protected Class<FactorialEfficiencyViewData> viewDataClass() {
		return FactorialEfficiencyViewData.class;
	}


	@Override
	protected Class<FactorialEfficiencyMessagesView> messagesViewClass() {
		return FactorialEfficiencyMessagesView.class;
	}

	@Override
	protected Class<FactorialEfficiencyMessageFragment> messageFragmentClass() {
		return FactorialEfficiencyMessageFragment.class;
	}

}
